#pragma once

void on_recv_socket_cq_event(int fd, void* ptr);
void on_recv_socket_tcp_cq_event(int fd, void* ptr);

void on_send_socket_cq_event(int fd, void* ptr);
void on_send_socket_tcp_cq_event(int fd, void* ptr);

