
/* Pythons Netio Callbacks */
extern "Python" void on_init(void*);
extern "Python" void on_file(int, void*);
extern "Python" void on_timer(void*);
extern "Python" void on_signal(void*);

extern "Python" void on_publish_subscribe(struct netio_publish_socket* socket, netio_tag_t tag, void* addr, size_t addrlen);
extern "Python" void on_publish_connection_established(struct netio_publish_socket* socket);
extern "Python" void on_publish_connection_closed(struct netio_publish_socket* socket);
extern "Python" void on_publish_buffer_available(struct netio_publish_socket* socket);

extern "Python" void on_subscribe_connection_established(struct netio_subscribe_socket* socket);
extern "Python" void on_subscribe_connection_closed(struct netio_subscribe_socket* socket);
extern "Python" void on_subscribe_error_connection_refused(struct netio_subscribe_socket* socket);
extern "Python" void on_subscribe_msg_received(struct netio_subscribe_socket* socket, netio_tag_t tag, void* data, size_t size);
extern "Python" void on_subscribe_buf_received(struct netio_buffer* buffer, size_t size);

extern "Python" void on_recv_connection_established(struct netio_buffered_recv_socket* socket);
extern "Python" void on_recv_connection_closed(struct netio_buffered_recv_socket* socket);
extern "Python" void on_recv_msg_received(struct netio_buffered_recv_socket* socket, void* data, size_t size);

extern "Python" void on_connection_established(struct netio_buffered_send_socket* socket);
extern "Python" void on_connection_closed(struct netio_buffered_send_socket* socket);
extern "Python" void on_error_connection_refused(struct netio_buffered_send_socket* socket);

""")

# set_source() gives the name of the python extension module to
# produce, and some C source code as a string.  This C code needs
# to make the declarated functions, types and globals available,
# so it is often just the "#include".
ffibuilder.set_source("netio_py",
                      """
                        #include "netio/netio.h"   // the C header of the library
                        #include "netio/netio_tcp.h"   // the C header of the library
                      """,
                      libraries=[])   # library name, for the linker

if __name__ == "__main__":
    ffibuilder.emit_c_code("netio_py.c")
