#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdarg.h>
#include <math.h>
#include <sys/types.h>
#include <unistd.h>
#include "util.h"

int str_append(char **dst, const char *format, ...)
{
    char *str = NULL;
    char *old_dst = NULL, *new_dst = NULL;

    va_list arg_ptr;
    va_start(arg_ptr, format);
    vasprintf(&str, format, arg_ptr);
    va_end(arg_ptr);

    // save old dst
    asprintf(&old_dst, "%s", (*dst == NULL ? "" : *dst));

    // calloc new dst memory
    new_dst = (char *)calloc(strlen(old_dst) + strlen(str) + 1, sizeof(char));

    strcat(new_dst, old_dst);
    strcat(new_dst, str);

    if (*dst) free(*dst);
    *dst = new_dst;

    free(old_dst);
    free(str);

    return 0;
}

char* execute(char* cmd) {
  char buffer[128];

  char *output = NULL;

  // Open pipe to file
  FILE* pipe = popen(cmd, "r");
  if (!pipe) {
    return (char*)"popen failed!";
  }

  // read till end of process:
  while (!feof(pipe)) {
    // use buffer to read and add to result
    if (fgets(buffer, 128, pipe) != NULL) {
      str_append(&output, "%s", buffer);
    }
  }

  pclose(pipe);
  return output;
}
