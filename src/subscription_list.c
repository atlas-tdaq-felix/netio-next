#include "netio.h"

void push_back_subscription(struct deferred_subscription** list, netio_tag_t tag)
{
    struct deferred_subscription* new_item = malloc(sizeof(struct deferred_subscription));
    new_item->tag = tag;
    new_item->next = NULL;

    if(*list == NULL){
        *list = new_item;
    } else {
        struct deferred_subscription* iterator  = *list;
        while(iterator->next){
            iterator = iterator->next;
        }
        iterator->next = new_item;
    }
}

void pop_subscription(struct deferred_subscription** list)
{
    if(*list == NULL) return;
    struct deferred_subscription* current_head = *list;
    *list = (*list)->next;
    free(current_head);
}

