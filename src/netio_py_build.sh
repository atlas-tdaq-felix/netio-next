#!/bin/sh

SRC=$(dirname $(readlink -f ${BASH_SOURCE[0]:-${(%):-%x}}))

sed -f ${SRC}/netio_py_filter.sed ${SRC}/../netio/netio.h > netio_py.txt
sed -f ${SRC}/netio_py_filter.sed ${SRC}/../netio/netio_tcp.h > netio_tcp_py.txt
cat ${SRC}/netio_py_prefix.txt netio_py.txt netio_tcp_py.txt ${SRC}/netio_py_postfix.txt > cffi_generate.py
chmod +x cffi_generate.py
./cffi_generate.py
