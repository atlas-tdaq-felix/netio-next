#include "netio/netio.h"

/**
 * @brief Creates a stack of completion objects
 *
 * A message can be bound to multiple subscribers.
 * A completion object (co) is associated to each message.
 * The co is held until the message has been delivered to all the subscribers.
 * To keep track of how many subscribers received the message the co is equipped with a semaphore.
 * The semaphore has a threshold equal to the number of subscriptions and a counter.
 * When the counter exceeds the threshold the semaphore fires;
 * only then `msg_published` will be called and the DMA read pointer advanced.
 *
 * @param netio_completion_stack: stack of completion objects
 * @param num:     total number of completion objects
 */

void
netio_completion_stack_init(struct netio_completion_stack* stack, size_t num)
{
    stack->objects = malloc(num * sizeof(struct netio_completion_object));
    stack->stack = malloc(num * sizeof(struct netio_completion_object*));
    stack->key_array = malloc(num * sizeof(uint64_t));
    for(size_t i=0; i<num; i++) {
      stack->stack[i] = &(stack->objects[i]);
      stack->stack[i]->key = 0xFFFFFFFFFFFFFFFF;
      stack->key_array[i] = 0xFFFFFFFFFFFFFFFF;
    }
    stack->available_objects = num;
    stack->num_objects = num;
    stack->buf.data = stack->objects;
    stack->buf.size = num * sizeof(struct netio_completion_object);
    stack->printed = 0;
}


void
netio_completion_stack_register_send_socket(struct netio_completion_stack* stack, struct netio_send_socket* socket)
{
    netio_register_send_buffer(socket, &stack->buf, 0);
}


int
netio_completion_stack_pop(struct netio_completion_stack* stack, struct netio_completion_object** object)
{
    if(stack->available_objects == 0) {
      return 1;
    }
    stack->available_objects--;
    *object = stack->stack[stack->available_objects];
    return 0;
}


int
netio_completion_stack_push(struct netio_completion_stack* stack, struct netio_completion_object* object)
{
    if(stack->available_objects == stack->num_objects) {
      return 1;
    }
    object->key = 0xFFFFFFFFFFFFFFFF;
    stack->stack[stack->available_objects] = object;
    stack->key_array[stack->available_objects] = 0xFFFFFFFFFFFFFFFF;
    stack->available_objects++;
    if(stack->printed == 1 && stack->available_objects == 250){
      stack->printed = 0;
    }
    return 0;
}
