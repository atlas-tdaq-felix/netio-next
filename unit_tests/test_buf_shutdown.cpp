#include <catch2/catch_test_macros.hpp>

extern "C" {
#include "netio/netio.h"
}

#include <cstring>
#include "util.h"

namespace test_shutdown {

unsigned long TAG = 1UL << 42;

struct State {
  bool initialized = false;
  bool pub_closed_cb_called = false;
  bool sub_closed_cb_called = false;
  unsigned num_subscriptions = 0;
  unsigned num_connections = 0;
  unsigned num_msg_received = 0;
} state;

char pdata[64];

struct netio_context ctx;
struct netio_publish_socket pub;
struct netio_subscribe_socket sub;

void sub_on_msg_received(struct netio_subscribe_socket* socket, netio_tag_t tag, void* data, size_t size);
void sub_on_connection_closed(struct netio_subscribe_socket* socket);


void on_init(void* data) {
  state.initialized = true;
  unsigned port = port_of_listen_socket(&pub.lsocket);
  REQUIRE(port > 0);

  struct netio_buffered_socket_attr attr;
  attr.num_pages = 8;
  attr.pagesize = 4*1024;
  attr.watermark = 3*1024;

  netio_subscribe_socket_init(&sub, &ctx, &attr, test_iface_addr(), test_iface_addr(), port);
  sub.cb_msg_received = sub_on_msg_received;
  sub.cb_connection_closed = sub_on_connection_closed;
  netio_subscribe(&sub, TAG);
}

//------  publish socket callbacks

void pub_on_subscribe(struct netio_publish_socket* socket, netio_tag_t tag, void* addr, size_t addrlen)
{
  state.num_subscriptions++;
}

void pub_on_unsubscribe(struct netio_publish_socket* socket, netio_tag_t tag, void* addr, size_t addrlen)
{
  state.num_subscriptions--;
}

void pub_on_connection_established(struct netio_publish_socket* socket)
{
  state.num_connections++;
  memcpy(pdata, "foobar", 7);
  REQUIRE( NETIO_STATUS_OK == netio_buffered_publish(
                              &pub,       // publish socket
                              TAG,        // tag
                              &pdata,     // data
                              7,          // data length
                              0,          // flags
                              NULL));     // subscription cache
  netio_buffered_publish_flush(
                              &pub,       // publish socket
                              TAG,        // tag
                              NULL);      // subscription cache
}

void pub_on_connection_closed(struct netio_publish_socket* socket)
{
  printf("On connection closed on publisher side.\n");
  state.pub_closed_cb_called = true;
  //netio_close_socket(&ctx.evloop, &pub, BPUB);
  if (state.sub_closed_cb_called){
    netio_terminate(&ctx.evloop);
    //netio_terminate_signal(&ctx.evloop);
  }
}


//------  subscribe socket callbacks

void sub_on_msg_received(struct netio_subscribe_socket* socket, netio_tag_t tag, void* data, size_t size)
{
  REQUIRE( size == 7 );
  REQUIRE( tag == TAG );
  REQUIRE( memcmp(data, "foobar", 7) == 0 );
  state.num_msg_received++;
  netio_unsubscribe(socket, tag);
}

void sub_on_connection_closed(struct netio_subscribe_socket* socket)
{
  printf("On connection closed on subscriber side.\n");
  state.sub_closed_cb_called = true;
  //netio_close_socket(&ctx.evloop, &sub, BSUB);
  if (state.pub_closed_cb_called){
    netio_terminate(&ctx.evloop);
    //netio_terminate_signal(&ctx.evloop);
  }
}

//------  test

TEST_CASE( "buffered publisher-subscriber shutdown", "[buf_shutdown]" ) {
  state = State();
  netio_init(&ctx);
  ctx.evloop.cb_init = on_init;

  struct netio_buffered_socket_attr attr;
  attr.num_pages = 8;
  attr.pagesize = 4*1024;
  attr.watermark = 3*1024;

  netio_publish_socket_init(&pub, &ctx, test_iface_addr(), 0, &attr);
  pub.cb_subscribe = pub_on_subscribe;
  pub.cb_unsubscribe = pub_on_unsubscribe;
  pub.cb_connection_established = pub_on_connection_established;
  pub.cb_connection_closed = pub_on_connection_closed;

  netio_run(&ctx.evloop);
  printf("END OF RUN\n");
  REQUIRE( state.num_subscriptions == 0 );
  REQUIRE( state.num_msg_received == 1 );
  REQUIRE( (state.pub_closed_cb_called == true && state.sub_closed_cb_called == true) );
  REQUIRE( state.num_msg_received == 1 );
  REQUIRE( pub.subscription_table.num_subscriptions == 0);
  REQUIRE( is_socket_list_empty(pub.subscription_table.socket_list) == 1);
}

}
