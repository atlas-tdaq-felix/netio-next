#include <catch2/catch_test_macros.hpp>

extern "C" {
#include "netio/netio.h"
}

#include <cstring>
#include "util.h"

namespace test_unbuf_shutdown {

unsigned long TAG = 1UL << 42;

struct State {
  bool initialized = false;
  bool pub_closed_cb_called = false;
  bool sub_closed_cb_called = false;
  unsigned num_subscriptions = 0;
  unsigned num_connections = 0;
  unsigned num_msg_received = 0;
} state;

char pdata[64];
struct netio_buffer pbuf;
uint64_t key;

struct netio_context ctx;
struct netio_unbuffered_publish_socket pub;
struct netio_unbuffered_subscribe_socket sub;

void sub_on_msg_received(struct netio_unbuffered_subscribe_socket* socket, netio_tag_t tag, void* data, size_t size);
void sub_on_connection_closed(struct netio_unbuffered_subscribe_socket* socket);


void on_init(void* data) {
  state.initialized = true;
  unsigned port = port_of_listen_socket(&pub.lsocket);
  REQUIRE(port > 0);
  netio_unbuffered_subscribe_socket_init(&sub, &ctx, test_iface_addr(), test_iface_addr(), port, 1024, 1);
  sub.cb_msg_received = sub_on_msg_received;
  sub.cb_connection_closed = sub_on_connection_closed;
  netio_unbuffered_subscribe(&sub, TAG);
}

//------  publish socket callbacks

void pub_on_subscribe(struct netio_unbuffered_publish_socket* socket, netio_tag_t tag, void* addr, size_t addrlen)
{
  state.num_subscriptions++;
}

void pub_on_unsubscribe(struct netio_unbuffered_publish_socket* socket, netio_tag_t tag, void* addr, size_t addrlen)
{
  state.num_subscriptions--;
}

void pub_on_connection_established(struct netio_unbuffered_publish_socket* socket)
{
  state.num_connections++;
  memcpy(pdata, "foobar", 7);
  struct iovec iov;
  iov.iov_base = pdata;
  iov.iov_len = 7;
  key = 0xDEADBEEF;

  REQUIRE( NETIO_STATUS_OK == netio_unbuffered_publishv(
                              &pub,       // publish socket
                              TAG,        // tag
                              &iov,       // iov
                              1,          // iov count
                              &key,       //key for completion object
                              0,          // flags
                              NULL));     // subscription cache
}

void pub_on_connection_closed(struct netio_unbuffered_publish_socket* socket)
{
  printf("On connection closed on publisher side.\n");
  state.pub_closed_cb_called = true;
  if(state.sub_closed_cb_called){
    netio_terminate(&ctx.evloop);
  }
}


//------  subscribe socket callbacks

void sub_on_msg_received(struct netio_unbuffered_subscribe_socket* socket, netio_tag_t tag, void* data, size_t size)
{
  REQUIRE( size == 7 );
  REQUIRE( tag == TAG );
  REQUIRE( memcmp(data, "foobar", 7) == 0 );
  state.num_msg_received++;
  netio_unbuffered_unsubscribe(socket, tag);
}

void sub_on_connection_closed(struct netio_unbuffered_subscribe_socket* socket)
{
  printf("On connection closed on subscriber side.\n");
  state.sub_closed_cb_called = true;
  if(state.pub_closed_cb_called){
    netio_terminate(&ctx.evloop);
  }
}

//------  test

TEST_CASE( "unbuffered publisher-subscriber shutdown", "[unbuf_shutdown]" ) {
  state = State();
  netio_init(&ctx);
  ctx.evloop.cb_init = on_init;

  pbuf.data = pdata;
  pbuf.size = 1024;
  netio_unbuffered_publish_socket_init(&pub, &ctx, test_iface_addr(), 0, &pbuf);
  pub.cb_subscribe = pub_on_subscribe;
  pub.cb_unsubscribe = pub_on_unsubscribe;
  pub.cb_connection_established = pub_on_connection_established;
  pub.cb_connection_closed = pub_on_connection_closed;

  netio_run(&ctx.evloop);
  REQUIRE( state.num_subscriptions == 0 );
  REQUIRE( state.num_msg_received == 1 );
  REQUIRE( (state.pub_closed_cb_called == true && state.sub_closed_cb_called == true) );
  REQUIRE( state.num_msg_received == 1 );
  REQUIRE( pub.subscription_table.num_subscriptions == 0);
  REQUIRE(  is_socket_list_empty(pub.subscription_table.socket_list) == 1 );
}

}
