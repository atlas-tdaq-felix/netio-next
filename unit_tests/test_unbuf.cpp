#include <catch2/catch_test_macros.hpp>

extern "C" {
#include "netio/netio.h"
}

#include <cstring>

#include "util.h"

namespace {


struct netio_context ctx;
struct netio_send_socket ssocket;
struct netio_listen_socket lsocket;
struct netio_buffer sbuf;

const size_t BUFSIZE = 1024;
char rdata[BUFSIZE];
char sdata[BUFSIZE];

struct State {
  bool initialized = false;
  struct {
    bool connection_established = false;
    bool connection_closed = false;
    bool connection_refused = false;
    bool msg_handled = false;
    int fi_errno = 0;
    char* fi_message = NULL;
  } ss, ls;
} state;


void on_init(void *data)
{
  state.initialized = true;
}


namespace connect_test {
  void on_ss_connection_established(struct netio_send_socket*)
  {
    state.ss.connection_established = true;
    if(state.ls.connection_established && state.ss.connection_established) {
      netio_terminate(&ctx.evloop);
    }
  }

  void on_ss_connection_closed(struct netio_send_socket*)
  {
    state.ss.connection_closed = true;
  }

  void on_ls_connection_established(struct netio_recv_socket*)
  {
    state.ls.connection_established = true;
    if(state.ls.connection_established && state.ss.connection_established) {
      netio_terminate(&ctx.evloop);
    }
  }

  void on_ls_connection_closed(struct netio_recv_socket*)
  {
    state.ls.connection_closed = true;
  }

  TEST_CASE( "connect an unbuffered socket", "[unbuf]" )
  {
    state = State();

    netio_init(&ctx);
    ctx.evloop.cb_init = on_init;

    netio_init_send_socket(&ssocket, &ctx);
    ssocket.cb_connection_established = on_ss_connection_established;
    ssocket.cb_connection_closed = on_ss_connection_closed;

    netio_init_listen_socket(&lsocket, &ctx, NULL);
    lsocket.cb_connection_established = on_ls_connection_established;
    lsocket.cb_connection_closed = on_ls_connection_closed;
    lsocket.recv_sub_msg = 1;

    netio_listen(&lsocket,  test_iface_addr(), 0);
    netio_connect(&ssocket, test_iface_addr(), port_of_listen_socket(&lsocket));

    netio_run(&ctx.evloop);

    REQUIRE(state.initialized == true);
    REQUIRE(state.ss.connection_established == true);
    REQUIRE(state.ls.connection_established == true);
  }
}

namespace send_msg_test {
  void on_ss_connection_established(struct netio_send_socket* ss)
  {
    REQUIRE( state.ls.connection_established );

    state.ss.connection_established = true;

    sbuf.data = sdata;
    sbuf.size = BUFSIZE;
    netio_register_send_buffer(&ssocket, &sbuf, 0);
    memcpy(sdata, "Hello, ", 7);
    memcpy(sdata+512, "World", 6);
    struct iovec iov[2];
    iov[0].iov_base = sdata;
    iov[0].iov_len = 7;
    iov[1].iov_base = sdata+512;
    iov[1].iov_len = 6;

    struct netio_buffer* bufs[] = { &sbuf, &sbuf };

    REQUIRE( 0 == netio_sendv_imm(ss, bufs, iov, 2, 0xDEADBEEF, 0xABCDE) );
  }

  void on_ss_connection_closed(struct netio_send_socket*)
  {
    state.ss.connection_closed = true;
  }

  void on_ss_send_completed(struct netio_send_socket* ss, uint64_t key)
  {
    state.ss.msg_handled = true;
    REQUIRE(key == 0xDEADBEEF);
    if(state.ls.msg_handled && state.ss.msg_handled) {
      netio_terminate(&ctx.evloop);
    }
  }

  void on_ls_connection_established(struct netio_recv_socket* rs)
  {
    state.ls.connection_established = true;
  }

  void on_ls_connection_closed(struct netio_recv_socket*)
  {
    state.ls.connection_closed = true;
  }

  void on_ls_msg_received(struct netio_recv_socket* rs, struct netio_buffer* buf, void* data, size_t size, uint64_t imm)
  {
    state.ls.msg_handled = true;
    if(state.ls.msg_handled && state.ss.msg_handled) {
      netio_terminate(&ctx.evloop);
    }
    REQUIRE(size == 13);
    CAPTURE((char*)data);
    REQUIRE( data != NULL );
    REQUIRE( 0 == strncmp((char*)data, "Hello, World", 12) );
    //REQUIRE(buf == &rbuf);
    REQUIRE(imm == 0xABCDE);
  }

  TEST_CASE( "send message on unbuffered socket", "[unbuf][.]" )
  {
    state = State();

    netio_init(&ctx);
    ctx.evloop.cb_init = on_init;

    memset(&ssocket, 0, sizeof(ssocket));
    netio_init_send_socket(&ssocket, &ctx);
    ssocket.cb_connection_established = on_ss_connection_established;
    ssocket.cb_connection_closed = on_ss_connection_closed;
    ssocket.cb_send_completed = on_ss_send_completed;

    memset(&lsocket, 0, sizeof(lsocket));

    struct netio_unbuffered_socket_attr attr = {1, BUFSIZE};
    netio_init_listen_socket(&lsocket, &ctx, &attr);
    lsocket.cb_connection_established = on_ls_connection_established;
    lsocket.cb_connection_closed = on_ls_connection_closed;
    lsocket.cb_msg_imm_received = on_ls_msg_received;

    netio_listen(&lsocket, test_iface_addr(), 0);
    netio_connect(&ssocket, test_iface_addr(), port_of_listen_socket(&lsocket));

    netio_run(&ctx.evloop);

    REQUIRE(state.initialized == true);
    REQUIRE(state.ss.connection_established == true);
    REQUIRE(state.ls.connection_established == true);
    REQUIRE(state.ss.msg_handled == true);
    REQUIRE(state.ls.msg_handled == true);
  }
}

namespace disconnect_ss_test {
  void on_ss_connection_established(struct netio_send_socket* ss)
  {
    state.ss.connection_established = true;
    if(state.ss.connection_established && state.ls.connection_established) {
      netio_disconnect(ss);
    }
  }

  void on_ss_connection_closed(struct netio_send_socket*)
  {
    state.ss.connection_closed = true;
    if(state.ss.connection_closed && state.ls.connection_closed) {
      netio_terminate(&ctx.evloop);
    }
  }

  void on_ls_connection_established(struct netio_recv_socket* rs)
  {
    state.ls.connection_established = true;
    if(state.ss.connection_established && state.ls.connection_established) {
      netio_disconnect(&ssocket);
    }
  }

  void on_ls_connection_closed(struct netio_recv_socket* rs)
  {
    state.ls.connection_closed = true;
    if(state.ss.connection_closed && state.ls.connection_closed) {
      netio_terminate(&ctx.evloop);
    }
  }

  void on_ls_msg_received(struct netio_recv_socket* rs, struct netio_buffer* buf, void* data, size_t size, uint64_t imm)
  {
    REQUIRE(false);
  }

  TEST_CASE( "disconnect while waiting for message", "[unbuf]" )
  {
    state = State();

    netio_init(&ctx);
    ctx.evloop.cb_init = on_init;

    memset(&ssocket, 0, sizeof(ssocket));
    netio_init_send_socket(&ssocket, &ctx);
    ssocket.cb_connection_established = on_ss_connection_established;
    ssocket.cb_connection_closed = on_ss_connection_closed;

    memset(&lsocket, 0, sizeof(lsocket));
    struct netio_unbuffered_socket_attr attr = {1, BUFSIZE};
    netio_init_listen_socket(&lsocket, &ctx, &attr);
    lsocket.cb_connection_established = on_ls_connection_established;
    lsocket.cb_connection_closed = on_ls_connection_closed;
    lsocket.cb_msg_imm_received = on_ls_msg_received;

    netio_listen(&lsocket, test_iface_addr(), 0);
    netio_connect(&ssocket, test_iface_addr(), port_of_listen_socket(&lsocket));

    netio_run(&ctx.evloop);

    REQUIRE(state.ls.connection_established);
    REQUIRE(state.ss.connection_established);
    REQUIRE(state.ls.connection_closed);
    REQUIRE(state.ss.connection_closed);
  }
}


namespace disconnect_rs_test {
  void on_ss_connection_established(struct netio_send_socket* ss)
  {
    state.ss.connection_established = true;
    sbuf.data = sdata;
    sbuf.size = BUFSIZE;
    netio_register_send_buffer(ss, &sbuf, 0);
    memcpy(sdata, "Hello, World", 13);
    struct iovec iov;
    iov.iov_base = sdata;
    iov.iov_len = 13;

    struct netio_buffer* bufs[] = { &sbuf };

    REQUIRE( 0 == netio_sendv_imm(ss, bufs, &iov, 1, 0xDEADBEEF, 0xABCDE) );
  }

  void on_ss_connection_closed(struct netio_send_socket*)
  {
    state.ss.connection_closed = true;
  }

  void on_ls_connection_established(struct netio_recv_socket* rs)
  {
    state.ls.connection_established = true;
    fi_shutdown(rs->ep, 0);
  }

  void on_ls_connection_closed(struct netio_recv_socket* rs)
  {
    state.ls.connection_closed = true;
  }

  void on_ls_msg_received(struct netio_recv_socket* rs, struct netio_buffer* buf, void* data, size_t size, uint64_t imm)
  {
    REQUIRE(false);
  }

  TEST_CASE( "disconnect while sending message", "[unbuf][.]" )
  {
    state = State();

    netio_init(&ctx);
    ctx.evloop.cb_init = on_init;

    memset(&ssocket, 0, sizeof(ssocket));
    netio_init_send_socket(&ssocket, &ctx);
    ssocket.cb_connection_established = on_ss_connection_established;
    ssocket.cb_connection_closed = on_ss_connection_closed;

    memset(&lsocket, 0, sizeof(lsocket));
    struct netio_unbuffered_socket_attr attr = {1, BUFSIZE};
    netio_init_listen_socket(&lsocket, &ctx, &attr);
    lsocket.cb_connection_established = on_ls_connection_established;
    lsocket.cb_connection_closed = on_ls_connection_closed;
    lsocket.cb_msg_imm_received = on_ls_msg_received;

    netio_listen(&lsocket, test_iface_addr(), 0);
    netio_connect(&ssocket, test_iface_addr(), port_of_listen_socket(&lsocket));

    netio_run(&ctx.evloop);

    REQUIRE(state.ls.connection_established);
    REQUIRE(state.ss.connection_established);
    REQUIRE(state.ls.connection_closed);
    REQUIRE(state.ss.connection_closed);
  }
}



namespace connection_error_test {

  void on_error_connection_refused(struct netio_send_socket* socket) {
    state.ss.connection_refused = true;
    state.ss.fi_errno = socket->fi_errno;
    state.ss.fi_message = (socket->fi_message) ? strdup(socket->fi_message) : NULL;
    netio_terminate(&ctx.evloop);
  }

  TEST_CASE( "connect to non-existing remote", "[unbuf]" )
  {
    state = State();

    netio_init(&ctx);
    ctx.evloop.cb_init = on_init;

    memset(&ssocket, 0, sizeof(ssocket));
    netio_init_send_socket(&ssocket, &ctx);
    ssocket.cb_error_connection_refused = on_error_connection_refused;

    SECTION( "non-existing node" ) {
      netio_connect(&ssocket, "unknown", 111); // non-existing node -> -61 getinfo: No data available, FI_ENODATA
      netio_run(&ctx.evloop);
      REQUIRE(state.ss.connection_refused);
      REQUIRE(state.ss.fi_errno == FI_ENODATA);
      REQUIRE(!strcmp(state.ss.fi_message, "fi_getinfo failed"));
    }

    SECTION( "non-existing port" ) {
      netio_connect(&ssocket, "www.cern.ch", 111); // non-existing port -> -111 fi_connect: Connection refused, FI_ECONNREFUSED
      netio_run(&ctx.evloop);
      REQUIRE(state.ss.connection_refused);
      REQUIRE(state.ss.fi_errno == FI_ECONNREFUSED);
      REQUIRE(!strcmp(state.ss.fi_message, "fi_connect failed"));
    }

    // SECTION( "non-existing interface" ) {
    //   netio_connect(&ssocket, "129.0.0.1", 111); // non-existing interface -> -110 fi_connect: Connection timed out, FI_ETIMEDOUT
    //   netio_run(&ctx.evloop);
    //   REQUIRE(state.ss.connection_refused);
    //   REQUIRE(state.ss.fi_errno == FI_ETIMEDOUT);
    //   REQUIRE(!strcmp(state.ss.fi_message, "fi_connect failed"));
    // }

    SECTION( "known node, non-existing port" ) {
      netio_connect(&ssocket, test_iface_addr(), 111); // non-existing port
      netio_run(&ctx.evloop);
      REQUIRE(state.ss.connection_refused);
      REQUIRE(state.ss.fi_errno == 0);
      REQUIRE(state.ss.fi_message == NULL);
    }
  }
}

} // anonymous namespace
