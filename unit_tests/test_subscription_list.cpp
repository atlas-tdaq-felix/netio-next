#include <catch2/catch_test_macros.hpp>

extern "C" {
#include "netio/netio.h"
}

TEST_CASE( "subscription_list tests", "[subscription_list]" )
{
  struct deferred_subscription* ds = NULL;

  //push three items
  netio_tag_t tags[3] = {0x0, 0x1, 0x3};
  for(unsigned int i=0; i<3; ++i){
    push_back_subscription(&ds, tags[i]);
  }

  //read and pop items in order
  int counter = 0;
  while(ds){
    REQUIRE( ds->tag == tags[counter] );
    pop_subscription(&ds);
    ++counter;
  }

  //try to pop
  REQUIRE( ds == NULL );

  //push two
  push_back_subscription(&ds, 0x5);
  push_back_subscription(&ds, 0x7);
  REQUIRE( ds->tag == 0x5 );
  pop_subscription(&ds);
  REQUIRE( ds->tag == 0x7 );
  push_back_subscription(&ds, 0x9);
  REQUIRE( ds->tag == 0x7 );
  pop_subscription(&ds);
  REQUIRE( ds->tag == 0x9 );

  //pop on empty
  pop_subscription(&ds);
  REQUIRE( ds == NULL );
  pop_subscription(&ds);
  REQUIRE( ds == NULL );

}
