#include <catch2/catch_test_macros.hpp>

extern "C" {
#include "netio/netio.h"
}

#include <cstring>
#include "util.h"

namespace test_unbuf_pubsub_flood {

struct State {
  bool initialized = false;
  unsigned num_subscriptions = 0;
  unsigned num_connections = 0;
  unsigned num_msgs_attempted = 0;
  unsigned num_msgs_published = 0;
  unsigned num_msg_received_sub1_tag5 = 0;
  unsigned num_msg_received_sub2_tag5 = 0;
} state;

char pdata[1024];
char sdata1[1024];
char sdata2[1024];

uint64_t key5;

struct netio_context ctx;
struct netio_unbuffered_publish_socket pub;
struct netio_unbuffered_subscribe_socket sub1, sub2;
struct netio_buffer pbuf, sbuf1, sbuf2;
struct netio_signal signal_try_again;

void on_msg_received(struct netio_unbuffered_subscribe_socket* socket, netio_tag_t tag, void* data, size_t size);

void on_init(void *data) {
  state.initialized = true;
  unsigned port = port_of_listen_socket(&pub.lsocket);
  REQUIRE(port > 0);

  // sbuf1.data = sdata1;
  // sbuf1.size = 1024;
  netio_unbuffered_subscribe_socket_init(&sub1, &ctx, test_iface_addr(), test_iface_addr(), port, 1024, 1);
  //sub1.recv_socket.recv_sockets. buffers->data = sdata1;
  sub1.cb_msg_received = on_msg_received;

  // sbuf2.data = sdata2;
  // sbuf2.size = 1024;
  netio_unbuffered_subscribe_socket_init(&sub2, &ctx, test_iface_addr(), test_iface_addr(), port, 1024, 1);
  //sub2.buffers->data = sdata2;
  sub2.cb_msg_received = on_msg_received;

  netio_unbuffered_subscribe(&sub1, 5);
  netio_unbuffered_subscribe(&sub2, 5);
}

void on_subscribe(struct netio_unbuffered_publish_socket* socket, netio_tag_t tag, void* addr, size_t addrlen)
{
  state.num_subscriptions++;
}

void on_connection_established(struct netio_unbuffered_publish_socket* socket)
{
  state.num_connections++;
  if(state.num_subscriptions == 2 && state.num_connections == 2) {
    memcpy(pdata, "foobar", 7);
    struct iovec iov;
    iov.iov_base = pdata;
    iov.iov_len = 7;

    int ret;
    do {
      key5 = 0xDEADBEEF;
      ret = netio_unbuffered_publishv(
                              &pub,       // publish socket
                              5,          // tag
                              &iov,       // iov
                              1,          // iov count
                              &key5,      // key
                              0,          // flags
                              NULL);      // subscription cache;
      INFO("flood #" << state.num_msgs_attempted);
      state.num_msgs_attempted++;
    } while(ret == NETIO_STATUS_OK);

    REQUIRE((ret == NETIO_STATUS_PARTIAL || ret == NETIO_STATUS_AGAIN));
    netio_signal_fire(&signal_try_again);
  }
}


void try_again(void* p) {
  struct iovec iov;
  iov.iov_base = pdata;
  iov.iov_len = 7;

  INFO("trying again - completions available=" << pub.completion_stack.available_objects);
  int ret = netio_unbuffered_publishv(
                              &pub,            // publish socket
                              5,               // tag
                              &iov,            // iov
                              1,               // iov count
                              &key5,           // key
                              0,               // flags
                              NULL);           // subscription cache;
  if(ret == NETIO_STATUS_AGAIN || ret == NETIO_STATUS_PARTIAL) {
    INFO("nope");
    netio_signal_fire(&signal_try_again);
  }
}

void on_msg_published(struct netio_unbuffered_publish_socket*, uint64_t key)
{
  CAPTURE( state.num_msgs_attempted );
  CAPTURE( state.num_msgs_published );
  REQUIRE( key == 0xDEADBEEF );
  state.num_msgs_published++;
}

void on_msg_received(struct netio_unbuffered_subscribe_socket* socket, netio_tag_t tag, void* data, size_t size)
{
  if(tag == 5) {
    if(socket == &sub1) {
      state.num_msg_received_sub1_tag5 ++;
    } else {
      state.num_msg_received_sub2_tag5 ++;
    }
  } else {
    REQUIRE(false);
  }
  if(state.num_msg_received_sub1_tag5 == state.num_msgs_attempted
     && state.num_msg_received_sub2_tag5 == state.num_msgs_attempted) {
    netio_terminate(&ctx.evloop);
  }
}

TEST_CASE( "flood an unbuf publish socket", "[unbuf_pubsub][!mayfail]" ) {
  state = State();
  netio_init(&ctx);
  ctx.evloop.cb_init = on_init;

  pbuf.data = pdata;
  pbuf.size = 1024;
  netio_unbuffered_publish_socket_init(&pub, &ctx, test_iface_addr(), 0, &pbuf);
  pub.cb_subscribe = on_subscribe;
  pub.cb_connection_established = on_connection_established;
  pub.cb_msg_published = on_msg_published;

  netio_signal_init(&ctx.evloop, &signal_try_again);
  signal_try_again.cb = try_again;

  netio_run(&ctx.evloop);
  REQUIRE( state.num_subscriptions == 2 );

  REQUIRE( state.num_msg_received_sub1_tag5 == state.num_msgs_attempted );
  REQUIRE( state.num_msg_received_sub2_tag5 == state.num_msgs_attempted );
}

}
