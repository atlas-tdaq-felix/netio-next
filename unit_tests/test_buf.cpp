#include <catch2/catch_test_macros.hpp>

extern "C" {
#include "netio/netio.h"
}

#include <cstring>

#include "util.h"

namespace {


struct netio_context ctx;
struct netio_buffered_send_socket ssocket;
struct netio_buffered_listen_socket lsocket;
struct netio_buffered_socket_attr attr;

struct State {
  bool initialized = false;
  struct {
    bool connection_established = false;
    bool connection_closed = false;
    bool connection_refused = false;
    bool msg_handled = false;
    int fi_errno = 0;
    char* fi_message = NULL;
  } ss, ls;
} state;


void on_init(void* data)
{
  state.initialized = true;
}


namespace connect_test {
  void on_ss_connection_established(struct netio_buffered_send_socket*)
  {
    state.ss.connection_established = true;
    if(state.ls.connection_established && state.ss.connection_established) {
      netio_terminate(&ctx.evloop);
    }
  }

  void on_ss_connection_closed(struct netio_buffered_send_socket*)
  {
    state.ss.connection_closed = true;
  }

  void on_ls_connection_established(struct netio_buffered_recv_socket*)
  {
    state.ls.connection_established = true;
    if(state.ls.connection_established && state.ss.connection_established) {
      netio_terminate(&ctx.evloop);
    }
  }

  void on_ls_connection_closed(struct netio_buffered_recv_socket*)
  {
    state.ls.connection_closed = true;
  }

  TEST_CASE( "connect a buffered socket", "[buf]" )
  {
    state = State();

    netio_init(&ctx);
    ctx.evloop.cb_init = on_init;

    attr.num_pages = 256;
    attr.pagesize = 64*1024;
    attr.watermark = 63*1024;

    netio_buffered_send_socket_init(&ssocket, &ctx, &attr);
    ssocket.cb_connection_established = on_ss_connection_established;
    ssocket.cb_connection_closed = on_ss_connection_closed;

    netio_buffered_listen_socket_init(&lsocket, &ctx, &attr);
    lsocket.cb_connection_established = on_ls_connection_established;
    lsocket.cb_connection_closed = on_ls_connection_closed;

    netio_buffered_listen(&lsocket,  test_iface_addr(), 0);
    netio_buffered_connect(&ssocket, test_iface_addr(), port_of_buffered_listen_socket(&lsocket));

    netio_run(&ctx.evloop);

    REQUIRE(state.initialized == true);
    REQUIRE(state.ss.connection_established == true);
    REQUIRE(state.ls.connection_established == true);
  }
}


namespace connection_error_test {

  void on_error_connection_refused(struct netio_buffered_send_socket* socket) {
    state.ss.connection_refused = true;
    state.ss.fi_errno = socket->send_socket.fi_errno;
    state.ss.fi_message = (socket->send_socket.fi_message) ? strdup(socket->send_socket.fi_message) : NULL;
    netio_terminate(&ctx.evloop);
  }

  TEST_CASE( "connect buffered socket to non-existing remote", "[buf]" )
  {
    state = State();

    netio_init(&ctx);
    ctx.evloop.cb_init = on_init;

    attr.num_pages = 256;
    attr.pagesize = 64*1024;
    attr.watermark = 63*1024;
    netio_buffered_send_socket_init(&ssocket, &ctx, &attr);
    ssocket.cb_error_connection_refused = on_error_connection_refused;

    SECTION( "buffered non-existing node" ) {
      netio_buffered_connect(&ssocket, "unknown", 111); // non-existing node -> -61 getinfo: No data available, FI_ENODATA
      netio_run(&ctx.evloop);
      REQUIRE(state.ss.connection_refused);
      REQUIRE(state.ss.fi_errno == FI_ENODATA);
      REQUIRE(!strcmp(state.ss.fi_message, "fi_getinfo failed"));
    }

    SECTION( "buffered non-existing port" ) {
      netio_buffered_connect(&ssocket, "www.cern.ch", 111); // non-existing port -> -111 fi_connect: Connection refused, FI_ECONNREFUSED
      netio_run(&ctx.evloop);
      REQUIRE(state.ss.connection_refused);
      REQUIRE(state.ss.fi_errno == FI_ECONNREFUSED);
      REQUIRE(!strcmp(state.ss.fi_message, "fi_connect failed"));
    }

    // SECTION( "buffered non-existing interface" ) {
    //   netio_buffered_connect(&ssocket, "129.0.0.1", 111); // non-existing interface -> -110 fi_connect: Connection timed out, FI_ETIMEDOUT
    //   netio_run(&ctx.evloop);
    //   REQUIRE(state.ss.connection_refused);
    //   REQUIRE(state.ss.fi_errno == FI_ETIMEDOUT);
    //   REQUIRE(!strcmp(state.ss.fi_message, "fi_connect failed"));
    // }

    SECTION( "buffered known node, non-existing port" ) {
      netio_buffered_connect(&ssocket, test_iface_addr(), 111); // non-existing port
      netio_run(&ctx.evloop);
      REQUIRE(state.ss.connection_refused);
      REQUIRE(state.ss.fi_errno == 0);
      REQUIRE(state.ss.fi_message == NULL);
    }
  }
}


}
