#include <catch2/catch_test_macros.hpp>

extern "C" {
#include "netio/netio.h"
}

static bool triggered;
static void trigger(void* data)
{
    triggered = true;
    REQUIRE((uint64_t)data == 0xABCDE);
}

TEST_CASE("test semaphore", "[semaphore]")
{
    triggered = false;

    struct netio_semaphore sem;
    netio_semaphore_init(&sem, 10);
    sem.cb = trigger;
    sem.data = (void*)0xABCDE;

    SECTION("do not trigger") {
      netio_semaphore_increment(&sem, 1);
      REQUIRE(triggered == false);
    }

    SECTION("trigger") {
      netio_semaphore_increment(&sem, 10);
      REQUIRE(triggered == true);
    }

    SECTION("trigger on not exact increments") {
      netio_semaphore_increment(&sem, 20);
      REQUIRE(triggered == true);
    }

    SECTION("do not trigger when threshold is zero") {
      netio_semphore_set_threshold(&sem, 0);
      netio_semaphore_increment(&sem, 20);
      REQUIRE(triggered == false);
    }

    SECTION("trigger when threshold adjusted") {
      netio_semaphore_increment(&sem, 5);
      netio_semphore_set_threshold(&sem, 5);
      REQUIRE(triggered == true);
    }

    SECTION("do not trigger twice") {
      netio_semaphore_increment(&sem, 10);
      REQUIRE(triggered == true);
      triggered = false;
      netio_semaphore_increment(&sem, 5);
      REQUIRE(triggered == false);
    }

}
