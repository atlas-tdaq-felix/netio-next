#include <catch2/catch_test_macros.hpp>

extern "C" {
#include "netio/netio.h"
}


TEST_CASE( "subscription_list setup", "[subscription_list]" )
{
    struct netio_socket_list* head = NULL;
    std::vector<uintptr_t> sockets;
    int ret = -1;

    SECTION( "Check 0: table is empty" ) {
        ret = is_socket_list_empty(head);
        REQUIRE( ret == 1 );
    }

    SECTION( "Check 1: enter some sockets with address and find one" ) {
        for(uint64_t i = 0; i < 32; ++i){
            uint64_t addr[1];
            addr[0] = i; size_t addrlen = sizeof(uint64_t);
            struct netio_socket_list* newitem = add_socket_with_address(&head, TESTSOCKET, addr, addrlen, 0);
            sockets.push_back(reinterpret_cast<uintptr_t>(newitem->socket));
            int check = memcmp(newitem->addr, addr, addrlen);
            REQUIRE( check == 0 );
        }
        ret = print_sockets(head);
        REQUIRE( ret == 32 );

        //find entry using socket pointer
        uintptr_t findme = sockets.at(7);
        struct netio_socket_list* item = find_socket(head, reinterpret_cast<void*>(findme));
        REQUIRE( (uintptr_t)item->socket == findme );

        //find entry using address
        uint64_t addr[1] = {7};
        size_t addrlen = sizeof(uint64_t);
        item = find_socket_by_address(head, (void*)addr, addrlen, 0);
        REQUIRE( item != nullptr );
        int cmp = 1;
        if (item != nullptr){
            cmp = memcmp(item->addr, addr, addrlen);
        }
        REQUIRE( cmp == 0 );
        free_socket_list(&head);
    }

    SECTION( "Check 2: enter sockets with same address" ) {
        uint64_t addr[1] = {55};
        size_t addrlen = sizeof(uint64_t);

        for(uint64_t i = 0; i < 4; ++i){
            add_socket_with_address(&head, TESTSOCKET, addr, addrlen, 0);
        }

        ret = print_sockets(head);
        REQUIRE( ret == 1 );
        free_socket_list(&head);
    }

    SECTION( "Check 3: add sockets and remove them" ) {
        for(uintptr_t i = 0; i < 32; ++i){
            struct netio_socket_list* newitem = add_socket(&head, TESTSOCKET);
            sockets.push_back(reinterpret_cast<uintptr_t>(newitem->socket));
        }
        for(uintptr_t i = 0; i < 32; ++i){
            remove_socket(&head, reinterpret_cast<void*>(sockets.at(i)));
        }
        ret = print_sockets(head);
        REQUIRE( ret == 0 );
        ret = is_socket_list_empty(head);
        REQUIRE( ret == 1 );
        free_socket_list(&head);
    }

    SECTION( "Check 4: remove non-existing items" ) {
        //add three items
        struct netio_socket_list* newitem = add_socket(&head, TESTSOCKET);
        sockets.push_back(reinterpret_cast<uintptr_t>(newitem->socket));
        newitem = add_socket(&head, TESTSOCKET);
        sockets.push_back(reinterpret_cast<uintptr_t>(newitem->socket));
        newitem = add_socket(&head, TESTSOCKET);
        sockets.push_back(reinterpret_cast<uintptr_t>(newitem->socket));

        //remove one socket twice
        remove_socket(&head, reinterpret_cast<void*>(sockets.at(0)));
        ret = print_sockets(head);
        REQUIRE( ret == 2 );
        remove_socket(&head, reinterpret_cast<void*>(sockets.at(0)));
        ret = print_sockets(head);
        REQUIRE( ret == 2 );

        //remove non-existing item
        ret = remove_socket(&head, (void*)0x00007);
        REQUIRE( ret == 1 );

        //check there are still 3 items
        ret = print_sockets(head);
        REQUIRE( ret == 2 );
        free_socket_list(&head);
    }

    SECTION( "Check 5: free entire list" ) {
        for(uint64_t i = 0; i < 32; ++i){
            void* addr = (void*)&i;
            size_t addrlen = sizeof(uint64_t);
            add_socket_with_address(&head, TESTSOCKET, &i, addrlen, 0);
        }
        int deleted = free_socket_list(&head);
        REQUIRE( deleted == 32 );
        ret = is_socket_list_empty(head);
        REQUIRE( ret == 1 );
    }

}
