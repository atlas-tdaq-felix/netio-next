#include <catch2/catch_test_macros.hpp>

extern "C" {
#include "netio/netio.h"
}

#include <cstring>
#include "util.h"

namespace unbuf_simple {

struct State {
  bool initialized = false;
  unsigned num_subscriptions = 0;
  unsigned num_connections = 0;
  unsigned num_msgs_published = 0;
  unsigned num_msg_received_tag5 = 0;
  unsigned num_msg_received_tag7 = 0;
} state;

char pdata[1024];
char sdata1[1024];
char sdata2[1024];

uint64_t key5, key7;

struct netio_context ctx;
struct netio_unbuffered_publish_socket pub;
struct netio_unbuffered_subscribe_socket sub1, sub2;
struct netio_buffer pbuf, sbuf1, sbuf2;

void on_msg_received(struct netio_unbuffered_subscribe_socket* socket, netio_tag_t tag, void* data, size_t size);

void on_init(void *data) {
  state.initialized = true;
  unsigned port = port_of_listen_socket(&pub.lsocket);
  REQUIRE(port > 0);

  netio_unbuffered_subscribe_socket_init(&sub1, &ctx, test_iface_addr(), test_iface_addr(), port, 1024, 1);
  //sub1.buffers->data = sdata1;
  sub1.cb_msg_received = on_msg_received;

  netio_unbuffered_subscribe_socket_init(&sub2, &ctx, test_iface_addr(), test_iface_addr(), port, 1024, 1);
  //sub2.buffers->data = sdata2;
  sub2.cb_msg_received = on_msg_received;

  netio_unbuffered_subscribe(&sub1, 5);
  netio_unbuffered_subscribe(&sub2, 5);
  netio_unbuffered_subscribe(&sub2, 7);
}

void on_subscribe(struct netio_unbuffered_publish_socket* socket, netio_tag_t tag, void* addr, size_t addrlen)
{
  state.num_subscriptions++;
}

void on_connection_established(struct netio_unbuffered_publish_socket* socket)
{
  printf("ON CONNECTION ESTABLISHED\n");
  state.num_connections++;
  if(state.num_subscriptions == 3 && state.num_connections == 2) {
    memcpy(pdata, "foobar", 7);
    struct iovec iov;
    iov.iov_base = pdata;
    iov.iov_len = 7;

    key5 = 0xDEADBEEF;
    REQUIRE( NETIO_STATUS_OK == netio_unbuffered_publishv(
                              &pub,       // publish socket
                              5,          // tag
                              &iov,       // iov
                              1,          // iov count
                              &key5,      // key
                              0,          // flags
                              NULL));     // subscription cache

    key7 = 0xACDCACDC;
    REQUIRE( NETIO_STATUS_OK == netio_unbuffered_publishv(
                              &pub,       // publish socket
                              7,          // tag
                              &iov,       // iov
                              1,          // iov count
                              &key7,      // key
                              0,          // flags
                              NULL));      // subscription cache

    key5 = 0xABCDABCD;
    REQUIRE( NETIO_STATUS_OK == netio_unbuffered_publishv(
                              &pub,       // publish socket
                              5,          // tag
                              &iov,       // iov
                              1,          // iov count
                              &key5,      // key
                              0,          // flags
                              NULL));      // subscription cache
  }
}

void terminate_loop() {
  if ((state.num_msgs_published > 2) && (state.num_msg_received_tag5 + state.num_msg_received_tag7 >= 5)) {
    netio_terminate(&ctx.evloop);
  }
}

void on_msg_published(struct netio_unbuffered_publish_socket*, uint64_t key)
{
  REQUIRE( (key == 0xDEADBEEF || key == 0xACDCACDC || key == 0xABCDABCD) );
  state.num_msgs_published++;
  terminate_loop();
}

void on_msg_received(struct netio_unbuffered_subscribe_socket* socket, netio_tag_t tag, void* data, size_t size)
{
  REQUIRE( size == 7 );

  if(tag == 5) {
    state.num_msg_received_tag5 ++;
  } else if(tag == 7) {
    state.num_msg_received_tag7 ++;
  } else {
    REQUIRE(false);
  }
  terminate_loop();
}

TEST_CASE( "subscribe to an unbuf publish socket", "[unbuf_pubsub]" ) {
  state = State();
  netio_init(&ctx);
  ctx.evloop.cb_init = on_init;

  pbuf.data = pdata;
  pbuf.size = 1024;
  netio_unbuffered_publish_socket_init(&pub, &ctx, test_iface_addr(), 0, &pbuf);
  pub.cb_subscribe = on_subscribe;
  pub.cb_connection_established = on_connection_established;
  pub.cb_msg_published = on_msg_published;

  netio_run(&ctx.evloop);
  REQUIRE( state.num_subscriptions == 3 );
  REQUIRE( state.num_msgs_published == 3 );
  REQUIRE( state.num_msg_received_tag5 == 4 );
  REQUIRE( state.num_msg_received_tag7 == 1 );
}

}


namespace unbuf_big_tag {

unsigned long TAG = 1UL << 42;

struct State {
  bool initialized = false;
  unsigned num_subscriptions = 0;
  unsigned num_connections = 0;
  unsigned num_msgs_published = 0;
  unsigned num_msg_received = 0;
} state;

char pdata[1024];
char sdata1[1024];

uint64_t key;

struct netio_context ctx;
struct netio_unbuffered_publish_socket pub;
struct netio_unbuffered_subscribe_socket sub;
struct netio_buffer pbuf, sbuf;

void on_msg_received(struct netio_unbuffered_subscribe_socket* socket, netio_tag_t tag, void* data, size_t size);

void on_init(void* data) {
  state.initialized = true;
  unsigned port = port_of_listen_socket(&pub.lsocket);
  REQUIRE(port > 0);

  netio_unbuffered_subscribe_socket_init(&sub, &ctx, test_iface_addr(), test_iface_addr(), port, 1024, 1);
  //sub.buffers->data = sdata1;
  sub.cb_msg_received = on_msg_received;

  netio_unbuffered_subscribe(&sub, TAG);
}

void on_subscribe(struct netio_unbuffered_publish_socket* socket, netio_tag_t tag, void* addr, size_t addrlen)
{
  state.num_subscriptions++;
}

void on_connection_established(struct netio_unbuffered_publish_socket* socket)
{
  state.num_connections++;
  memcpy(pdata, "foobar", 7);
  struct iovec iov;
  iov.iov_base = pdata;
  iov.iov_len = 7;

  key = 0xDEADBEEF;
  REQUIRE( NETIO_STATUS_OK == netio_unbuffered_publishv_usr(
                              &pub,       // publish socket
                              TAG,        // tag
                              &iov,       // iov
                              1,          // iov count
                              &key,       // key
                              0,          // flags
                              NULL,       // subscription cache
                              0xAB,       // usr header
                              1));        // usr header size
}

void on_msg_published(struct netio_unbuffered_publish_socket*, uint64_t key)
{
  REQUIRE( key == 0xDEADBEEF );
  state.num_msgs_published++;
}

void on_msg_received(struct netio_unbuffered_subscribe_socket* socket, netio_tag_t tag, void* data, size_t size)
{
  REQUIRE( size == 8 );
  REQUIRE( tag == TAG );
  REQUIRE( ((uint8_t*)(data))[0] == 0xAB );
  REQUIRE( memcmp(data+1, "foobar", 7) == 0 );
  state.num_msg_received ++;
  netio_terminate(&ctx.evloop);
}

TEST_CASE( "unbuf publish with a 64-bit tag", "[unbuf_pubsub]" ) {
  state = State();
  netio_init(&ctx);
  ctx.evloop.cb_init = on_init;

  pbuf.data = pdata;
  pbuf.size = 1024;
  netio_unbuffered_publish_socket_init(&pub, &ctx, test_iface_addr(), 0, &pbuf);
  pub.cb_subscribe = on_subscribe;
  pub.cb_connection_established = on_connection_established;
  pub.cb_msg_published = on_msg_published;

  netio_run(&ctx.evloop);
  REQUIRE( state.num_subscriptions == 1 );
  REQUIRE( state.num_msg_received == 1 );
}



}


namespace pubsub_unbuffered_connection_error_test {
  unsigned long TAG = 1UL << 42;

  struct netio_context ctx;
  struct netio_unbuffered_subscribe_socket sub;

  struct State {
    bool initialized = false;
    bool connection_refused = false;
    int fi_errno = 0;
    char* fi_message = NULL;
  } state;

  void on_init(void* data) {
    state.initialized = true;
  }

  void on_error_connection_refused(struct netio_unbuffered_subscribe_socket* socket) {
    state.connection_refused = true;
    state.fi_errno = socket->socket.fi_errno;
    state.fi_message = (socket->socket.fi_message) ? strdup(socket->socket.fi_message) : NULL;
    netio_terminate(&ctx.evloop);
  }

  TEST_CASE( "subscribe unbuffered to non-existing remote", "[unbuf_pubsub]" ) {
    state = State();

    netio_init(&ctx);
    ctx.evloop.cb_init = on_init;

    SECTION( "subscribe unbuffered non-existing node" ) {
      netio_unbuffered_subscribe_socket_init(&sub, &ctx, test_iface_addr(), "unknown", 111, 1024, 0);  // non-existing node -> -61 getinfo: No data available, FI_ENODATA
      sub.cb_error_connection_refused = on_error_connection_refused;

      netio_unbuffered_subscribe(&sub, TAG);
      netio_run(&ctx.evloop);
      REQUIRE(state.connection_refused);
      REQUIRE(state.fi_errno == 61);
      REQUIRE(!strcmp(state.fi_message, "fi_getinfo failed"));
    }

    SECTION( "subscribe unbuffered non-existing port" ) {
      netio_unbuffered_subscribe_socket_init(&sub, &ctx, test_iface_addr(), "www.cern.ch", 111, 1024, 0); // non-existing port -> -111 fi_connect: Connection refused, FI_ECONNREFUSED
      sub.cb_error_connection_refused = on_error_connection_refused;

      netio_unbuffered_subscribe(&sub, TAG);
      netio_run(&ctx.evloop);
      REQUIRE(state.connection_refused);
      REQUIRE(state.fi_errno == 111);
      REQUIRE(!strcmp(state.fi_message, "fi_connect failed"));
    }

    // SECTION( "subscribe unbuffered non-existing interface" ) {
    //   netio_unbuffered_subscribe_socket_init(&sub, &ctx, test_iface_addr(), "129.0.0.1", 111, NULL, 0); // non-existing interface -> -110 fi_connect: Connection timed out, FI_ETIMEDOUT
    //   sub.cb_error_connection_refused = on_error_connection_refused;

    //   netio_unbuffered_subscribe(&sub, TAG);
    //   netio_run(&ctx.evloop);
    //   REQUIRE(state.connection_refused);
    //   REQUIRE(state.fi_errno == 110);
    //   REQUIRE(!strcmp(state.fi_message, "fi_connect failed"));
    // }

    SECTION( "subscribe unbuffered known node, non-existing port" ) {
      netio_unbuffered_subscribe_socket_init(&sub, &ctx, test_iface_addr(), test_iface_addr(), 111, 1024, 0); // non-existing port
      sub.cb_error_connection_refused = on_error_connection_refused;

      netio_unbuffered_subscribe(&sub, TAG);
      netio_run(&ctx.evloop);
      REQUIRE(state.connection_refused);
      REQUIRE(state.fi_errno == 0);
      REQUIRE(state.fi_message == NULL);
    }
  }
}


namespace pubsub_unbuffered_bind_error_test {

  struct netio_context ctx;
  struct netio_unbuffered_publish_socket pub;
  struct netio_buffer pbuf;

  struct State {
    bool initialized = false;
    bool bind_refused = false;
    int fi_errno = 0;
    char* fi_message = NULL;
  } state;

  void on_init(void* data) {
    state.initialized = true;
  }

  void on_error_bind_refused(struct netio_listen_socket* socket) {
    state.bind_refused = true;
    state.fi_errno = socket->fi_errno;
    state.fi_message = (socket->fi_message) ? strdup(socket->fi_message) : NULL;
    netio_terminate(&ctx.evloop);
  }

  TEST_CASE( "unbuf publish bind error", "[unbuf_pubsub]" ) {
    state = State();
    netio_init(&ctx);
    ctx.evloop.cb_init = on_init;

    SECTION( "non-existing node" ) {
      netio_unbuffered_publish_socket_init(&pub, &ctx, "unknown", 80, &pbuf);
      pub.lsocket.cb_error_bind_refused = on_error_bind_refused;
      netio_run(&ctx.evloop);
      REQUIRE(state.bind_refused);
      REQUIRE(state.fi_errno == FI_ENODATA);
      REQUIRE(!strcmp(state.fi_message, "fi_getinfo failed"));
    }

    SECTION( "non-existing port" ) {
      netio_unbuffered_publish_socket_init(&pub, &ctx, "www.cern.ch", 0, &pbuf);
      pub.lsocket.cb_error_bind_refused = on_error_bind_refused;
      netio_run(&ctx.evloop);
      REQUIRE(state.bind_refused);
      REQUIRE(state.fi_errno == FI_EINVAL);
      REQUIRE(!strcmp(state.fi_message, "fi_listen failed"));
    }

    SECTION( "known node, non-existing port" ) {
      netio_unbuffered_publish_socket_init(&pub, &ctx, test_iface_addr(), 80, &pbuf);
      pub.lsocket.cb_error_bind_refused = on_error_bind_refused;
      netio_run(&ctx.evloop);
      REQUIRE(state.bind_refused);
      REQUIRE(state.fi_errno == FI_EINVAL);
      REQUIRE(!strcmp(state.fi_message, "fi_listen failed"));
    }
  }
}
