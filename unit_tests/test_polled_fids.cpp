#include <catch2/catch_test_macros.hpp>

extern "C" {
#include "netio/netio.h"
}

struct polled_fids testpfids;
struct fid_fabric testfab;
struct fid testfid;
struct netio_send_socket testsocket;
void* testsocket_ptr = (void*)(&testsocket);

void callback(int fd, void* ptr){
    return;
}


TEST_CASE("test polled fids", "[polledfids]")
{

    init_polled_fids(&testpfids, 8);

    SECTION("Add fids") {
        add_polled_fid(&testpfids, &testfab, &testfid, 0, testsocket_ptr, &callback);
        printf("Added one\n");
        add_polled_fid(&testpfids, &testfab, &testfid, 1, testsocket_ptr, &callback);
        add_polled_fid(&testpfids, &testfab, &testfid, 2, testsocket_ptr, &callback);
        REQUIRE(testpfids.count == 3);
    }

    SECTION("Add fids, remove one") {
        add_polled_fid(&testpfids, &testfab, &testfid, 99, testsocket_ptr, &callback);
        add_polled_fid(&testpfids, &testfab, &testfid, 1, testsocket_ptr, &callback);
        add_polled_fid(&testpfids, &testfab, &testfid, 2, testsocket_ptr, &callback);
        remove_polled_fid(&testpfids, 1);
        REQUIRE(testpfids.count == 2);
        REQUIRE(testpfids.data[0].fd == 99);
        REQUIRE(testpfids.data[1].fd == 2);
    }

    SECTION("Add fids, remove all") {
        add_polled_fid(&testpfids, &testfab, &testfid, 1, testsocket_ptr, &callback);
        add_polled_fid(&testpfids, &testfab, &testfid, 2, testsocket_ptr, &callback);
        add_polled_fid(&testpfids, &testfab, &testfid, 3, testsocket_ptr, &callback);
        remove_polled_fid(&testpfids, 1);
        remove_polled_fid(&testpfids, 2);
        remove_polled_fid(&testpfids, 3);
        REQUIRE(testpfids.count == 0);
    }

    SECTION("Add fids, remove all add one") {
        add_polled_fid(&testpfids, &testfab, &testfid, 1, testsocket_ptr, &callback);
        add_polled_fid(&testpfids, &testfab, &testfid, 2, testsocket_ptr, &callback);
        add_polled_fid(&testpfids, &testfab, &testfid, 3, testsocket_ptr, &callback);
        remove_polled_fid(&testpfids, 1);
        remove_polled_fid(&testpfids, 2);
        remove_polled_fid(&testpfids, 3);
        add_polled_fid(&testpfids, &testfab, &testfid, 4, testsocket_ptr, &callback);
        REQUIRE(testpfids.data[0].fd == 4);
        REQUIRE(testpfids.count == 1);
    }

    SECTION("Remove one multiple times") {
        add_polled_fid(&testpfids, &testfab, &testfid, 1, testsocket_ptr, &callback);
        add_polled_fid(&testpfids, &testfab, &testfid, 8, testsocket_ptr, &callback);
        remove_polled_fid(&testpfids, 8);
        remove_polled_fid(&testpfids, 8);
        remove_polled_fid(&testpfids, 8);
        REQUIRE(testpfids.count == 1);
    }

    SECTION("Reallocation") {
        add_polled_fid(&testpfids, &testfab, &testfid, 1, testsocket_ptr, &callback);
        add_polled_fid(&testpfids, &testfab, &testfid, 2, testsocket_ptr, &callback);
        add_polled_fid(&testpfids, &testfab, &testfid, 3, testsocket_ptr, &callback);
        add_polled_fid(&testpfids, &testfab, &testfid, 4, testsocket_ptr, &callback);
        add_polled_fid(&testpfids, &testfab, &testfid, 5, testsocket_ptr, &callback);
        add_polled_fid(&testpfids, &testfab, &testfid, 6, testsocket_ptr, &callback);
        add_polled_fid(&testpfids, &testfab, &testfid, 7, testsocket_ptr, &callback);
        add_polled_fid(&testpfids, &testfab, &testfid, 8, testsocket_ptr, &callback);
        add_polled_fid(&testpfids, &testfab, &testfid, 9, testsocket_ptr, &callback);
        add_polled_fid(&testpfids, &testfab, &testfid, 10, testsocket_ptr, &callback);
        add_polled_fid(&testpfids, &testfab, &testfid, 11, testsocket_ptr, &callback);
        add_polled_fid(&testpfids, &testfab, &testfid, 12, testsocket_ptr, &callback);
        REQUIRE(testpfids.count == 12);
        REQUIRE(testpfids.data[0].fd == 1);
        REQUIRE(testpfids.data[11].fd == 12);
    }

}
