#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <signal.h>
#include <sys/types.h>
#include <unistd.h>
#include <valgrind/callgrind.h>

#include "netio/netio.h"
#include "felixtag.h"

struct {
  char* hostname;
  unsigned port;
  netio_tag_t tag;
  unsigned delay;
  unsigned long timeout;
  unsigned long messages;
  unsigned long sent_messages;

  struct netio_context ctx;
  struct netio_publish_socket socket;

  struct netio_timer timer;

  char* data;
  size_t datasize;
} config;

void __gcov_dump(void);

void my_handler(int signum) {
  printf("received signal\n");
  netio_timer_stop(&config.timer);
  netio_terminate(&config.ctx.evloop);
#ifdef FELIX_COVERAGE
  __gcov_dump(); /* dump coverage data on receiving SIGINT */
#endif
}

void on_subscribe(struct netio_publish_socket* socket, netio_tag_t tag, void* addr, size_t addrlen)
{
  CALLGRIND_START_INSTRUMENTATION;
  printf("Remote subscribed to tag 0x%lx\n", tag);
}


void on_connection_established(struct netio_publish_socket* socket)
{
  printf("connection to subscriber established\n");
  printf("Starting to publish every %u milliseconds\n", config.delay);
  netio_timer_start_ms(&config.timer, config.delay);
}


void on_connection_closed(struct netio_publish_socket* socket)
{
  printf("connection to subscriber closed\n");
}


void on_timer(void* ptr)
{
  config.data[0] = (char)(config.sent_messages+1);
  netio_buffered_publish(&config.socket, config.tag, config.data, config.datasize, 0, NULL);
  if(!config.timeout){netio_buffered_publish_flush(&config.socket, 0, NULL);}
  ++config.sent_messages;
  if(config.messages){
    if(config.messages == config.sent_messages){
      printf("%lu messages sent.\n", config.messages);
      netio_timer_stop(&config.timer);
    }
  }
}


void on_init()
{
  struct netio_buffered_socket_attr attr;
  attr.num_pages = 256;
  attr.pagesize = 64*1024;
  attr.watermark = 56*1024;
  attr.timeout_ms =  config.timeout;

  printf("Opening publish socket on %s:%u\n", config.hostname, config.port);
  netio_publish_socket_init(&config.socket, &config.ctx, config.hostname, config.port, &attr);
  config.socket.cb_subscribe = on_subscribe;
  config.socket.cb_connection_established = on_connection_established;
  config.socket.cb_connection_closed = on_connection_closed;
  config.datasize = 40;
  config.data = (char*)malloc(config.datasize);
  for(unsigned i=0; i<config.datasize; i++) {
    config.data[i] = 'x';
  }
}

int main(int argc, char** argv)
{

  if(argc < 5) {
    fprintf(stderr, "Usage: %s <hostname> <port> <tag> <delay ms> [<timeout> <messages>]\n", argv[0]);
    fprintf(stderr, "Version: %s", FELIX_TAG);
    return 1;
  }

  // Add ctrl-c handler
  struct sigaction new_action, old_action;
  new_action.sa_handler = my_handler;
  sigemptyset(&new_action.sa_mask);
  new_action.sa_flags = 0;
  sigaction(SIGINT, NULL, &old_action);
  if (old_action.sa_handler != SIG_IGN) {
    sigaction (SIGINT, &new_action, NULL);
  }

  config.hostname = argv[1];
  config.port = atoi(argv[2]);
  config.tag = strtol(argv[3], NULL, 0);
  config.delay = atoi(argv[4]);
  config.timeout = 0;
  if(argc>5){
    config.timeout = atoi(argv[5]);
    printf("Setting the timeout to %lu\n", config.timeout);
  }
  config.messages = 0;
  config.sent_messages = 0;
  if(argc>6){
    config.messages = atoi(argv[6]);
    printf("Sending %lu\n", config.messages);
  }

  netio_init(&config.ctx);
  config.ctx.evloop.cb_init = on_init;

  netio_timer_init(&config.ctx.evloop, &config.timer);
  config.timer.cb = on_timer;
  netio_run(&config.ctx.evloop);
  CALLGRIND_STOP_INSTRUMENTATION;

  return 0;
}
