#include <stdio.h>
#include <string.h>
#include <math.h>

#include "netio/netio.h"
#include "felixtag.h"
#include <valgrind/callgrind.h>

#define NUM_PAGES (256)
#define WATERMARK (63*1024)
#define PAGESIZE (64*1024)

struct {
  char* hostname;
  unsigned port;

  struct netio_context ctx;
  struct netio_buffered_send_socket socket;
  struct netio_buffered_socket_attr attr;

  struct netio_timer timer;
  const char* data;
  size_t len;
  int counter;
} config;

// Forward declarations
void on_connection_established(struct netio_buffered_send_socket*);
void on_connection_closed(struct netio_buffered_send_socket*);
void on_error_connection_refused(struct netio_buffered_send_socket*);
void on_timer(void* ptr);

// Callbacks
void on_init(void* ptr)
{
  printf("on_init\n");
  config.data = "Helloworld";
  config.len = strlen(config.data);

  config.attr.num_pages = NUM_PAGES;
  config.attr.pagesize = PAGESIZE;
  config.attr.watermark = WATERMARK;
  config.attr.timeout_ms = 1000;

  netio_buffered_send_socket_init(&config.socket, &config.ctx, &config.attr);
  netio_buffered_connect(&config.socket, config.hostname, config.port);
  config.socket.cb_connection_established = on_connection_established;
  config.socket.cb_connection_closed = on_connection_closed;
  config.socket.cb_error_connection_refused = on_error_connection_refused;
  config.timer.cb = on_timer;
}

void on_connection_established(struct netio_buffered_send_socket* socket)
{
  printf("connection established\n");
  for (int i=0; i<1000000; i++) {
    netio_buffered_send(&config.socket, (void*)config.data, config.len);
  }
  netio_buffered_flush(&config.socket);
  netio_timer_start_ms(&config.timer, 500);
}

void on_connection_closed(struct netio_buffered_send_socket* socket) {
  printf("on_connection_closed\n");
}

void on_error_connection_refused(struct netio_buffered_send_socket* socket) {
  printf("on_error_connection_refused\n");
}

void on_timer(void *ptr) {
  if(config.counter == 0){
    CALLGRIND_START_INSTRUMENTATION;
    netio_disconnect(&config.socket.send_socket);
  } else {
    netio_timer_stop(&config.timer);
    netio_terminate(&config.ctx.evloop);
  }
  config.counter++;
}

int main(int argc, char** argv)
{
  if(argc != 3) {
    fprintf(stderr, "Usage: %s <hostname> <port>\n", argv[0]);
    fprintf(stderr, "Version: %s", FELIX_TAG);
    return 1;
  }

  config.hostname = argv[1];
  config.port = atoi(argv[2]);
  config.counter=0;
  netio_init(&config.ctx);
  config.ctx.evloop.cb_init = on_init;
  netio_timer_init(&config.ctx.evloop, &config.timer);
  netio_run(&config.ctx.evloop);
  CALLGRIND_STOP_INSTRUMENTATION;
  return 0;
}
