# BUFFERED PUBLISH/SUBSCERIBE

valgrind --tool=callgrind --dump-instr=yes --compress-pos=no --compress-strings=no --instr-atstart=no --read-inline-info=yes --collect-jumps=yes --callgrind-out-file=/tmp/publish.out ./publish 10.193.176.30 12350 0 10

valgrind --tool=callgrind --dump-instr=yes --compress-pos=no --compress-strings=no --instr-atstart=no --read-inline-info=yes --collect-jumps=yes --callgrind-out-file=/tmp/subscribe.out ./subscribe 10.193.176.30 10.193.176.30 12350 0 0 500


# UNBUFFERED PUBLISH SUBSCRIBE

valgrind --tool=callgrind --dump-instr=yes --compress-pos=no --compress-strings=no --instr-atstart=no --read-inline-info=yes --collect-jumps=yes --callgrind-out-file=/tmp/unbuf_publish.out ./unbuf_publish 10.193.176.30 12350 0 10000

valgrind --tool=callgrind --dump-instr=yes --compress-pos=no --compress-strings=no --instr-atstart=no --read-inline-info=yes --collect-jumps=yes --callgrind-out-file=/tmp/unbuf_subscribe.out ./unbuf_subscribe 10.193.176.30 10.193.176.30 12350 0 0 500
