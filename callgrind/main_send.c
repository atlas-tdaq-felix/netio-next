#include <stdio.h>
#include <string.h>
#include <math.h>
#include <valgrind/callgrind.h>

#include "netio/netio.h"

#include "felixtag.h"

#define NUM_BUFFERS (256)
#define BUFFER_SIZE (64*1024)

#define MIN(X, Y) (((X) < (Y)) ? (X) : (Y))

struct {
  char* hostname;
  unsigned port;

  struct netio_context ctx;
  struct netio_send_socket socket;
  struct netio_buffer buf[NUM_BUFFERS];
} config;

// Forward declarations
void on_connection_established(struct netio_send_socket* socket);
void on_send_completed(struct netio_send_socket* socket, uint64_t key);
void on_connection_closed(struct netio_send_socket* socket);

// Callbacks
void on_init(void* ptr)
{
  netio_init_send_socket(&config.socket, &config.ctx);
  netio_connect(&config.socket, config.hostname, config.port);
  config.socket.cb_connection_established = on_connection_established;
  config.socket.cb_connection_closed = on_connection_closed;
  config.socket.cb_send_completed = on_send_completed;
}

void on_connection_established(struct netio_send_socket* socket)
{
  printf("connection established\n");
  for(unsigned i=0; i<NUM_BUFFERS; i++) {
    config.buf[i].size = BUFFER_SIZE;
    config.buf[i].data = malloc(config.buf[i].size);
    netio_register_send_buffer(socket, &config.buf[i], 0);
    netio_send_buffer(socket, &config.buf[i]);
  }
}

void on_send_completed(struct netio_send_socket* socket, uint64_t key)
{
  CALLGRIND_START_INSTRUMENTATION;
  netio_disconnect(&config.socket);
}

void on_connection_closed(struct netio_send_socket* socket) {
  printf("on_connection_closed\n");
  netio_terminate(&config.ctx.evloop);
}

int main(int argc, char** argv)
{
  if(argc != 3) {
    fprintf(stderr, "Usage: %s <hostname> <port>\n", argv[0]);
    fprintf(stderr, "Version: %s", FELIX_TAG);
    return 1;
  }

  config.hostname = argv[1];
  config.port = atoi(argv[2]);

  netio_init(&config.ctx);
  config.ctx.evloop.cb_init = on_init;
  netio_run(&config.ctx.evloop);
  CALLGRIND_STOP_INSTRUMENTATION; 
  return 0;
}
