#pragma once

#ifdef __cplusplus
extern "C" {
#endif

enum netio_tcp_state {NETIO_TCP_EMPTY, NETIO_TCP_NEW, NETIO_TCP_IN_PROGRESS};
struct netio_tcp_send_item
{
  enum netio_tcp_state     element_active;
  uint32_t                  total_bytes;
  uint32_t                  bytes_left;
  struct netio_send_socket *socket;
  struct netio_buffer      *buffer;
  uint64_t                 key;
  void                     *next_element;                                  //MJ "void *" or "netio_tcp_send_item *"
  //void (*cb_send_completed)(struct netio_send_socket*, uint64_t key);
};


struct netio_tcp_recv_item
{
  enum netio_tcp_state     element_active;
  struct netio_recv_socket *socket;
  struct netio_buffer      *buffer;
  uint32_t                 bytes_received;
  uint32_t                 message_size;
  void                     *next_element;
  //void (*cb_msg_received)(struct netio_recv_socket*, uint64_t key);
};



void make_socket_non_blocking(int sfd);
void netio_tcp_send_on_signal(void *ptr);
void netio_tcp_send_on_signal_test(void *ptr);
void netio_tcp_recv_on_signal(void *ptr);

void netio_init_send_tcp_socket(struct netio_send_socket* socket, struct netio_context* ctx);
void netio_init_listen_tcp_socket(struct netio_listen_socket* socket, struct netio_context* ctx, struct netio_unbuffered_socket_attr* attr);
void netio_init_recv_tcp_socket(struct netio_recv_socket* socket, struct netio_listen_socket* lsocket);

void netio_connect_tcp(struct netio_send_socket* niosocket, const char* hostname, unsigned port);
void netio_listen_tcp(struct netio_listen_socket* niosocket, const char* hostname, unsigned port);


void netio_register_read_tcp_fd(struct netio_eventloop* evloop, struct netio_event_context* ctx);
void netio_register_write_tcp_fd(struct netio_eventloop* evloop, struct netio_event_context* ctx);


void netio_buffered_listen_tcp_socket_init(struct netio_buffered_listen_socket* socket,
                                           struct netio_context* ctx,
                                           struct netio_buffered_socket_attr* attr);


void netio_publish_tcp_socket_init(struct netio_publish_socket* socket,
                                   struct netio_context* ctx,
                                   const char* hostname,
                                   unsigned port,
                                   struct netio_buffered_socket_attr* attr);
void netio_subscribe_tcp_socket_init(struct netio_subscribe_socket* socket,
                                     struct netio_context* ctx,
                                     struct netio_buffered_socket_attr* attr,
                                     const char* hostname,
                                     const char* remote_host,
                                     unsigned remote_port);

void netio_buffered_recv_tcp_socket_init(struct netio_buffered_recv_socket* socket,
                                         struct netio_buffered_listen_socket* lsocket);
void netio_buffered_send_tcp_socket_init(struct netio_buffered_send_socket* socket,
                                         struct netio_context* ctx,
                                         struct netio_buffered_socket_attr* attr);

void netio_buffered_listen_tcp(struct netio_buffered_listen_socket* socket,
                               const char* hostname, unsigned port);
void netio_unbuffered_publish_tcp_socket_init(struct netio_unbuffered_publish_socket* socket,
                                              struct netio_context* ctx,
                                              const char* hostname,
                                              unsigned port,
                                              struct netio_buffer* buf);

void netio_unbuffered_subscribe_tcp_socket_init(struct netio_unbuffered_subscribe_socket* socket,
                                                struct netio_context* ctx,
                                                const char* hostname,
                                                const char* remote_host,
                                                unsigned remote_port,
                                                //struct netio_buffer* buffers,
                                                size_t buffer_size,
                                                size_t count);

int
netio_tcp_sendv_imm(struct netio_send_socket* socket,
                    uint32_t total_size,
                    struct iovec* iov,
                    size_t count,
                    uint64_t key,
                    uint64_t imm);
#ifdef __cplusplus
}
#endif
