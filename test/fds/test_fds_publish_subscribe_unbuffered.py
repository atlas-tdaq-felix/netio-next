#!/usr/bin/env python3

import subprocess
import unittest

from felix_test_case import FelixTestCase


class TestPublishSubscribeUnbufferedFDs(FelixTestCase):

    def setUp(self):
        self.start('fds-unbuffered-' + FelixTestCase.netio_protocol)

    def tearDown(self):
        self.stop('fds-unbuffered-' + FelixTestCase.netio_protocol)

    def test_subscribe_unbuffered_fds(self):
        try:
            timeout = 90
            ip = FelixTestCase.ip
            fid = FelixTestCase.fid
            port = FelixTestCase.port
            iterations = "20"
            subprocess.check_output(' '.join(("./unbuf_subscribe_unsubscribe_loop",
                                              FelixTestCase.netio_protocol + ":" + ip, ip, port, fid, iterations)),
                                    timeout=timeout, stderr=subprocess.STDOUT, shell=True, encoding="UTF-8")
        except subprocess.CalledProcessError as e:
            print(e.returncode)
            print(e.cmd)
            print(e.output)
            self.assertEqual(e.returncode, 42)
        except subprocess.TimeoutExpired as e:
            print(e.cmd)
            print(e.output.decode())
            print("Timeout !")
            self.assertTrue(False)


if __name__ == '__main__':
    unittest.main()
