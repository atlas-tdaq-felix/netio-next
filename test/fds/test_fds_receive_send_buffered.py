#!/usr/bin/env python3

import subprocess
import unittest

from felix_test_case import FelixTestCase


class TestReceiveSendBufferedFDs(FelixTestCase):

    def setUp(self):
        self.start('receive-buffered-fds-' + FelixTestCase.netio_protocol)

    def tearDown(self):
        self.stop('receive-buffered-fds-' + FelixTestCase.netio_protocol)

    def test_receive_send_buffered_fds(self):
        try:
            timeout = 90
            iterations = "100"
            ip = FelixTestCase.ip
            port = FelixTestCase.port
            output = subprocess.check_output(' '.join(("./send_loop_buffered",
                                                       FelixTestCase.netio_protocol + ":" + ip, port, iterations)),
                                             timeout=timeout, stderr=subprocess.STDOUT, shell=True, encoding="UTF-8")
            print(output)
        except subprocess.CalledProcessError as e:
            print(e.returncode)
            print(e.cmd)
            print(e.output)
            self.assertEqual(e.returncode, 0)
        except subprocess.TimeoutExpired as e:
            print(e.cmd)
            print(e.output.decode())
            print("Timeout !")
            self.assertTrue(False)


if __name__ == '__main__':
    unittest.main()
