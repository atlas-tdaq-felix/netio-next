#!/usr/bin/env python3

import subprocess
import unittest

from felix_test_case import FelixTestCase


class TestSendReceiveUnbuffered(FelixTestCase):

    def setUp(self):
        self.start('receive-unbuffered-' + FelixTestCase.netio_protocol)

    def tearDown(self):
        self.stop('receive-unbuffered-' + FelixTestCase.netio_protocol)

    def test_send_receive_unbuffered(self):
        try:
            timeout = 30
            port = FelixTestCase.port
            ip = FelixTestCase.ip
            msgs = "1000"
            subprocess.check_output(' '.join(("./send", FelixTestCase.netio_protocol + ":" + ip, port, msgs)), timeout=timeout, stderr=subprocess.STDOUT, shell=True, encoding='UTF-8')
        except subprocess.CalledProcessError as e:
            print(e.returncode)
            print(e.cmd)
            print(e.output)
            self.assertEqual(e.returncode, 0)


if __name__ == '__main__':
    unittest.main()
