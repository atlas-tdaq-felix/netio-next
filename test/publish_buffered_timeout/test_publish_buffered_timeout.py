#!/usr/bin/env python3

import subprocess
import unittest

from felix_test_case import FelixTestCase


class TestPublishBufferedTimeout(FelixTestCase):

    def setUp(self):
        self.start('publish-timeout-' + FelixTestCase.netio_protocol)

    def tearDown(self):
        self.stop('publish-timeout-' + FelixTestCase.netio_protocol)

    def test_publish_subscribe(self):
        try:
            timeout = 20
            elink_start = FelixTestCase.fid
            elink_end = FelixTestCase.fid
            port = FelixTestCase.port
            msgs = "50"
            ip = FelixTestCase.ip
            subprocess.check_output(' '.join(("./subscribe", FelixTestCase.netio_protocol + ":" + ip, ip, port, elink_start, elink_end, msgs)), timeout=timeout, stderr=subprocess.STDOUT, shell=True, encoding='UTF-8')
        except subprocess.CalledProcessError as e:
            print(e.returncode)
            print(e.cmd)
            print(e.output)
            self.assertEqual(e.returncode, 42)


if __name__ == '__main__':
    unittest.main()
