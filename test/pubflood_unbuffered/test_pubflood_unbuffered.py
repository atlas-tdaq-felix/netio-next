#!/usr/bin/env python3

import subprocess
import unittest

from felix_test_case import FelixTestCase


class TestPubfloodUnbuffered(FelixTestCase):

    def setUp(self):
        self.start('unbuf-pubflood')

    def tearDown(self):
        self.stop('unbuf-pubflood')

    def test_pubflood_subscribe(self):
        try:
            timeout = 20
            elink_start = FelixTestCase.fid
            elink_end = FelixTestCase.fid_10
            port = FelixTestCase.port
            msgs = 200000000
            ip = FelixTestCase.ip
            subprocess.check_output("./unbuf_subscribe " + ip + " " + ip + " " + str(port) + " " + str(elink_start) + " " + str(elink_end) + " " + str(msgs), timeout=timeout, stderr=subprocess.STDOUT, shell=True, encoding='UTF-8')
        except subprocess.CalledProcessError as e:
            print(e.returncode)
            print(e.cmd)
            print(e.output)
            self.assertEqual(e.returncode, 42)


if __name__ == '__main__':
    unittest.main()
