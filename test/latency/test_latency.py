#!/usr/bin/env python3

import subprocess
import unittest

from felix_test_case import FelixTestCase


class TestLatency(FelixTestCase):

    def setUp(self):
        self.start('latency-' + FelixTestCase.netio_protocol)

    def tearDown(self):
        self.stop('latency-' + FelixTestCase.netio_protocol)

    def test_latency(self):
        try:
            timeout = 20
            port = FelixTestCase.port
            alt_port = FelixTestCase.alt_port
            msgs = "10000"
            mean_latency = "20" if FelixTestCase.netio_protocol == 'tcp' else "12"
            ip = FelixTestCase.ip
            subprocess.check_output(' '.join(("./latency", "-c", FelixTestCase.netio_protocol + ":" + ip, "-p", port, "-r", alt_port, "-i", msgs, "-m", mean_latency)), timeout=timeout, stderr=subprocess.STDOUT, shell=True, encoding='UTF-8')
        except subprocess.CalledProcessError as e:
            print(e.returncode)
            print(e.cmd)
            print(e.output)
            self.assertEqual(e.returncode, 0)


if __name__ == '__main__':
    unittest.main()
