#!/usr/bin/env python3

import subprocess
import unittest

from felix_test_case import FelixTestCase


class TestPubfloodMultiBuffered(FelixTestCase):

    def setUp(self):
        self.start('pubflood-multi-' + FelixTestCase.netio_protocol)

    def tearDown(self):
        self.stop('pubflood-multi-' + FelixTestCase.netio_protocol)

    def test_pubflood_multi_subscribemt(self):
        try:
            timeout = 30
            elink_start = FelixTestCase.fid
            threads = "-10"
            port = FelixTestCase.port
            msgs = "1000000"
            ip = FelixTestCase.ip
            subprocess.check_output(' '.join(("./subscribemt", FelixTestCase.netio_protocol + ":" + ip, ip, port, elink_start, threads, msgs)), timeout=timeout, stderr=subprocess.STDOUT, shell=True, encoding='UTF-8')
            self.assertTrue(False)
        except subprocess.CalledProcessError as e:
            print(e.returncode)
            print(e.cmd)
            print(e.output)
            self.assertEqual(e.returncode, 42)


if __name__ == '__main__':
    unittest.main()
