#!/usr/bin/env python3
"""
Netio Timer.

Usage:
    timer.py
"""
import sys

from docopt import docopt  # noqa: E402

from netio_py import ffi, lib

from felixtag import FELIX_TAG


@ffi.def_extern()
def on_init():
    """On Init."""
    print("on_init")


@ffi.def_extern()
def on_timer(ptr):
    """On Timer."""
    print("on_timer")


if __name__ == "__main__":
    """Timer."""
    args = docopt(__doc__, version=sys.argv[0] + " " + FELIX_TAG)

    print("netio_context")
    context = ffi.new("struct netio_context *")

    print("netio_init")
    lib.netio_init(context)

    print("netio_eventloop and cb")
    evloop = ffi.addressof(context.evloop)
    evloop.cb_init = lib.on_init

    print("netio_timer_init and cb")
    timer = ffi.new("struct netio_timer *")
    lib.netio_timer_init(evloop, timer)
    timer.cb = lib.on_timer

    print("netio_timer_start_s")
    lib.netio_timer_start_s(timer, 3)

    print("netio_run")
    lib.netio_run(evloop)

    print("Never Ends")
