#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <unistd.h>
#include "netio/netio.h"

#include "felixtag.h"

struct Config {
  char* local_hostname;
  char* remote_hostname;
  unsigned port;
  netio_tag_t tag;

  struct netio_context ctx;
  struct netio_subscribe_socket socket;

  struct netio_timer subscribe_timer;
  int subrequest_sent;

  uint64_t expected_messages;
};


struct Statistics {
    struct netio_timer timer;
    struct timespec t0;
    uint64_t messages_received;
    uint64_t bytes_received;
    int subscribed;

    uint64_t total_messages_received;
};


struct SubscribeThreadData {
  pthread_t thread_id;
  struct Config* config;
  struct Statistics* stats;
};


void try_subscribe(void* arg)
{
  struct Config* config = ((struct SubscribeThreadData*)arg)->config;
  struct Statistics* stats = ((struct SubscribeThreadData*)arg)->stats;
  if(stats->subscribed == -1){
    netio_terminate(&config->ctx.evloop);
  }
  if (!config->subrequest_sent) {
    printf("attempting to subscribe for 0x%lx to remote on %s:%u\n", config->tag, config->socket.remote_hostname, config->socket.remote_port);
    netio_subscribe(&config->socket, config->tag);
    config->subrequest_sent = 1;
  }
}


void on_connection_closed(struct netio_subscribe_socket* socket)
{
  printf("connection to publisher closed from %s:%u\n", socket->remote_hostname, socket->remote_port);
  struct Config* config = ((struct SubscribeThreadData*)socket->usr)->config;
  struct Statistics* stats = ((struct SubscribeThreadData*)socket->usr)->stats;
  if(config->expected_messages == 0){
    stats->subscribed = 0;
  }
  config->subrequest_sent = 0;
  // try to reconnect periodically
  netio_timer_start_s(&config->subscribe_timer, 1);
}


void on_connection_established(struct netio_subscribe_socket* socket) {
  printf("connection established to %s:%u\n", socket->remote_hostname, socket->remote_port);
  struct Config* config = ((struct SubscribeThreadData*)socket->usr)->config;
  struct Statistics* stats = ((struct SubscribeThreadData*)socket->usr)->stats;
  stats->subscribed = 1;
  config->subrequest_sent = 0;
  netio_timer_stop(&config->subscribe_timer);
}


void on_error_connection_refused(struct netio_subscribe_socket* socket)
{
  printf("connection refused for subscribing from %s:%u\n", socket->remote_hostname, socket->remote_port);
  struct Config* config = ((struct SubscribeThreadData*)socket->usr)->config;
  config->subrequest_sent = 0;
  // retry to connect periodically
  netio_timer_start_s(&config->subscribe_timer, 1);
}


void on_msg_received(struct netio_subscribe_socket* socket, netio_tag_t tag, void* data, size_t size)
{
  // printf("msg tag=%lu size=%lu\n", tag, size);
  struct Config* config = ((struct SubscribeThreadData*)socket->usr)->config;
  struct Statistics* stats = ((struct SubscribeThreadData*)socket->usr)->stats;
  pthread_t thread_id = ((struct SubscribeThreadData*)socket->usr)->thread_id;

  stats->total_messages_received++;
  if ((stats->total_messages_received > config->expected_messages) && (config->expected_messages > 0)) {
    if(stats->subscribed == 1){
      printf("Thread 0x%lx tag 0x%lx: all messages received\n", thread_id, tag);
      stats->subscribed = -1;
      netio_unsubscribe(socket, tag);
      netio_timer_close(&config->ctx.evloop, &stats->timer);
      netio_timer_close(&config->ctx.evloop, &config->subscribe_timer);
      netio_terminate(&config->ctx.evloop);
    }
  }

  stats->messages_received++;
  stats->bytes_received += size;
}


void on_stats(void* arg)
{
  struct Config* config = ((struct SubscribeThreadData*)arg)->config;
  struct Statistics* stats = ((struct SubscribeThreadData*)arg)->stats;
  pthread_t thread_id = ((struct SubscribeThreadData*)arg)->thread_id;

  if (!stats->subscribed) return;

  time_t t = time(NULL);
  struct tm tm = *localtime(&t);
  char timestr[32];
  sprintf(timestr, "%d-%02d-%02d %02d:%02d:%02d", (tm.tm_year + 1900) % 10000, (tm.tm_mon + 1) % 100, tm.tm_mday % 100, tm.tm_hour % 100, tm.tm_min % 100, tm.tm_sec % 100);

  struct timespec t1;
  clock_gettime(CLOCK_MONOTONIC_RAW, &t1);

  double seconds = t1.tv_sec - stats->t0.tv_sec
                   + 1e-9*(t1.tv_nsec - stats->t0.tv_nsec);
  printf("%s thread 0x%lx tag 0x%lx: data rate: %2f Gb/s   message rate: %2f kHz\n",
      timestr,
      thread_id,
      config->tag,
      stats->bytes_received*8/1024./1024./1024./seconds,
      stats->messages_received/1000./seconds);
  stats->bytes_received = 0;
  stats->messages_received = 0;
  stats->t0 = t1;


}


void on_init(void* arg)
{
  struct Config* config = ((struct SubscribeThreadData*)arg)->config;
  struct Statistics* stats = ((struct SubscribeThreadData*)arg)->stats;

  struct netio_buffered_socket_attr attr;
  attr.num_pages = 256;
  attr.pagesize = 64*1024;
  attr.watermark = 56*1024;
  attr.timeout_ms = 1000;

  printf("Opening subscribe socket from %s on %s:%u\n",
         config->local_hostname, config->remote_hostname, config->port);
  netio_subscribe_socket_init(&config->socket, &config->ctx, &attr, config->local_hostname, config->remote_hostname, config->port);

  config->socket.cb_connection_closed = on_connection_closed;
  config->socket.cb_connection_established = on_connection_established;
  config->socket.cb_error_connection_refused = on_error_connection_refused;
  config->socket.cb_msg_received = on_msg_received;
  config->socket.usr = arg;

  netio_timer_init(&config->ctx.evloop, &stats->timer);
  clock_gettime(CLOCK_MONOTONIC_RAW, &stats->t0);
  stats->timer.cb = on_stats;
  stats->timer.data = arg;
  netio_timer_start_s(&stats->timer, 1);

  // init timer but don't start it now
  netio_timer_init(&config->ctx.evloop, &config->subscribe_timer);
  config->subscribe_timer.cb = try_subscribe;
  config->subscribe_timer.data = arg;
  // attempt to subscribe
  try_subscribe(arg);
}


void* subscribe_thread(void* arg)
{
  struct Config* config = ((struct SubscribeThreadData*)arg)->config;
  // struct Statistics* stats = ((struct SubscribeThreadData*)arg)->stats;

  netio_init(&config->ctx);
  config->ctx.evloop.cb_init = on_init;
  config->ctx.evloop.data = arg;
  netio_run(&config->ctx.evloop);

  return NULL;
}


int main(int argc, char** argv)
{

  if(argc < 5 || argc > 7) {
    fprintf(stderr, "Usage: %s <local_hostname> <remote_hostname> <port> <tag> [nb_of_threads!=0] [expected_messages>=0]\n", argv[0]);
    fprintf(stderr, "       nb_of_threads > 0: Different threads will use consecutive ports and tags starting from the given values\n");
    fprintf(stderr, "       nb_of_threads < 0: Different threads will use one port and one tag\n");
    fprintf(stderr, "Version: %s", FELIX_TAG);
    return 1;
  }


  char* local_hostname = argv[1];
  char* remote_hostname = argv[2];
  unsigned port = atoi(argv[3]);
  netio_tag_t tag = strtol(argv[4], NULL, 0);
  int use_fid = (tag & 0x1000000000000000) > 0L;
  fprintf(stderr, "%d 0x%lx\n", use_fid, tag);
  int nb_threads = 1;
  int single_tag = 0;
  if (argc >= 6) {
    nb_threads = atoi(argv[5]);
    if (nb_threads == 0) {
      fprintf(stderr, "nb_threads must be greater or less than 0\n");
      return 2;
    }
    if (nb_threads < 0) {
      nb_threads = abs(nb_threads);
      single_tag = 1;
    }
  }
  unsigned expected_messages = (argc == 7) ? atoi(argv[6]) : 0;

  struct SubscribeThreadData* threads_data = (struct SubscribeThreadData*)malloc(nb_threads * sizeof(struct SubscribeThreadData));
  for(int i = 0; i < nb_threads; ++i) {
    threads_data[i].config = (struct Config*)malloc(sizeof(struct Config));
    threads_data[i].stats = (struct Statistics*)malloc(sizeof(struct Statistics));
    threads_data[i].config->local_hostname = local_hostname;
    threads_data[i].config->remote_hostname = remote_hostname;
    threads_data[i].config->port = port + (single_tag ? 0 : i);
    threads_data[i].config->tag = tag + (single_tag ? 0 : use_fid ? i * 0x100 : i);
    threads_data[i].config->expected_messages = expected_messages;
    int rv = pthread_create(&threads_data[i].thread_id, NULL, subscribe_thread, (void*)(threads_data+i));
    if (rv) {
      fprintf(stderr, "pthread_create error: %d\n", rv);
    }

  }

  for(int i = 0; i < nb_threads; ++i) {
    pthread_join(threads_data[i].thread_id, NULL);
    free(threads_data[i].config);
    free(threads_data[i].stats);
  }
  free(threads_data);

  return 42;
}
