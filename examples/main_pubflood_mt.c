#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <signal.h>
#include <sys/types.h>
#include <unistd.h>
#include <time.h>

#include "netio/netio.h"

#include "felixtag.h"

void __gcov_dump(void);

void my_handler(int signum) {
  printf("received signal\n");
#ifdef FELIX_COVERAGE
  __gcov_dump(); /* dump coverage data on receiving SIGINT */
#endif
}

#define MAX_MESSAGES_PER_FLOOD (100000)

struct Config {
  char* hostname;
  unsigned port;
  netio_tag_t tag;

  struct netio_context ctx;
  struct netio_publish_socket socket;
  struct netio_signal signal;

  char* data;
  size_t datasize;
};

struct Statistics {
  struct netio_timer timer;
  struct timespec t0;
  uint64_t messages_sent;
  uint64_t bytes_sent;
  uint64_t again;
  uint64_t nosub;
};

struct PublishThreadData {
  pthread_t thread_id;
  struct Config* config;
  struct Statistics* stats;
};

void flood(void* arg) {
  struct Config* config = ((struct PublishThreadData*)arg)->config;
  struct Statistics* stats = ((struct PublishThreadData*)arg)->stats;

  for(unsigned i=0; i<MAX_MESSAGES_PER_FLOOD; i++) {
    int ret = netio_buffered_publish(&config->socket, config->tag, config->data, config->datasize, 0, NULL);
    stats->nosub = 0;
    if(ret == NETIO_STATUS_OK) {
      stats->messages_sent += 1;
      stats->bytes_sent += config->datasize;
    } else if (ret == NETIO_STATUS_AGAIN) {
      stats->again += 1;
      netio_signal_fire(&config->signal);
      return;
    } else if (ret == NETIO_STATUS_OK_NOSUB) {
      stats->nosub += 1;
    }
  }
  netio_buffered_publish_flush(&config->socket, 0, NULL);
  netio_signal_fire(&config->signal);
}


void flood_buffer_available(struct netio_publish_socket *socket) {
    flood(socket->usr);
}

void on_stats(void* arg) {
  struct Config* config = ((struct PublishThreadData*)arg)->config;
  struct Statistics* stats = ((struct PublishThreadData*)arg)->stats;

  time_t t = time(NULL);
  struct tm tm = *localtime(&t);
  char timestr[32];
  sprintf(timestr, "%d-%02d-%02d %02d:%02d:%02d", (tm.tm_year + 1900) % 10000, (tm.tm_mon + 1) % 100, tm.tm_mday % 100, tm.tm_hour % 100, tm.tm_min % 100, tm.tm_sec % 100);

  struct timespec t1;
  clock_gettime(CLOCK_MONOTONIC_RAW, &t1);

  if (stats->nosub) {
    printf("%s thread 0x%lx: no subscription\n", timestr, config->tag);
  } else {
    double seconds = t1.tv_sec - stats->t0.tv_sec
                     + 1e-9*(t1.tv_nsec - stats->t0.tv_nsec);
    printf("%s thread 0x%lx: data rate: %2.2f Gb/s  message rate: %2.2f kHz\n",
           timestr,
           config->tag,
           stats->bytes_sent*8/1024./1024./1024./seconds,
           stats->messages_sent/1000./seconds
           );
  }

  stats->bytes_sent = 0;
  stats->messages_sent = 0;
  stats->t0 = t1;
}


void on_init(void* arg)
{
  struct Config* config = ((struct PublishThreadData*)arg)->config;
  struct Statistics* stats = ((struct PublishThreadData*)arg)->stats;

  struct netio_buffered_socket_attr attr;
  attr.num_pages = 256;
  attr.pagesize = 64*1024;
  attr.watermark = 56*1024;
  attr.timeout_ms = 1000;

  printf("Opening publish socket on %s:%u\n", config->hostname, config->port);
  netio_publish_socket_init(&config->socket, &config->ctx, config->hostname, config->port, &attr);
  config->socket.cb_buffer_available = flood_buffer_available;
  config->socket.usr = arg;

  config->datasize = 128;
  config->data = (char*)malloc(config->datasize);
  for(unsigned i=0; i<config->datasize; ++i) {
    config->data[i] = 'x';
  }

  netio_signal_init(&config->ctx.evloop, &config->signal);
  config->signal.cb = flood;
  config->signal.data = arg;
  netio_signal_fire(&config->signal);

  netio_timer_init(&config->ctx.evloop, &stats->timer);
  clock_gettime(CLOCK_MONOTONIC_RAW, &stats->t0);
  stats->timer.cb = on_stats;
  stats->timer.data = arg;
  netio_timer_start_s(&stats->timer, 1);
}

void* publish_thread(void* arg)
{
  struct Config* config = ((struct PublishThreadData*)arg)->config;
  // struct Statistics* stats = ((struct PublishThreadData*)arg)->stats;

  netio_init(&config->ctx);
  config->ctx.evloop.cb_init = on_init;
  config->ctx.evloop.data = arg;
  netio_run(&config->ctx.evloop);

  return NULL;
}

int main(int argc, char** argv)
{
  if(argc < 4 || argc > 5) {
    fprintf(stderr, "Usage: %s <hostname> <port> <tag> [nb_of_threads>=1]\n", argv[0]);
    fprintf(stderr, "       Different threads will use consecutive ports and tags starting from the\n");
    fprintf(stderr, "       given values\n");
    fprintf(stderr, "Version: %s", FELIX_TAG);
    return 1;
  }

  // Add ctrl-c handler
  struct sigaction new_action, old_action;
  new_action.sa_handler = my_handler;
  sigemptyset(&new_action.sa_mask);
  new_action.sa_flags = 0;
  sigaction(SIGINT, NULL, &old_action);
  if (old_action.sa_handler != SIG_IGN) {
    sigaction (SIGINT, &new_action, NULL);
  }

  char* hostname = argv[1];
  unsigned port = atoi(argv[2]);
  netio_tag_t tag = strtol(argv[3], NULL, 0);
  int use_fid = (tag & 0x1000000000000000) > 0L;
  int nb_threads = 1;
  if (argc == 5) {
    nb_threads = atoi(argv[4]);
    if (nb_threads < 1) {
      fprintf(stderr, "nb_threads must be greater than 0\n");
      return 2;
    }
  }

  struct PublishThreadData* threads_data = (struct PublishThreadData*)malloc(nb_threads * sizeof(struct PublishThreadData));
  for(int i = 0; i < nb_threads; ++i) {
    threads_data[i].config = (struct Config*)malloc(sizeof(struct Config));
    threads_data[i].stats = (struct Statistics*)malloc(sizeof(struct Statistics));
    threads_data[i].config->hostname = hostname;
    threads_data[i].config->port = port + i;
    threads_data[i].config->tag = tag + (use_fid ? i * 0x100 : i);
    int rv = pthread_create(&threads_data[i].thread_id, NULL, publish_thread, (void*)(threads_data+i));
    if (rv) {
      fprintf(stderr, "pthread_create error: %d\n", rv);
    }
  }

  for(int i = 0; i < nb_threads; ++i) {
    pthread_join(threads_data[i].thread_id, NULL);
    free(threads_data[i].config);
    free(threads_data[i].stats);
  }
  free(threads_data);

  return 0;
}
