#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <argp.h>
#include <time.h>
#include <sys/uio.h>

#include "netio/netio.h"
#include "netio/netio_tcp.h"

#include "felixtag.h"

#define DEBUG_LOG( ... ) if (config.verbose) do { printf("[netio@%s:%3d] ", __FILE__, __LINE__); printf(__VA_ARGS__); printf("\n"); fflush(stdout); } while(0)
#define HISTOGRAM_SIZE (1000)

const char *argp_program_version = FELIX_TAG;
const char *argp_program_bug_address = "<https://its.cern.ch/jira/projects/FLX>";

/* Program documentation. */
static char doc[] = "Netio-next latency benchmark.\n"
                    "The client establishes a connection with the server and"
                    " sends its address in a message without immediate data."
                    " The server reacts by opening a connection and sending a"
                    " reply with immediate data."
                    " From that point onwards the ping poing continues."
                    " The use of immediate data is a trick to dinstinguish the"
                    " client message containing an address from all other"
                    " ping-pong messages.\n"
                    "NOTE: a single buffer is used on both sides.";

/* A description of the arguments we accept. */
static char args_doc[] = "";

/* The options we understand. */
static struct argp_option options[] = {
  {"client",       'c', "HOSTNAME",   0, "Run in client mode and connect to the given IP. Default: server mode)" },
  {"hostname",     'H', "HOSTNAME",   0, "Listen on the given IP. Default: localhost" },
  {"port",         'p', "PORT",       0, "Listen on the given port. Default: 12345" },
  {"client-port",  'r', "PORT",       0, "Connect to the given port. Default: 12346" },
  {"iterations",   'i', "MESSAGES",   0, "Number of ping-pong messages to send. Default: 10000" },
  {"size",         's', "PACKETSIZE", 0, "Size in Bytes of the ping-pong messages. If specified, needed on both sides! Default: 256 Bytes." },
  {"mean-latency", 'm', "us",         0, "Expected max mean latency to exit with 0. Default: 20" },
  {"verbose",      'v', 0,            0, "Run in verbose mode"},
  { 0 }
};

size_t MINIMUM_BUF_SIZE = sizeof(struct sockaddr_storage);


struct {
  unsigned verbose;
  const char* local_host;
  const char* remote_host;
  unsigned port;
  unsigned client_port;

  unsigned mean_latency;
  enum { CLIENT, SERVER } mode;

  struct netio_context ctx;
  struct netio_send_socket send_socket;
  struct netio_listen_socket recv_socket;
  struct netio_timer timer;

  struct sockaddr_storage sa;
  struct netio_buffer addr_buf;

  uint32_t msg_size;

  char* bufdata;
  struct netio_buffer buf;

  struct timespec t0;
} config;


struct {
  uint64_t iterations;
  uint64_t microseconds[HISTOGRAM_SIZE];
  uint64_t overshoot;
  double total_latency;
} counters;

/* Parse a single option. */
static error_t
parse_opt (int key, char *arg, struct argp_state *state)
{
  char* end;

  switch (key)
    {
    case 'v':
      config.verbose = 1;
      break;

    case 'c':
      config.mode = CLIENT;
      config.remote_host = strdup(arg);
      break;

    case 'p':
      config.port = strtol(arg, &end, 0);
      if(end==arg) {
        argp_error(state, "'%s' is not a number", arg);
      }
      break;

    case 'r':
      config.client_port = strtol(arg, &end, 0);
      if(end==arg) {
        argp_error(state, "'%s' is not a number", arg);
      }
      break;

    case 'm':
      config.mean_latency = strtol(arg, &end, 0);
      if(end==arg) {
        argp_error(state, "'%s' is not a number", arg);
      }
      break;

    case 'H':
      config.local_host = strdup(arg);
      break;

    case 'i':
      counters.iterations = strtol(arg, &end, 0);
      if(end==arg) {
        argp_error(state, "'%s' is not a number", arg);
      }
      break;

    case 's':
      config.msg_size = strtol(arg, &end, 0);
      DEBUG_LOG("Message size set to %u", config.msg_size);
      if(end==arg) {
        argp_error(state, "'%s' is not a number", arg);
      }
      if(config.msg_size < MINIMUM_BUF_SIZE) {
        argp_error(state, "packet size '%d' is too small, min is %ld ",
                   config.msg_size, MINIMUM_BUF_SIZE);
      }

      break;

    case ARGP_KEY_ARG:
    case ARGP_KEY_END:
      if (state->arg_num >= 1)
        /* Too many arguments. */
        argp_usage (state);
      break;

    default:
      return ARGP_ERR_UNKNOWN;
    }
  return 0;
}

/* Our argp parser. */
static struct argp argp = { options, parse_opt, args_doc, doc };

void cli_parse(int argc, char **argv)
{
  // set default values
  config.local_host = "libfabric:127.0.0.1";
  config.port = 12345;
  config.client_port = 12346;
  config.mean_latency = 20;
  config.mode = SERVER;
  config.msg_size = 256;
  counters.iterations = 10000;
  // parse arguments
  argp_parse (&argp, argc, argv, 0, 0, NULL);
}

// Forward Declarations
void on_client_init();
void on_server_init();
void on_client_send_connection_established(struct netio_send_socket* socket);
void on_client_recv_connection_established(struct netio_recv_socket* socket);
void on_client_recv_msg_imm_received(struct netio_recv_socket* socket, struct netio_buffer* buf, void* data, size_t size, uint64_t imm);
void on_client_recv_msg_received(struct netio_recv_socket* socket, struct netio_buffer* buf, void* data, size_t size);
void on_server_send_connection_established(struct netio_send_socket* socket);
void on_server_recv_connection_established(struct netio_recv_socket* socket);
void on_server_recv_connection_closed(struct netio_recv_socket* socket);
void on_server_recv_msg_received(struct netio_recv_socket* socket, struct netio_buffer* buf, void* data, size_t size);
void on_server_recv_msg_imm_received(struct netio_recv_socket* socket, struct netio_buffer* buf, void* data, size_t size, uint64_t imm);


double diff_in_us(struct timespec t1, struct timespec t2)
{
  struct timespec diff;
  if (t2.tv_nsec-t1.tv_nsec < 0) {
    diff.tv_sec  = t2.tv_sec - t1.tv_sec - 1;
    diff.tv_nsec = t2.tv_nsec - t1.tv_nsec + 1000000000;
  } else {
    diff.tv_sec  = t2.tv_sec - t1.tv_sec;
    diff.tv_nsec = t2.tv_nsec - t1.tv_nsec;
  }
  return (diff.tv_sec * 1000000.0 + diff.tv_nsec / 1000.0);
}

void send_one()
{
  DEBUG_LOG("send_one");
  clock_gettime(CLOCK_MONOTONIC_RAW, &config.t0);
  if (!netio_tcp_mode(config.remote_host)) {
    //socket, buffer for mr, data ptr, data size, key, imm;
    netio_send_imm(&config.send_socket, &config.buf, config.buf.data, config.msg_size, 0, 5);
  }
  else {
    // tcp mode does not support sending immediate data at the moment send from buffer
    // netio_tcp_send_imm(&config.send_socket, &config.buf, &config.buf.data, config.msg_size, 0, 0);
    struct iovec iov[2];
    iov[0].iov_base=config.bufdata;
    iov[0].iov_len=config.msg_size;
    netio_tcp_sendv_imm(&config.send_socket,config.msg_size,&iov[0],1,0,0);
  }
}

void send_address()
{
  DEBUG_LOG("send_address");
  if (netio_tcp_mode(config.remote_host)) {
    DEBUG_LOG("%s", netio_hostname(config.local_host));
    config.addr_buf.data = config.bufdata;
    strncpy(config.bufdata, netio_hostname(config.local_host), config.msg_size-1);
    config.addr_buf.size = strlen(netio_hostname(config.local_host))+1;
  }
  else {
    size_t addrsize = netio_listen_socket_endpoint(&config.recv_socket, &config.sa);
    config.addr_buf.data = &config.sa;
    config.addr_buf.size = addrsize;
    netio_register_send_buffer(&config.send_socket, &config.addr_buf, 0);
  }
  netio_send_buffer(&config.send_socket, &config.addr_buf);
}

void on_client_init()
{
  DEBUG_LOG("on_client_init");
  DEBUG_LOG("Opening %s listen socket on %s:%u\n", netio_protocol(config.remote_host), config.local_host, config.client_port);
  struct netio_unbuffered_socket_attr attr = {1, config.msg_size};

  //recv - we do with the protocol where we connect to (remote_host)
  if (netio_tcp_mode(config.remote_host)) {
    netio_init_listen_tcp_socket(&config.recv_socket, &config.ctx, &attr);
    netio_listen_tcp(&config.recv_socket, netio_hostname(config.local_host), config.client_port);
    config.recv_socket.cb_msg_received = on_client_recv_msg_received;
  } else {
    netio_init_listen_socket(&config.recv_socket, &config.ctx, &attr);
    netio_listen(&config.recv_socket, netio_hostname(config.local_host), config.client_port);
  }
  config.recv_socket.cb_connection_established = on_client_recv_connection_established;
  config.recv_socket.cb_msg_imm_received = on_client_recv_msg_imm_received;

  //send
  DEBUG_LOG("Sending to %s remote socket on %s:%u\n", netio_protocol(config.remote_host), config.remote_host, config.client_port);
  netio_send_socket_init_and_connect(&config.send_socket, &config.ctx, config.remote_host, config.port);
  config.send_socket.cb_connection_established = on_client_send_connection_established;
}

void on_client_send_connection_established(struct netio_send_socket* socket)
{
  DEBUG_LOG("on_client_send_connection_established");
  if (!netio_tcp_mode(config.local_host)) {
    config.buf.data = config.bufdata;
    config.buf.size = config.msg_size;
    netio_register_send_buffer(&config.send_socket, &config.buf, 0);
  }
  send_address();
}

void on_client_recv_connection_established(struct netio_recv_socket* socket)
{
  DEBUG_LOG("on_client_recv_connection_established");
  send_one();
}

void on_client_recv_msg_imm_received(struct netio_recv_socket* socket, struct netio_buffer* buf, void* data, size_t size, uint64_t imm)
{
  on_client_recv_msg_received(socket, buf, data, size);
}

void on_client_recv_msg_received(struct netio_recv_socket* socket, struct netio_buffer* buf, void* data, size_t size)
{
  DEBUG_LOG("on_client_recv_msg_received");
  struct timespec t1;
  clock_gettime(CLOCK_MONOTONIC_RAW, &t1);

  double us = diff_in_us(config.t0, t1);
  counters.total_latency+=us;
  if((int)us >= HISTOGRAM_SIZE) {
    counters.overshoot++;
  } else {
    counters.microseconds[(int)us]++;
  }

  counters.iterations--;
  if(counters.iterations == 0) {
    netio_terminate(&socket->ctx->evloop);
    return;
  }
  send_one();
  netio_post_recv(socket, buf);
}

void on_server_init()
{
  DEBUG_LOG("on_server_init");
  DEBUG_LOG("Opening %s listen socket on %s:%u\n", netio_tcp_mode(config.local_host) ? "tcp":"libfabric", config.local_host, config.port);
  struct netio_unbuffered_socket_attr attr = {1, config.msg_size};

  netio_listen_socket_init_and_listen(&config.recv_socket, &config.ctx, config.local_host, config.port, &attr);
  config.recv_socket.cb_connection_established = on_server_recv_connection_established;
  config.recv_socket.cb_connection_closed=on_server_recv_connection_closed;
  config.recv_socket.cb_msg_received = on_server_recv_msg_received;
  config.recv_socket.cb_msg_imm_received = on_server_recv_msg_imm_received;
}

void on_server_send_connection_established(struct netio_send_socket* socket)
{
  DEBUG_LOG("on_server_send_connection_established");
  config.buf.data = config.bufdata;
  config.buf.size = config.msg_size;
  netio_register_send_buffer(socket, &config.buf, 0);
}

void on_server_recv_connection_established(struct netio_recv_socket* socket)
{
  DEBUG_LOG("on_server_recv_connection_established");
}

void on_server_recv_connection_closed(struct netio_recv_socket* socket) {
  if (netio_tcp_mode(config.local_host)) {
    if (config.send_socket.ctx != 0) {
      printf("Recv socket closed, clearing send socket context\n");
      config.send_socket.ctx=0;
    }
  }
}

void on_server_recv_msg_received(struct netio_recv_socket* socket, struct netio_buffer* buf, void* data, size_t size)
{
  DEBUG_LOG("on_server_recv_msg_received");
  if (netio_tcp_mode(config.local_host)) {
    if (config.send_socket.ctx == 0) {
      netio_init_send_tcp_socket(&config.send_socket, &config.ctx);
      netio_connect_tcp(&config.send_socket, buf->data, config.client_port);
    }
    else {
      struct iovec iov[2];
      iov[0].iov_base=data;
      iov[0].iov_len=size;
      netio_tcp_sendv_imm(&config.send_socket,size,&iov[0],1,0,0);
    }
  }
  else {
    netio_init_send_socket(&config.send_socket, &config.ctx);
    config.send_socket.cb_connection_established = on_server_send_connection_established;
    netio_connect_rawaddr(&config.send_socket, data, size);
  }
  netio_post_recv(socket, buf);
}

void on_server_recv_msg_imm_received(struct netio_recv_socket* socket, struct netio_buffer* buf, void* data, size_t size, uint64_t imm)
{
  DEBUG_LOG("on_server_recv_msg_imm_received");
  netio_send_imm(&config.send_socket, &config.buf, &config.buf.data, 0, 0, imm);
  netio_post_recv(socket, buf);
}


int main(int argc, char** argv)
{
  memset(&config, 0, sizeof(config));

  cli_parse(argc, argv);
  config.bufdata = malloc(config.msg_size);
  uint8_t* bufbytes = (uint8_t*)(config.bufdata);
  for(size_t i = 0; i < config.msg_size; ++i){
    //fill payload with sequential data
    bufbytes[i] = i;
  }

  for(unsigned i=0; i<HISTOGRAM_SIZE; i++) {
    counters.microseconds[i] = 0;
  }
  counters.overshoot = 0;
  counters.total_latency = 0.0;

  netio_init(&config.ctx);
  if(config.mode == CLIENT) {
    config.ctx.evloop.cb_init = on_client_init;
  } else {
    config.ctx.evloop.cb_init = on_server_init;
  }

  netio_run(&config.ctx.evloop);

  uint64_t iterations_complete=0;
  for(unsigned i=0; i<HISTOGRAM_SIZE; i++) {
    if(counters.microseconds[i]) {
      printf("%u: %lu\n", i, counters.microseconds[i]);
      iterations_complete+=counters.microseconds[i];
    }
  }
  if(counters.overshoot) {
    printf("overshoot: %lu\n", counters.overshoot);
    iterations_complete+=counters.overshoot;
  }
  double mean_latency = counters.total_latency/iterations_complete;
  printf ("mean latency %.0fus\n", mean_latency);

  return mean_latency <= config.mean_latency ? 0 : 1;
}
