#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <signal.h>
#include <sys/types.h>
#include <unistd.h>

#include "netio/netio.h"
#include "felixtag.h"

struct {
  char* hostname;
  uint port;
  netio_tag_t tag;
  uint delay;

  struct netio_context ctx;
  struct netio_unbuffered_publish_socket socket;
  struct netio_buffer buf;

  struct netio_timer timer;

  char* data;
  size_t datasize;
} config;

struct {
  struct netio_timer timer;
  struct timespec t0;
  uint64_t total_messages_sent;
  uint64_t messages_sent;
  uint64_t bytes_sent;
  int nosub;
} statistics;


void __gcov_dump(void);


void my_handler(int signum) {
  printf("received signal\n");
  netio_timer_stop(&config.timer);
  netio_terminate(&config.ctx.evloop);
#ifdef FELIX_COVERAGE
  __gcov_dump(); /* dump coverage data on receiving SIGINT */
#endif
}


void on_stats(void* ptr) {
  struct timespec t1;
  clock_gettime(CLOCK_MONOTONIC_RAW, &t1);

  if (!statistics.nosub) {
    statistics.total_messages_sent+=statistics.messages_sent;
    double seconds = t1.tv_sec - statistics.t0.tv_sec
                     + 1e-9*(t1.tv_nsec - statistics.t0.tv_nsec);
    printf("data rate: %2.2f Gb/s  message rate: %2.2f kHz, total message: %lu\n",
           statistics.messages_sent*config.datasize*8/1024./1024./1024./seconds,
           statistics.messages_sent/1000./seconds,
           statistics.total_messages_sent
           );
  }

  statistics.bytes_sent = 0;
  statistics.messages_sent = 0;
  statistics.t0 = t1;
}


void on_timer(void* ptr)
{
  struct iovec iov;
  iov.iov_base = config.data;
  iov.iov_len = config.datasize;

  static uint64_t msgid = 0;
  ++msgid;
  uint64_t key = msgid;
  int flags = 0;

  int rv = netio_unbuffered_publishv(&config.socket,
      config.tag,
      &iov,
      1,    // iov count
      &key, // key
      flags,// flags
      NULL  // subscription cache
      );

  while (rv == NETIO_STATUS_PARTIAL) {
    flags |= NETIO_REENTRY;
    rv = netio_unbuffered_publishv(&config.socket, config.tag, &iov, 1, &key, flags, NULL);
  }
  if (rv == NETIO_STATUS_OK || rv == NETIO_STATUS_OK_NOSUB || rv == NETIO_STATUS_AGAIN) {
    statistics.messages_sent++;
    return;
  }
  else if (rv < 0) {
    fprintf(stderr, "netio_unbuffered_publishv error: %d\n", rv);
    return;
  }
}

void on_subscribe(struct netio_unbuffered_publish_socket* socket, netio_tag_t tag, void* addr, size_t addrlen)
{
  statistics.nosub=0;
  printf("remote subscribed to tag 0x%lx\n", tag);
}

void on_connection_established(struct netio_unbuffered_publish_socket* socket)
{
  puts("connection to subscriber established");
}

void on_connection_closed(struct netio_unbuffered_publish_socket* socket)
{
  puts("connection to subscriber closed");
}

void on_msg_published(struct netio_unbuffered_publish_socket* socket, uint64_t key)
{
  // printf("msg published, completion key=%lu\n", key);
}

void on_init()
{
  config.buf.size = 16*64*1024;
  config.buf.data = malloc(config.buf.size);

  printf("Opening publish socket on %s:%u\n", config.hostname, config.port);
  netio_unbuffered_publish_socket_init(&config.socket, &config.ctx, config.hostname, config.port, &config.buf);

  config.socket.cb_subscribe = on_subscribe;
  config.socket.cb_connection_established = on_connection_established;
  config.socket.cb_connection_closed = on_connection_closed;
  config.socket.cb_msg_published = on_msg_published;

  config.datasize = 1024;
  config.data = config.buf.data;
  for(unsigned i=0; i<config.datasize; i++) {
    config.data[i] = 'x';
  }
  unsigned int* xptr=(unsigned int*) &config.data[1020];
  *xptr=0xdeadbeef;

  netio_timer_init(&config.ctx.evloop, &config.timer);
  config.timer.cb = on_timer;
  netio_timer_start_us(&config.timer, config.delay);

  printf("Publishing with delay %u us\n", config.delay);

  netio_timer_init(&config.ctx.evloop, &statistics.timer);
  clock_gettime(CLOCK_MONOTONIC_RAW, &statistics.t0);
  statistics.timer.cb = on_stats;
  statistics.bytes_sent = 0;
  statistics.messages_sent = 0;
  statistics.nosub = 1;
  netio_timer_start_s(&statistics.timer, 1);
}

int main(int argc, char** argv)
{

  if(argc != 5) {
    fprintf(stderr, "usage: %s <hostname> <port> <tag> <delay us>\n", argv[0]);
    fprintf(stderr, "Version: %s", FELIX_TAG);
    return 1;
  }

  // Add ctrl-c handler
  struct sigaction new_action, old_action;
  new_action.sa_handler = my_handler;
  sigemptyset(&new_action.sa_mask);
  new_action.sa_flags = 0;
  sigaction(SIGINT, NULL, &old_action);
  if (old_action.sa_handler != SIG_IGN) {
    sigaction (SIGINT, &new_action, NULL);
  }

  config.hostname = argv[1];
  config.port = atoi(argv[2]);
  config.tag = strtol(argv[3], NULL, 0);
  config.delay = atoi(argv[4]);

  netio_init(&config.ctx);
  config.ctx.evloop.cb_init = on_init;
  netio_run(&config.ctx.evloop);

  return 0;
}
