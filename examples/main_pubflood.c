#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <signal.h>
#include <sys/types.h>
#include <unistd.h>

#include "netio/netio.h"

#include "felixtag.h"

void __gcov_dump(void);

#define MAX_MESSAGES_PER_FLOOD (250)

struct {
  char* hostname;
  unsigned port;
  int use_fid;
  netio_tag_t first_tag;
  netio_tag_t last_tag;

  struct netio_context ctx;
  struct netio_publish_socket socket;
  struct netio_signal signal;

  char* data;
  size_t datasize;
} config;

void my_handler(int signum) {
  printf("received signal\n");
  netio_terminate(&config.ctx.evloop);
#ifdef FELIX_COVERAGE
  __gcov_dump(); /* dump coverage data on receiving SIGINT */
#endif
}

struct {
  struct netio_timer timer;
  struct timespec t0;
  uint64_t total_messages_sent;
  uint64_t messages_sent;
  uint64_t bytes_sent;
  uint64_t again;
  int nosub;
} statistics;


void on_subscribe(struct netio_publish_socket* socket, netio_tag_t tag, void* addr, size_t addrlen)
{
  printf("remote subscribed to tag 0x%lx\n", tag);
}

void on_unsubscribe(struct netio_publish_socket* socket, netio_tag_t tag, void* addr, size_t addrlen)
{
  printf("remote unsubscribed from tag 0x%lx\n", tag);
}

void on_connection_established(struct netio_publish_socket* socket)
{
  printf("connection to subscriber established\n");
}

void on_connection_closed(struct netio_publish_socket* socket)
{
  printf("connection to subscriber closed\n");
}

void flood() {
  for(netio_tag_t tag=config.first_tag; tag<=config.last_tag; tag += config.use_fid ? 0x10000 : 1) {
    for(unsigned i=0; i<MAX_MESSAGES_PER_FLOOD; i++) {
      int ret = netio_buffered_publish(&config.socket, tag, config.data, config.datasize, 0, NULL);
      if(ret == NETIO_STATUS_OK) {
        statistics.messages_sent += 1;
        statistics.bytes_sent += config.datasize;
      } else if (ret == NETIO_STATUS_AGAIN) {
        statistics.again += 1;
        netio_signal_fire(&config.signal);
        return;
      } else if (ret == NETIO_STATUS_OK_NOSUB) {
        continue;
      }
    }
    netio_buffered_publish_flush(&config.socket, tag, NULL);
  }
  netio_signal_fire(&config.signal);
}


void flood_buffer_available(struct netio_publish_socket *socket) {
    flood();
}

void on_stats(void* ptr) {
  struct timespec t1;
  clock_gettime(CLOCK_MONOTONIC_RAW, &t1);

  if (statistics.nosub) {
    printf("no subscription\n");
  } else {
    statistics.total_messages_sent+=statistics.messages_sent;
    double seconds = t1.tv_sec - statistics.t0.tv_sec
                     + 1e-9*(t1.tv_nsec - statistics.t0.tv_nsec);
    printf("data rate: %2.2f Gb/s  message rate: %2.2f kHz, total message: %lu\n",
           statistics.bytes_sent*8/1024./1024./1024./seconds,
           statistics.messages_sent/1000./seconds,
           statistics.total_messages_sent
           );
  }

  statistics.bytes_sent = 0;
  statistics.messages_sent = 0;
  statistics.t0 = t1;
}


void on_init()
{
  struct netio_buffered_socket_attr attr;
  attr.num_pages = 256;
  attr.pagesize = 64*1024;
  attr.watermark = 56*1024;
  attr.timeout_ms = 1000;

  printf("Opening publish socket on %s:%u\n", config.hostname, config.port);
  netio_publish_socket_init(&config.socket, &config.ctx, config.hostname, config.port, &attr);
  config.socket.cb_buffer_available = flood_buffer_available;
  config.socket.cb_subscribe = on_subscribe;
  config.socket.cb_unsubscribe = on_unsubscribe;
  config.socket.cb_connection_established = on_connection_established;
  config.socket.cb_connection_closed = on_connection_closed;

  config.datasize = 128;
  config.data = (char*)malloc(config.datasize);
  for(unsigned i=0; i<config.datasize; i++) {
    config.data[i] = 'x';
  }

  netio_signal_init(&config.ctx.evloop, &config.signal);
  config.signal.cb = flood;
  netio_signal_fire(&config.signal);

  netio_timer_init(&config.ctx.evloop, &statistics.timer);
  clock_gettime(CLOCK_MONOTONIC_RAW, &statistics.t0);
  statistics.timer.cb = on_stats;
  statistics.bytes_sent = 0;
  statistics.messages_sent = 0;
  statistics.nosub = 0;
  netio_timer_start_s(&statistics.timer, 1);
}


int main(int argc, char** argv)
{
  if(argc != 5) {
    fprintf(stderr, "Usage: %s <hostname> <port> <first_tag> <last_tag>\n", argv[0]);
    fprintf(stderr, "Version: %s", FELIX_TAG);
    return 1;
  }

  // Add ctrl-c handler
  struct sigaction new_action, old_action;
  new_action.sa_handler = my_handler;
  sigemptyset(&new_action.sa_mask);
  new_action.sa_flags = 0;
  sigaction(SIGINT, NULL, &old_action);
  if (old_action.sa_handler != SIG_IGN) {
    sigaction (SIGINT, &new_action, NULL);
  }

  config.hostname = argv[1];
  config.port = atoi(argv[2]);
  config.first_tag = strtol(argv[3], NULL, 0);
  config.last_tag = strtol(argv[4], NULL, 0);
  config.use_fid = (config.first_tag & 0x1000000000000000) > 0L;
  printf("Publishing on links 0x%lx to 0x%lx \n", config.first_tag, config.last_tag);

  netio_init(&config.ctx);
  config.ctx.evloop.cb_init = on_init;
  netio_run(&config.ctx.evloop);

  return 0;
}
