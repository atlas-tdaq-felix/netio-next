/****************************************************************/
/*								*/
/*  Playing with C1 generics 					*/
/*								*/
/*  10. Feb. 21  M. Joos  created				*/
/*								*/
/***********C 2021 - A nickel program worth a dime***************/

#include <stdlib.h>
#include <stdio.h>


#define doubleit(X) _Generic((X), \
  long:doubleit_int, \
  default:doubleit_long, \
  float:doubleit_float \
  )(X)

int doubleit_int(int x)
{
  printf("calling %s\n", __func__);
  return (2 * x);
}

int doubleit_long(long x)
{
  printf("calling %s\n", __func__);
  return (2 * x);
}
 
int doubleit_float(float x){
  printf("calling %s\n", __func__);
  return (2 * x);
}


/*****************************/
int main(int argc, char **argv)
/*****************************/
{
  int d = 2, res;
  long l = 4;
  float f = 8;

  res = doubleit(d);
  printf("res = %d\n", res);
  res = doubleit(l);
  printf("res = %d\n", res);
  res = doubleit(f);
  printf("res = %d\n", res);
}

