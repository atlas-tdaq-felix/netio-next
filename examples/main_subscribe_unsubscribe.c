/**
* @brief Buffered subscription/unsubscription test routine
*
* The application first subscribes to all e-links provided through the arguments
* and supposedly published by main_pubflood. Then, in steps of 6 seconds, the
* application unsubscribes and resubscribes to subsets of e-links.
*
**/


#include <stdio.h>
#include <string.h>
#include <math.h>
#include <unistd.h>

#include "netio/netio.h"
#include "felixtag.h"


struct {
  char* local_hostname;
  char* remote_hostname;
  unsigned port;
  int use_fid;
  netio_tag_t first_tag;
  netio_tag_t last_tag;

  struct netio_context ctx;
  struct netio_subscribe_socket socket;

  struct netio_timer subscribe_timer;
  //to alternate subscription and unsubscription
  int subscribed;

  //keep track of all rounds
  // 0 beginning, subscribe to all e-links
  // 1-3 unsubscribe from one link at a time: first, middle, last
  // 4 unsibscribe from a pair of links: first and last
  // 5 unsubscribe from half the links
  // 6 unsubscribe from all, resubscribe to all
  int test_step;
  size_t tag_array_size;
  netio_tag_t* tag_array;

  size_t test_size;
  netio_tag_t* test_array;

  int test_time_interval;
  int warnings;
} config;


struct {
  struct netio_timer timer;
  struct timespec t0;
  uint64_t ref_rate;
  uint64_t messages_received;
  uint64_t bytes_received;
  uint64_t total_messages_received;
  uint64_t* msg_per_tag;
} stats;

int return_code = 0;

void test_condition(){
  if (!config.subscribed) {
    for(unsigned int i=0; i<config.test_size; ++i){
      for(unsigned int j=0; j<config.tag_array_size; ++j){
        if(config.test_array[i]==config.tag_array[j]){
          if(stats.msg_per_tag[j]>(stats.ref_rate/4)){
            config.warnings++;
            printf("WARNING %d: too high rate for tag %lu which should be unsubscribed now!\n", config.warnings, config.test_array[i]);
          }
        }
      }
    }
  }
  if (config.subscribed) {
    for(unsigned int i=0; i<config.test_size; ++i){
      for(unsigned int j=0; j<config.tag_array_size; ++j){
        if(config.test_array[i]==config.tag_array[j]){
          if(stats.msg_per_tag[j]<(stats.ref_rate/4)){
            config.warnings++;
            printf("WARNING %d: too low rate for tag %lu which should be subscribed now!\n", config.warnings, config.test_array[i]);
          }
        }
      }
    }
  }
  if(config.warnings>=config.test_time_interval*config.test_size){printf("ERROR: %d warnings recorded. Aborting.\n", config.warnings);  exit(1);}
}


void prepare_tag_array(){
  if(config.test_step==0){
    config.test_size = config.tag_array_size;
    config.test_array = malloc(config.test_size*sizeof(netio_tag_t));
    for(unsigned int i=0; i<config.test_size; ++i){
      config.test_array[i] = config.tag_array[i];
    }
  }
  else if(config.test_step==1){
    free(config.test_array);
    config.test_array = malloc(sizeof(netio_tag_t));
    config.test_size = 1;
    config.test_array[0] = config.tag_array[0];
  }
  else if(config.test_step==2){
    config.test_array[0] = config.tag_array[(config.tag_array_size/2)];
  }
  else if(config.test_step==3){
    config.test_array[0] = config.tag_array[config.tag_array_size-1];
  }
  else if(config.test_step==4){
    free(config.test_array);
    config.test_size = config.tag_array_size==1 ? 1 : 2;
    config.test_array = malloc(config.test_size*sizeof(netio_tag_t));
    config.test_array[0] = config.tag_array[0];
    if(config.test_size == 2){
      config.test_array[1] = config.tag_array[config.tag_array_size-1];
    }
  }
  else if(config.test_step==5){
    free(config.test_array);
    config.test_size = config.tag_array_size > 1 ? config.tag_array_size/2 : config.tag_array_size;
    config.test_array = malloc(config.test_size*sizeof(netio_tag_t));
    if(config.tag_array_size > 1){
      unsigned int j=1;
      for(unsigned int i=0; (i<config.test_size && j<config.tag_array_size); ++i){
        config.test_array[i] =  config.tag_array[j];
        j+=2;
      }
    } else {
      config.test_array[0] = config.tag_array[0];
    }
  }
  else if(config.test_step==6){
    free(config.test_array);
    config.test_size = config.tag_array_size;
    config.test_array = malloc(config.test_size*sizeof(netio_tag_t));
    for(unsigned int i=0; i<config.test_size; ++i){
      config.test_array[i] = config.tag_array[i];
    }
  }
  else if(config.test_step>6){
      printf("Test completed. Terminating the eventloop.\n");
      return_code = 42;
      netio_terminate(&config.ctx.evloop);
  }
  else{
    printf("Invalid test step. Exit.\n");
    exit(1);
  }
}

void subscribe()
{
  for(unsigned int i=0; i < config.test_size; ++i) {
    printf(">> subscribing to tag 0x%lx\n", config.test_array[i]);
    netio_subscribe(&config.socket, config.test_array[i]);
    config.subscribed=1;
  }
}

void unsubscribe()
{
  for(unsigned int i=0; i < config.test_size; ++i) {
    printf(">> unsubscribing from tag 0x%lx\n", config.test_array[i]);
    netio_unsubscribe(&config.socket,config.test_array[i]);
    config.subscribed=0;
  }
}

void change_subscriptions()
{
  config.warnings=0;
  prepare_tag_array();
  if(config.subscribed){
    printf("--- TEST STEP %d --- \n", config.test_step );
    unsubscribe();
  }
  else{
    subscribe();
    config.test_step++;
  }
}


void on_connection_closed(struct netio_subscribe_socket* socket)
{
  printf("connection to publisher closed from %s:%u\n", socket->remote_hostname, socket->remote_port);
}


void on_connection_established(struct netio_subscribe_socket* socket) {
  printf("connection established to %s:%u\n", socket->remote_hostname, socket->remote_port);
}


void on_error_connection_refused(struct netio_subscribe_socket* socket)
{
  printf("connection refused for subscribing from %s:%u\n", socket->remote_hostname, socket->remote_port);
}


void on_msg_received(struct netio_subscribe_socket* socket, netio_tag_t tag, void* data, size_t size)
{
  stats.total_messages_received++;
  stats.messages_received++;
  stats.bytes_received += size;
  for(unsigned int i=0; i<config.tag_array_size; ++i){
    if(tag == config.tag_array[i]){stats.msg_per_tag[i]++;}
  }
}


void on_stats(void* ptr) {
  struct timespec t1;
  clock_gettime(CLOCK_MONOTONIC_RAW, &t1);
  // double seconds = t1.tv_sec - stats.t0.tv_sec
  //                  + 1e-9*(t1.tv_nsec - stats.t0.tv_nsec);
  printf("tag %lu %lu\t", config.tag_array[0], stats.msg_per_tag[0]);
  for(unsigned int i=1; i<config.tag_array_size; ++i){
    printf("tag %lu %lu\t", config.tag_array[i], stats.msg_per_tag[i]);
  }
  printf("\n");
  if(stats.ref_rate==0){stats.ref_rate = stats.msg_per_tag[0];}
  //check rates
  test_condition();

  for(unsigned int i=0; i<config.tag_array_size; ++i){
    stats.msg_per_tag[i]=0;
  }
  stats.messages_received = 0;
  stats.t0 = t1;
}


void on_init()
{
  struct netio_buffered_socket_attr attr;
  attr.num_pages = 256;
  attr.pagesize = 64*1024;
  attr.watermark = 56*1024;
  attr.timeout_ms = 1000;

  for(unsigned int i=0; i<config.tag_array_size; ++i){
    stats.msg_per_tag[i]=0;
  }
  printf("Opening subscribe socket from %s on %s:%u\n",
         config.local_hostname, config.remote_hostname, config.port);
  netio_subscribe_socket_init(&config.socket, &config.ctx, &attr, config.local_hostname, config.remote_hostname, config.port);

  config.socket.cb_connection_closed = on_connection_closed;
  config.socket.cb_connection_established = on_connection_established;
  config.socket.cb_error_connection_refused = on_error_connection_refused;
  config.socket.cb_msg_received = on_msg_received;

  netio_timer_init(&config.ctx.evloop, &stats.timer);
  clock_gettime(CLOCK_MONOTONIC_RAW, &stats.t0);
  stats.timer.cb = on_stats;
  netio_timer_start_s(&stats.timer, 1);

  netio_timer_init(&config.ctx.evloop, &config.subscribe_timer);
  config.subscribe_timer.cb = change_subscriptions;
  netio_timer_start_s(&config.subscribe_timer, config.test_time_interval);
  change_subscriptions();
}


int main(int argc, char** argv)
{
  if(argc < 6 || argc > 7) {
    fprintf(stderr, "Usage: %s <local_hostname> <remote_hostname> <port> <first_tag> <last_tag>\n", argv[0]);
    fprintf(stderr, "Version: %s", FELIX_TAG);
    return 1;
  }

  config.local_hostname = argv[1];
  config.remote_hostname = argv[2];
  config.port = atoi(argv[3]);
  config.first_tag = strtol(argv[4], NULL, 0);
  config.last_tag = strtol(argv[5], NULL, 0);
  config.use_fid = (config.first_tag & 0x1000000000000000) > 0L;

  config.test_time_interval = 4;
  config.test_step = 0;
  config.tag_array_size = 0;
  for(netio_tag_t tag = config.first_tag; tag <= config.last_tag; tag += config.use_fid ? 0x10000 : 1) {
    ++config.tag_array_size;
  }
  config.tag_array = malloc(config.tag_array_size*sizeof(netio_tag_t));
  unsigned int i=0;
  for(netio_tag_t tag = config.first_tag; tag <= config.last_tag; tag += config.use_fid ? 0x10000 : 1) {
    config.tag_array[i] = tag;
    ++i;
  }
  printf("Subscribing to %zu links \n", config.tag_array_size);
  stats.msg_per_tag = malloc(config.tag_array_size*sizeof(uint64_t));
  stats.ref_rate=0;

  netio_init(&config.ctx);
  config.ctx.evloop.cb_init = on_init;
  netio_run(&config.ctx.evloop);

  return return_code;
}
