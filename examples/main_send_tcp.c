#include <stdio.h>
#include <string.h>
#include <math.h>
#include "netio/netio.h"
#include "netio/netio_tcp.h"


//To run on pcepese48: ./send_tcp 128.141.94.209 61617


//Globals
int mess_count = 0;
struct netio_buffer databuf1, databuf2;
void *databuffer1, *databuffer2;


//#define NUM_BUFFERS (256)
//#define BUFFER_SIZE (64*1024)
#define NUM_BUFFERS 2
#define BUFFER_SIZE 16
#define MIN(X, Y) (((X) < (Y)) ? (X) : (Y))


struct {
  int receivers;
  char* hostname;
  unsigned port;

  struct netio_context ctx;
  struct netio_send_socket socket;
  struct netio_send_socket socket2;
} config;


// Forward declarations
void on_connection_established(struct netio_send_socket*);
void on_connection_refused(struct netio_send_socket*);
void on_send_completed(struct netio_send_socket* socket, uint64_t key);
void on_connection_established2(struct netio_send_socket*);
void on_connection_refused2(struct netio_send_socket*);
void on_send_completed2(struct netio_send_socket* socket, uint64_t key);


// Callbacks

/*********************/
void on_init(void* ptr)
/*********************/
{
  printf("---->Calling netio_init_send_tcp_socket\n");
  netio_init_send_tcp_socket(&config.socket, &config.ctx);

  printf("---->Calling netio_connect_tcp\n");
  netio_connect_tcp(&config.socket, config.hostname, config.port);

  config.socket.cb_connection_established = on_connection_established;
  printf("---->on_connection_established is at %p\n", on_connection_established);

  config.socket.cb_send_completed = on_send_completed;
  printf("---->on_send_completed         is at %p\n", on_send_completed);

  config.socket.cb_error_connection_refused = on_connection_refused;
  printf("---->on_connection_refused     is at %p\n", on_connection_refused);

  // print provider so we can easy check if using TCP or RDMA
  printf("Attempting connection to %s:%u.\n", config.hostname, config.port);

  if(config.receivers == 2)
  {
    printf("---->Calling second netio_init_send_tcp_socket\n");
    netio_init_send_tcp_socket(&config.socket2, &config.ctx);

    printf("---->Calling second netio_connect_tcp\n");
    netio_connect_tcp(&config.socket2, config.hostname, config.port + 1);

    config.socket2.cb_connection_established = on_connection_established2;
    printf("---->second on_connection_established is at %p\n", on_connection_established2);

    config.socket2.cb_send_completed = on_send_completed2;
    printf("---->second on_send_completed         is at %p\n", on_send_completed2);

    config.socket.cb_error_connection_refused = on_connection_refused2;
    printf("---->second on_connection_refused     is at %p\n", on_connection_refused2);

    // print provider so we can easy check if using TCP or RDMA
    printf("Attempting connection to %s:%u.\n", config.hostname, config.port + 1);
  }


}


/**********************************************************/
void on_connection_refused(struct netio_send_socket* socket)
/**********************************************************/
{
  printf(">>>>>>>>>>>>connection refused<<<<<<<<<<<<<<<<<");
}


/***********************************************************/
void on_connection_refused2(struct netio_send_socket* socket)
/***********************************************************/
{
  printf(">>>>>>>>>>>>connection refused<<<<<<<<<<<<<<<<<");
}

/**************************************************************/
void on_connection_established(struct netio_send_socket* socket)
/**************************************************************/
{
  int loop, *idata;

  printf("\n\n\n>>>>>>>>>>>>>>>on_connection_established start<<<<<<<<<<<<<<<<\n");
  printf("databuffer1 = %p\n", databuffer1);
  printf("databuf1    = %p\n", &databuf1);

  idata = (int *)databuffer1;
  for (loop = 0; loop < databuf1.size / 4; loop++)
    idata[loop] = loop;

  idata[0] = 0x11110001;
  idata[loop - 1] = 0x99990001;

  printf("databuf1.size = %d, databuf1.data = %p\n", (int)databuf1.size, databuf1.data);

  printf(" address of the buffer that will be sent: = %p\n", &databuf1);
  netio_send_buffer(socket, &databuf1);

  printf(">>>>>>>>>>>>>>>on_connection_established end<<<<<<<<<<<<<<<<\n");
}


/***************************************************************/
void on_connection_established2(struct netio_send_socket* socket)
/***************************************************************/
{
  int loop, *idata;

  printf("\n\n\n>>>>>>>>>>>>>>>on_connection_established2 start<<<<<<<<<<<<<<<<\n");
  printf("databuffer2 = %p\n", databuffer2);
  printf("databuf2    = %p\n", &databuf2);

  idata = (int *)databuffer2;
  for (loop = 0; loop < databuf2.size / 4; loop++)
    idata[loop] = loop;

  idata[0] = 0x11110002;
  idata[loop - 1] = 0x99990002;

  printf("databuf2.size = %d, databuf2.data = %p\n", (int)databuf2.size, databuf2.data);

  printf(" address of the buffer that will be sent: = %p\n", &databuf2);
  netio_send_buffer(socket, &databuf2);

  printf(">>>>>>>>>>>>>>>on_connection_established2 end<<<<<<<<<<<<<<<<\n");
}


/********************************************************************/
void on_send_completed(struct netio_send_socket* socket, uint64_t key)
/********************************************************************/
{
  int loop, *idata;

  printf("\n\n\n>>>>>>>>>>>>>>>on_send_completed start<<<<<<<<<<<<<<<<\n");
  printf("on_send_completed  >>>>  databuf1.size = %d, databuf1.data = %p, key = 0x%016lx\n", (int)databuf1.size, databuf1.data, key);

  idata = (int *)databuffer1;
  for (loop = 0; loop < databuf1.size / 4; loop++)
    //idata[loop] = loop * 10000 + mess_count;
    idata[loop] = loop;

  idata[0] = 0x11110001;
  idata[loop - 1] = 0x99990001;

  printf("on_send_completed  >>>>  address of the buffer that will be sent: = %p\n", &databuf1);
  netio_send_buffer(socket, &databuf1);

  mess_count++;
  printf("on_send_completed  >>>>  mess_count = %d\n", mess_count);
  if (mess_count == 100)
  {
    printf("That is enough\n");
    exit(0);
  }

  printf(">>>>>>>>>>>>>>>on_send_completed end<<<<<<<<<<<<<<<<\n");
}


/*********************************************************************/
void on_send_completed2(struct netio_send_socket* socket, uint64_t key)
/*********************************************************************/
{
  int loop, *idata;

  printf("\n\n\n>>>>>>>>>>>>>>>on_send_completed2 start<<<<<<<<<<<<<<<<\n");
  printf("on_send_completed  >>>>  databuf2.size = %d, databuf2.data = %p\n", (int)databuf2.size, databuf2.data);

  idata = (int *)databuffer2;
  for (loop = 0; loop < databuf2.size / 4; loop++)
    idata[loop] = loop * 10000 + mess_count;

  idata[0] = 0x11110002;
  idata[loop - 1] = 0x99990002;

  printf("on_send_completed  >>>>  address of the buffer that will be sent: = %p\n", &databuf2);
  netio_send_buffer(socket, &databuf2);

  mess_count++;
  printf("on_send_completed  >>>>  mess_count = %d\n", mess_count);
  if (mess_count == 10)
  {
    printf("That is enough\n");
    exit(0);
  }

  printf(">>>>>>>>>>>>>>>on_send_completed2 end<<<<<<<<<<<<<<<<\n");
}



/*****************************/
int main(int argc, char** argv)
/*****************************/
{
  if(argc != 4) {
    fprintf(stderr, "Usage: %s <hostname> <port> <#receivers>\n", argv[0]);
    return 1;
  }

  databuf1.size = 1000;
  databuffer1   = malloc(databuf1.size);
  databuf1.data = databuffer1;

  databuf2.size = 1000000;
  databuffer2   = malloc(databuf2.size);
  databuf2.data = databuffer2;

  config.hostname  = argv[1];
  config.port      = atoi(argv[2]);
  config.receivers = atoi(argv[3]);

  netio_init(&config.ctx);
  config.ctx.evloop.cb_init = on_init;

  printf("main: &config.ctx = %p\n", &config.ctx);
  printf("main: &config.ctx.evloop = %p\n", &config.ctx.evloop);

  netio_run(&config.ctx.evloop);
  free(databuffer1);
  free(databuffer2);

  return 0;
}
