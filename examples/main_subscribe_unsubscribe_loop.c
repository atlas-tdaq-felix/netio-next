#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdarg.h>
#include <math.h>
#include <sys/types.h>
#include <unistd.h>

#include "netio/netio.h"

#include "util.h"

#include "felixtag.h"

#define NUM_PAGES (256)
#define WATERMARK (63*1024)
#define PAGESIZE (64*1024)
#define TIMEOUT (1000)

struct {
  char* local_hostname;
  char* remote_hostname;
  unsigned subscribe_port;
  netio_tag_t subscribe_tag;

  struct netio_context ctx;
  struct netio_subscribe_socket subscribe_socket;

  int iterations;
} config;

void on_subscribe_connection_closed(struct netio_subscribe_socket* socket);
void on_subscribe_connection_established(struct netio_subscribe_socket* socket);
void on_subscribe_connection_established(struct netio_subscribe_socket* socket);
void on_subscribe_error_connection_refused(struct netio_subscribe_socket* socket);
void on_msg_received(struct netio_subscribe_socket* socket, netio_tag_t tag, void* data, size_t size);

void on_init()
{
  printf("1. on_init\n");
  struct netio_buffered_socket_attr attr;
  attr.num_pages = NUM_PAGES;
  attr.pagesize = PAGESIZE;
  attr.watermark = WATERMARK;
  attr.timeout_ms = TIMEOUT;

  printf("Opening subscribe socket from %s on %s:%u\n",
         config.local_hostname, config.remote_hostname, config.subscribe_port);
  netio_subscribe_socket_init(&config.subscribe_socket, &config.ctx, &attr, config.local_hostname, config.remote_hostname, config.subscribe_port);

  config.subscribe_socket.cb_connection_closed = on_subscribe_connection_closed;
  config.subscribe_socket.cb_connection_established = on_subscribe_connection_established;
  config.subscribe_socket.cb_error_connection_refused = on_subscribe_error_connection_refused;
  config.subscribe_socket.cb_msg_received = on_msg_received;

  printf("attempting to subscribe to remote on %s:%u\n", config.remote_hostname, config.subscribe_port);
  printf("subscribing to tag 0x%lx\n", config.subscribe_tag);
  netio_subscribe(&config.subscribe_socket, config.subscribe_tag);
}


void on_subscribe_connection_closed(struct netio_subscribe_socket* socket)
{
  printf("subscribe_connection to publisher closed from %s:%u\n", socket->remote_hostname, socket->remote_port);
}


void on_subscribe_connection_established(struct netio_subscribe_socket* socket) {
  printf("2. on_subscribe_connection_established to %s:%u\n", socket->remote_hostname, socket->remote_port);

  pid_t pid = getpid();
  char cmd[80];

  sprintf(cmd, "lsof -a -p %d | wc -l", pid);
  char* last_fds = execute(cmd);
  printf("%s\n", last_fds);

  for (int i=0; i<config.iterations; i++) {
    printf("%d unsubscribing from tag 0x%lx\n", i, config.subscribe_tag);
    netio_unsubscribe(&config.subscribe_socket, config.subscribe_tag);

    sleep(2);

    printf("%d subscribing to tag 0x%lx\n", i, config.subscribe_tag);
    netio_subscribe(&config.subscribe_socket, config.subscribe_tag);

    char* fds = execute(cmd);
    printf("Iteration: %d: Number of File Descrpiptors: %s\n", i, fds);
    if (strcmp(last_fds, fds)) {
      printf("Number of FDs is increasing over time.\n");
      exit(2);
    }

    free(last_fds);
    last_fds = fds;
  }

  free(last_fds);
  netio_terminate(&config.ctx.evloop);
}


void on_subscribe_error_connection_refused(struct netio_subscribe_socket* socket)
{
  printf("subscribe_connection refused for subscribing from %s:%u\n", socket->remote_hostname, socket->remote_port);
}


void on_msg_received(struct netio_subscribe_socket* socket, netio_tag_t tag, void* data, size_t size)
{
  printf("4. on_msg_received for tag 0x%lx with size %zu\n", tag, size);
}


int main(int argc, char** argv)
{

  if(argc < 6) {
    fprintf(stderr, "Usage: %s <local_hostname> <hostname> <subscribe-port> <subscribe-tag> <iterations>\n", argv[0]);
    fprintf(stderr, "Version: %s", FELIX_TAG);
    return 1;
  }

  config.local_hostname = argv[1];
  config.remote_hostname = argv[2];
  config.subscribe_port = atoi(argv[3]);
  config.subscribe_tag = strtol(argv[4], NULL, 0);
  config.iterations = atoi(argv[5]);

  netio_init(&config.ctx);
  config.ctx.evloop.cb_init = on_init;
  netio_run(&config.ctx.evloop);

  return 0;
}
