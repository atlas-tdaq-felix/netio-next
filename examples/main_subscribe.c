#include <stdio.h>
#include <string.h>
#include <math.h>

#include "netio/netio.h"

#include "felixtag.h"


struct {
  char* local_hostname;
  char* remote_hostname;
  unsigned port;
  int use_fid;
  netio_tag_t first_tag;
  netio_tag_t last_tag;
  unsigned pages;
  unsigned pagesize;

  struct netio_context ctx;
  struct netio_subscribe_socket socket;

  struct netio_timer subscribe_timer;
  int subrequest_sent;

  uint64_t expected_messages;
} config;


struct {
    struct netio_timer timer;
    struct timespec t0;
    uint64_t messages_received;
    uint64_t bytes_received;
    int subscribed;

    uint64_t total_messages_received;
} statistics;

int return_code = 0;

void try_subscribe()
{
  if (!config.subrequest_sent) {
    printf("attempting to subscribe to remote on %s:%u\n", config.socket.remote_hostname, config.socket.remote_port);
    for(netio_tag_t tag = config.first_tag; tag <= config.last_tag; tag += config.use_fid ? 0x10000 : 1) {
      printf("subscribing to tag 0x%lx\n", tag);
      netio_subscribe(&config.socket, tag);
      config.subrequest_sent = 1;
    }
  }
}


void on_connection_closed(struct netio_subscribe_socket* socket)
{
  printf("connection to publisher closed from %s:%u\n", socket->remote_hostname, socket->remote_port);
  statistics.subscribed = 0;
  config.subrequest_sent = 0;
  // try to reconnect periodically
  netio_timer_start_s(&config.subscribe_timer, 1);
}


void on_connection_established(struct netio_subscribe_socket* socket) {
  printf("connection established to %s:%u\n", socket->remote_hostname, socket->remote_port);
  statistics.subscribed = 1;
  config.subrequest_sent = 0;
  netio_timer_stop(&config.subscribe_timer);
}


void on_error_connection_refused(struct netio_subscribe_socket* socket)
{
  printf("connection refused for subscribing from %s:%u\n", socket->remote_hostname, socket->remote_port);
  config.subrequest_sent = 0;
  // retry to connect periodically
  netio_timer_start_s(&config.subscribe_timer, 1);
}


void on_msg_received(struct netio_subscribe_socket* socket, netio_tag_t tag, void* data, size_t size)
{
  //printf("msg tag=%lu size=%lu\n", tag, size);
  //printf("msg no=%d\n", ((char*)data)[0]);
  statistics.total_messages_received++;
  if ((statistics.total_messages_received >= config.expected_messages) && (config.expected_messages > 0) && return_code == 0) {
    printf("Expected %lu messages received\n", config.expected_messages);
    for(netio_tag_t t = config.first_tag; t <= config.last_tag; t += config.use_fid ? 0x10000 : 1) {
      netio_unsubscribe(socket, t);
    }
    netio_timer_stop(&statistics.timer);
    netio_terminate(&config.ctx.evloop);
    return_code = 42;
  }
  statistics.messages_received++;
  statistics.bytes_received += size;
}


void on_stats(void* ptr) {
  if (!statistics.subscribed) return;
  struct timespec t1;
  clock_gettime(CLOCK_MONOTONIC_RAW, &t1);
  double seconds = t1.tv_sec - statistics.t0.tv_sec
                   + 1e-9*(t1.tv_nsec - statistics.t0.tv_nsec);
  printf("data rate: %2f Gb/s, message rate: %2f kHz, total messages: %lu\n",
         statistics.bytes_received*8/1024./1024./1024./seconds,
         statistics.messages_received/1000./seconds,
         statistics.total_messages_received);
  statistics.bytes_received = 0;
  statistics.messages_received = 0;
  statistics.t0 = t1;
}


void on_init()
{
  struct netio_buffered_socket_attr attr;
  attr.num_pages = config.pages;
  attr.pagesize = config.pagesize;
  attr.watermark = config.pagesize;
  attr.timeout_ms = 1000;

  printf("Opening subscribe socket from %s on %s:%u\n",
         config.local_hostname, config.remote_hostname, config.port);
  netio_subscribe_socket_init(&config.socket, &config.ctx, &attr, config.local_hostname, config.remote_hostname, config.port);

  config.socket.cb_connection_closed = on_connection_closed;
  config.socket.cb_connection_established = on_connection_established;
  config.socket.cb_error_connection_refused = on_error_connection_refused;
  config.socket.cb_msg_received = on_msg_received;

  netio_timer_init(&config.ctx.evloop, &statistics.timer);
  clock_gettime(CLOCK_MONOTONIC_RAW, &statistics.t0);
  statistics.timer.cb = on_stats;
  netio_timer_start_s(&statistics.timer, 1);

  // init time but don't start it now
  netio_timer_init(&config.ctx.evloop, &config.subscribe_timer);
  config.subscribe_timer.cb = try_subscribe;
  // attempt to subscribe
  try_subscribe();
}


int main(int argc, char** argv)
{

  if(argc < 6 || argc > 9) {
    fprintf(stderr, "Usage: %s <local_hostname> <remote_hostname> <port> <first_tag> <last_tag> [expected_messages>=0 (pages=256 pagesize=65535)]\n", argv[0]);
    fprintf(stderr, "Version: %s", FELIX_TAG);
    return 1;
  }

  config.local_hostname = argv[1];
  config.remote_hostname = argv[2];
  config.port = atoi(argv[3]);
  config.first_tag = strtol(argv[4], NULL, 0);
  config.last_tag = strtol(argv[5], NULL, 0);
  config.pages = 256;
  config.pagesize = 64*1024;
  config.use_fid = (config.first_tag & 0x1000000000000000) > 0L;
  printf("%d\n", config.use_fid);

  config.expected_messages = 0;
  if (argc == 7) {
    config.expected_messages = atoi(argv[6]);
  }
  if (argc == 8) {
    config.pages = strtol(argv[6], NULL, 0);
  }
  if (argc == 9) {
    config.pagesize = strtol(argv[7], NULL, 0);
  }

  netio_init(&config.ctx);
  config.ctx.evloop.cb_init = on_init;
  netio_run(&config.ctx.evloop);

  return return_code;
}
