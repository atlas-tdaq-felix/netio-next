#include <stdio.h>
#include <string.h>
#include <math.h>
#include <unistd.h>
#include <sys/inotify.h>

#include "netio/netio.h"

#include "felixtag.h"

struct {
  char* dir;

  struct netio_context ctx;
  struct netio_event_context event_ctx;
} config;

int return_code = 0;

#define NAME_MAX 80
#define BUF_LEN (10 * (sizeof(struct inotify_event) + NAME_MAX + 1))

static void             /* Display information from inotify_event structure */
displayInotifyEvent(struct inotify_event *i)
{
    printf("    wd =%2d; ", i->wd);
    if (i->cookie > 0)
        printf("cookie =%4d; ", i->cookie);

    printf("mask = ");
    if (i->mask & IN_ACCESS)        printf("IN_ACCESS ");
    if (i->mask & IN_ATTRIB)        printf("IN_ATTRIB ");
    if (i->mask & IN_CLOSE_NOWRITE) printf("IN_CLOSE_NOWRITE ");
    if (i->mask & IN_CLOSE_WRITE)   printf("IN_CLOSE_WRITE ");
    if (i->mask & IN_CREATE)        printf("IN_CREATE ");
    if (i->mask & IN_DELETE)        printf("IN_DELETE ");
    if (i->mask & IN_DELETE_SELF)   printf("IN_DELETE_SELF ");
    if (i->mask & IN_IGNORED)       printf("IN_IGNORED ");
    if (i->mask & IN_ISDIR)         printf("IN_ISDIR ");
    if (i->mask & IN_MODIFY)        printf("IN_MODIFY ");
    if (i->mask & IN_MOVE_SELF)     printf("IN_MOVE_SELF ");
    if (i->mask & IN_MOVED_FROM)    printf("IN_MOVED_FROM ");
    if (i->mask & IN_MOVED_TO)      printf("IN_MOVED_TO ");
    if (i->mask & IN_OPEN)          printf("IN_OPEN ");
    if (i->mask & IN_Q_OVERFLOW)    printf("IN_Q_OVERFLOW ");
    if (i->mask & IN_UNMOUNT)       printf("IN_UNMOUNT ");
    printf("\n");

    if (i->len > 0)
        printf("        name = %s\n", i->name);
}

void on_file(int fd, void* ptr)
{
  char buf[BUF_LEN];
  ssize_t numRead;
  char *p;

  printf("File changed %p", ptr);

  numRead = read(fd, buf, BUF_LEN);
  if (numRead == 0) {
      printf("read() from inotify fd returned 0!");
      return_code = 1;
      return;
  }

  if (numRead == -1) {
      printf("read error");
      return_code = 1;
      return;
  }

  printf("Read %ld bytes from inotify fd\n", (long) numRead);

  /* Process all of the events in buffer returned by read() */

  for (p = buf; p < buf + numRead; ) {
      struct inotify_event* event = (struct inotify_event *) p;
      displayInotifyEvent(event);

      p += sizeof(struct inotify_event) + event->len;
  }
}


void on_init()
{
  printf("Init");
}


int main(int argc, char** argv)
{
  if(argc != 2) {
    fprintf(stderr, "Usage: %s <directory>\n", argv[0]);
    fprintf(stderr, "Version: %s", FELIX_TAG);
    return 1;
  }

  config.dir = argv[1];

  netio_init(&config.ctx);
  config.ctx.evloop.cb_init = on_init;

  int fd = inotify_init();
  if (fd == -1) {
    printf("inotify_init error");
    return 1;
  }

  int wd = inotify_add_watch(fd, config.dir, IN_ALL_EVENTS);
  if (wd == -1) {
    printf("inotify_watch error");
    return 1;
  }

  printf("Using netio\n");
  config.event_ctx.fd = fd;
  netio_register_read_fd(&config.ctx.evloop, &config.event_ctx);
  config.event_ctx.cb = on_file;

  netio_run(&config.ctx.evloop);

  return return_code;
}
