#include <stdio.h>
#include <string.h>
#include <math.h>

#include "netio/netio.h"

#include "felixtag.h"

#define NUM_BUFFERS (256)
#define BUFFER_SIZE (64*1024)

#define MIN(X, Y) (((X) < (Y)) ? (X) : (Y))

struct {
  char* hostname;
  unsigned port;

  struct netio_context ctx;
  struct netio_send_socket socket;
  struct netio_buffer buf[NUM_BUFFERS];

  uint64_t messages;
} config;

struct {
    uint64_t total_messages_sent;
} statistics;

// Forward declarations
void on_connection_established(struct netio_send_socket*);
void on_send_completed(struct netio_send_socket* socket, uint64_t key);
void on_connection_closed(struct netio_send_socket* socket);

// Callbacks
void on_init(void* ptr)
{
  netio_send_socket_init_and_connect(&config.socket, &config.ctx, config.hostname, config.port);
  config.socket.cb_connection_closed = on_connection_closed;
  config.socket.cb_connection_established = on_connection_established;
  config.socket.cb_send_completed = on_send_completed;
}

void on_connection_established(struct netio_send_socket* socket)
{
  printf("connection established\n");
  for(unsigned i=0; i<NUM_BUFFERS; i++) {
    config.buf[i].size = BUFFER_SIZE;
    config.buf[i].data = malloc(config.buf[i].size);
    netio_register_send_buffer(socket, &config.buf[i], 0);
    netio_send_buffer(socket, &config.buf[i]);
  }
}

void on_connection_closed(struct netio_send_socket* socket) {
  printf("on_connection_closed\n");
  if ((statistics.total_messages_sent >= config.messages) && (config.messages > 0)) {
    netio_terminate(&config.ctx.evloop);
  }
}

void on_send_completed(struct netio_send_socket* socket, uint64_t key)
{
  statistics.total_messages_sent++;
  if ((statistics.total_messages_sent >= config.messages) && (config.messages > 0)) {
    // no more send
    printf("Done sending\n");
    return;
  }

  struct netio_buffer* buf = (struct netio_buffer*)key;
  uint64_t header = 40;
  uint8_t data[40];
  for(unsigned i=0; i<BUFFER_SIZE; ) {
    memcpy((char *)buf->data+i, &header, MIN(BUFFER_SIZE-i, sizeof(header)));
    i += sizeof(header);
    memcpy((char *)buf->data+i, &header, MIN(BUFFER_SIZE-i, sizeof(data)));
    i += sizeof(data);
  }
  //printf("sending '%s'\n", buf->data);
  netio_send_buffer(socket, buf);
}

int main(int argc, char** argv)
{
  if (argc < 3 || argc > 4) {
    fprintf(stderr, "Usage: %s <hostname> <port> [messages>=0]\n", argv[0]);
    fprintf(stderr, "Version: %s", FELIX_TAG);
    return 1;
  }

  config.hostname = argv[1];
  config.port = atoi(argv[2]);

  config.messages = 0;
  if (argc >= 4) {
    config.messages = atoi(argv[3]);
  }

  netio_init(&config.ctx);
  config.ctx.evloop.cb_init = on_init;
  netio_run(&config.ctx.evloop);

  return 0;
}
