#include <stdio.h>
#include <pthread.h>
#include <unistd.h>
#include <stdlib.h>
#include <argp.h>
#include "netio/netio.h"
#include "src/log.h"
#include "felixtag.h"

/*
* This programs is meant to measure the time it takes for the
* callback of a newly registered signal to be called.
* The test is perfomed in two conditions:
* 1. no other signal registered
* 2. with other two signals fired continuously by another thread
* 3. with other twenty signals fired continuously by another thread
*/

const char *argp_program_version = "example-signals v0.1";
const char *argp_program_bug_address = "<https://its.cern.ch/jira/projects/FLXUSERS>";

/* Program documentation. */
static char doc[] = "Test application to measure latency in executing the callback of a netio signal.";

/* A description of the arguments we accept. */
static char args_doc[] = "";
static struct argp_option options[] = {
  {"thread",   't', 0,          0,  "Fire probe signal with a separate thread. Default: false" },
  {"period",   'p', "PERIOD",   0,  "Period of probe signal [us]. Default: 500000" },
  {"bnumber",  'n', "BPERIOD",  0,  "Period of probackground signals [us]. Default 1" },
  {"bthread",  'b', "BTHREADS", 0,  "Number of background signals fired by dedicated threads. Default: 0" },
  {"samples",  's', "SAMPLES",  0,  "Number of times the probe signal is fired. Default: 10" },
  {"random",   'r', "RAND",     0,  "Randomise background within +/- RAND*PERIOD. Default 0" },
  {"verbose",  'v', 0,          0,  "Print latency at every callback. Default: false" },
  {"mode",     'm', 0,          0,  "Test mode Default: false" },
  { 0 }
};

struct config {
  unsigned int probe_thread;
  unsigned int bkg_threads;
  unsigned int period_bkg_us;
  double period_bkg_rand;
  unsigned int period_probe_us;
  unsigned int probe_samples;
  unsigned int verbose;
  unsigned int test_mode;
};

struct config conf;

/* Parse a single option. */
static error_t
parse_opt (int key, char *arg, struct argp_state *state)
{
  char* end;

  switch (key)
    {
    case 't':
      conf.probe_thread = 1;
      break;

    case 'v':
      conf.verbose = 1;
      break;

    case 'm':
      conf.test_mode = 1;
      break;

    case 'p':
      conf.period_probe_us = strtol(arg, &end, 0);
      if(end==arg) {
        argp_error(state, "'%s' is not a number", arg);
      }
      break;

    case 'n':
      conf.period_bkg_us = strtol(arg, &end, 0);
      if(end==arg) {
        argp_error(state, "'%s' is not a number", arg);
      }
      break;

    case 'b':
      conf.bkg_threads = strtol(arg, &end, 0);
      if(end==arg) {
        argp_error(state, "'%s' is not a number", arg);
      }
      break;

    case 's':
      conf.probe_samples = strtol(arg, &end, 0);
      if(end==arg) {
        argp_error(state, "'%s' is not a number", arg);
      }
      break;

    case 'r':
      conf.period_bkg_rand = strtod(arg, &end);
      if(end==arg) {
        argp_error(state, "'%s' is not a number", arg);
      }
      break;

    case ARGP_KEY_ARG:
    case ARGP_KEY_END:
      if (state->arg_num >= 1)
        /* Too many arguments. */
        argp_usage (state);
      break;

    default:
      return ARGP_ERR_UNKNOWN;
    }
  return 0;
}


inline unsigned int rand_wait(int lower, int upper)
{
  return  (unsigned int)((rand() % (upper - lower + 1)) + lower);
}

/* Our argp parser. */
static struct argp argp = { options, parse_opt, args_doc, doc };

void cli_parse(int argc, char **argv)
{
  // set default values
  conf.probe_thread = 0;
  conf.bkg_threads = 0;
  conf.period_bkg_us = 1;
  conf.period_probe_us = 500000;
  conf.probe_samples = 10;
  conf.period_bkg_rand = 0;
  conf.test_mode = 0;

  // parse arguments
  argp_parse (&argp, argc, argv, 0, 0, NULL);
}


struct signal_stats
{
  uint64_t counter_fired;
  uint64_t counter_called;
  struct timespec fired;
  struct timespec called;
  double cumulative_delay_us;
};

int running;
struct config conf;
struct netio_context ctx;
struct netio_timer timer;
struct netio_signal probe_signal;
struct netio_signal* bkg_signals;
struct signal_stats* bkg_stats;
pthread_t* bkg_threads;
pthread_t probe_thread;
struct signal_stats probe_stats;


double difference_us(struct timespec* t1, struct timespec* t0)
{
  double diff = 1e6*(t1->tv_sec - t0->tv_sec) + 0.001*(t1->tv_nsec - t0->tv_nsec);
  return diff;
}

double difference_s(struct timespec* t1, struct timespec* t0)
{
  double diff = (t1->tv_sec - t0->tv_sec) + (1e-9)*(t1->tv_nsec - t0->tv_nsec);
  return diff;
}


void fire_probe_signal()
{
  probe_stats.counter_fired++;
  if (probe_stats.counter_fired == probe_stats.counter_called + 1){
    clock_gettime(CLOCK_MONOTONIC_RAW, &probe_stats.fired);
  } else {
    log_warn("Not resetting probe timer because last call not executed yet. Fired %lu, called %lu.", probe_stats.counter_fired, probe_stats.counter_called);
    log_warn("Suggestion: inrease probe period, check argument parsing");
  }
  netio_signal_fire(&probe_signal);
}


void* thread_fire_probe_signal()
{
  struct timespec wait;
  wait.tv_sec = 0;
  wait.tv_nsec = 1000*conf.period_probe_us;
  while(running){
    fire_probe_signal();
    nanosleep(&wait, NULL);
  }
  return NULL;
}

void on_timer(void* ptr)
{
  if(conf.probe_thread == 0){
    log_info("Timer callback fires probe signal");
    clock_gettime(CLOCK_MONOTONIC_RAW, &probe_stats.called);
    fire_probe_signal();
  } else {
    running = 1;
    int rv = pthread_create(&probe_thread, NULL, thread_fire_probe_signal, NULL);
    log_info("Timer callback starts probe firing thread, pthread_create retrurns %d", rv);
  }
  netio_timer_stop(&timer);
  netio_timer_close(&ctx.evloop, &timer);
}


void* fire_periodically(void* ptr)
{
  struct netio_signal* sig = (struct netio_signal*) ptr;
  struct timespec wait;
  struct timespec remaining;
  wait.tv_sec = 0;
  wait.tv_nsec = 1000*conf.period_bkg_us;
  while(running){
    netio_signal_fire(sig);
    if (wait.tv_nsec > 0){
      if(conf.period_bkg_rand > 0){
        wait.tv_nsec = rand_wait(conf.period_bkg_rand*wait.tv_nsec, (1+conf.period_bkg_rand)*wait.tv_nsec);
      }
      int rv = nanosleep(&wait, &remaining);
      if (rv == -1 && errno == EINTR){
        nanosleep(&remaining, NULL);
      }
    }
  }
  return NULL;
}


void on_init()
{
  log_info("on init fires timer to kick-start test");
  netio_timer_init(&ctx.evloop, &timer);
  timer.cb = on_timer;
  timer.data = NULL;
  log_info("Spawning %u background threads.", conf.bkg_threads);
  for(unsigned int i=0; i<conf.bkg_threads; ++i){
    running = 1;
    pthread_create(&bkg_threads[i], NULL, fire_periodically, (void*)(&bkg_signals[i]));
    clock_gettime(CLOCK_MONOTONIC_RAW, &bkg_stats[i].fired);
  }
  if(conf.bkg_threads > 0){
    log_info("Waiting 5 seconds for background threads to be running...");
    netio_timer_start_ms(&timer, 5000);
  } else {
    netio_timer_start_ms(&timer, 1000);
  }
}


void on_bkg_signal(void* ptr) 
{
  struct signal_stats* bs = (struct signal_stats*)ptr;
  bs->counter_called++;
}


void on_probe_signal(void* ptr)
{
  struct signal_stats* ps = (struct signal_stats*) ptr;
  ++(ps->counter_called);

  clock_gettime(CLOCK_MONOTONIC_RAW, &ps->called);
  double delay = difference_us(&ps->called, &ps->fired);
  if (conf.verbose){
    log_info("Probe callback: call %lu (fired %lu) delay %f us", ps->counter_fired, ps->counter_fired, delay);
  }
  ps->cumulative_delay_us += delay;

  //fire next if configured
  if (conf.probe_thread == 0 && ps->counter_fired < conf.probe_samples){
    fire_probe_signal();
  }

  if (ps->counter_called == conf.probe_samples){
      double avg = ps->cumulative_delay_us / ps->counter_called;
      log_info("RESULT: Average delay %f us\n", avg);

      //joing threads if started
      running = 0;
      if(conf.probe_thread){
        pthread_join(probe_thread, NULL);
      }
      for(unsigned int i=0; i<conf.bkg_threads; ++i){
        pthread_join(bkg_threads[i], NULL);
        clock_gettime(CLOCK_MONOTONIC_RAW, &bkg_stats[i].called);
      }

      netio_terminate(&ctx.evloop);
      netio_signal_close(&ctx.evloop, &probe_signal);
  }
}


int main(int argc, char** argv) {

  cli_parse(argc, argv);
  bkg_signals = malloc(conf.bkg_threads * sizeof(struct netio_signal));
  bkg_stats = malloc(conf.bkg_threads * sizeof(struct signal_stats));
  bkg_threads = malloc((conf.bkg_threads)*sizeof(pthread_t));

  netio_init(&ctx);
  ctx.evloop.cb_init = on_init;

  log_info("Probe settings: period %u us, samples %u firing thread %u", conf.probe_samples, conf.period_probe_us, conf.probe_thread);
  log_info("Background settings: period %u us, random fraction %f, signals %u \n", conf.period_bkg_us, conf.period_bkg_rand, conf.bkg_threads);

  //prepare probe signal
  probe_stats.counter_fired = 0;
  probe_stats.counter_called = 0;

  netio_signal_init(&ctx.evloop, &probe_signal);
  probe_signal.cb = on_probe_signal;
  probe_signal.data = (void*)(&probe_stats);

  //background signals
  for(unsigned int i=0; i < conf.bkg_threads; ++i){
    netio_signal_init(&ctx.evloop, &bkg_signals[i]);
    bkg_signals[i].cb = on_bkg_signal;
    bkg_signals[i].data = &bkg_stats[i];
    bkg_stats[i].counter_fired = 0;
    bkg_stats[i].counter_called = 0;
  }

  // run event loop
  log_info("Running eventloop");
  netio_run(&ctx.evloop);

  if (conf.verbose){
    for(unsigned int i=0; i < conf.bkg_threads; ++i){
      double runtime = difference_s(&bkg_stats[i].called, &bkg_stats[i].fired);
      double rate_called = bkg_stats[i].counter_called/runtime;
      log_info("Bkg signal %u: runtime %f s, call rate %f Hz", i, runtime, rate_called);
    }
  }

  if (conf.test_mode){
    if (probe_stats.counter_fired == conf.probe_samples){
      return 42;
    }
  } else {
    return 0;
  }
}
