#include <stdio.h>
#include <string.h>
#include <math.h>
#include "netio/netio.h"
#include "netio/netio_tcp.h"


//To run on pc-tbed-sh-42: ./send_tcp 10.193.170.193 61617 1 (or 2 for 2 senders)


struct 
{
  int receivers;
  char* hostname;
  unsigned port;
  struct netio_context ctx;
  struct netio_send_socket socket;
  struct netio_send_socket socket2;
} config;


// Forward declarations
void on_connection_established(struct netio_send_socket*);
void on_connection_refused(struct netio_send_socket*);
void on_send_completed(struct netio_send_socket* socket, uint64_t key);
void on_connection_established2(struct netio_send_socket*);
void on_connection_refused2(struct netio_send_socket*);
void on_send_completed2(struct netio_send_socket* socket, uint64_t key);


//Globals
int mess_count1 = 0, mess_count2 = 0 ;
struct netio_buffer databuf1, databuf2;
int *databuffer1, *databuffer2;

// Callbacks

/*********************/
void on_init(void* ptr)
/*********************/
{
  netio_init_send_tcp_socket(&config.socket, &config.ctx);
  netio_connect_tcp(&config.socket, config.hostname, config.port);
  config.socket.cb_connection_established = on_connection_established;
  config.socket.cb_send_completed = on_send_completed;
  config.socket.cb_error_connection_refused = on_connection_refused;

  if(config.receivers == 2)
  {
    netio_init_send_tcp_socket(&config.socket2, &config.ctx);
    netio_connect_tcp(&config.socket2, config.hostname, config.port + 1);
    config.socket2.cb_connection_established = on_connection_established2;
    config.socket2.cb_send_completed = on_send_completed2;
    config.socket.cb_error_connection_refused = on_connection_refused2;
  }
}


/**********************************************************/
void on_connection_refused(struct netio_send_socket* socket)
/**********************************************************/
{
  printf(">>>>>>>>>>>>connection refused<<<<<<<<<<<<<<<<<");
}


/***********************************************************/
void on_connection_refused2(struct netio_send_socket* socket)
/***********************************************************/
{
  printf(">>>>>>>>>>>>connection refused<<<<<<<<<<<<<<<<<");
}

/**************************************************************/
void on_connection_established(struct netio_send_socket* socket)
/**************************************************************/
{
  databuffer1[0]                     = 0x10000000;
  databuffer1[databuf1.size / 4 - 1] = 0x20000000;
  netio_send_buffer(socket, &databuf1);
  mess_count1++;
  printf("on_connection_established OK\n");
}


/***************************************************************/
void on_connection_established2(struct netio_send_socket* socket)
/***************************************************************/
{
  databuffer2[0]                     = 0x30000000;
  databuffer2[databuf2.size / 4 - 1] = 0x40000000;
  mess_count2++;
  netio_send_buffer(socket, &databuf2);  
  printf("on_connection_established2 OK\n");
}


/********************************************************************/
void on_send_completed(struct netio_send_socket* socket, uint64_t key)
/********************************************************************/
{
  if (mess_count1 < 10)
    printf("Sender 1, on_send_completed: %d messages sent\n", mess_count1);

  databuffer1[0]                     = 0x10000000 + mess_count1;
  databuffer1[databuf1.size / 4 - 1] = 0x20000000 + mess_count1;
  netio_send_buffer(socket, &databuf1);
  mess_count1++;

  if ((mess_count1 % 10000) == 0)
    printf("Sender 1, %d messages sent\n", mess_count1);
}


/*********************************************************************/
void on_send_completed2(struct netio_send_socket* socket, uint64_t key)
/*********************************************************************/
{
  if (mess_count2 < 10)
    printf("Sender 2, on_send_completed: %d messages sent\n", mess_count2);

  databuffer2[0]                     = 0x30000000 + mess_count2;
  databuffer2[databuf1.size / 4 - 1] = 0x40000000 + mess_count2;
  netio_send_buffer(socket, &databuf2); 
  mess_count2++;

  if ((mess_count2 % 10000) == 0)
    printf("Sender 2, %d messages sent\n", mess_count2);
}


/*****************************/
int main(int argc, char** argv)
/*****************************/
{
  if(argc != 4) 
  {
    fprintf(stderr, "Usage: %s <hostname> <port> <#receivers>\n", argv[0]);
    return 1;
  }

  databuf1.size = 1000000;
  databuffer1   = malloc(databuf1.size);
  databuf1.data = (void *)databuffer1;

  databuf2.size = 1000000;
  databuffer2   = malloc(databuf2.size);
  databuf2.data = (void *)databuffer2;

  config.hostname  = argv[1];
  config.port      = atoi(argv[2]);
  config.receivers = atoi(argv[3]);
  
  netio_init(&config.ctx);
  config.ctx.evloop.cb_init = on_init;
  
  netio_run(&config.ctx.evloop);
  free(databuffer1);
  free(databuffer2);

  return 0;
}









