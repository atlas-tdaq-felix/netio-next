#include <stdio.h>
#include <string.h>
#include <math.h>
#include <unistd.h>

#include "netio/netio.h"

#include "felixtag.h"

#include "util.h"

#define NUM_PAGES (256)
#define WATERMARK (63*1024)
#define PAGESIZE (64*1024)

struct {
  char* hostname;
  unsigned port;

  struct netio_context ctx;
  struct netio_buffered_send_socket socket;
  struct netio_buffered_socket_attr attr;

  const char* data;
  size_t len;

  uint64_t messages;
  int count;

  char cmd[80];
  pid_t pid;
  char* last_fds;
} config;

struct {
    uint64_t total_messages_sent;
} statistics;

// Forward declarations
void on_connection_established(struct netio_buffered_send_socket*);
void on_connection_closed(struct netio_buffered_send_socket*);
void on_error_connection_refused(struct netio_buffered_send_socket*);
void on_timer(void* ptr);

// Callbacks
void on_init(void* ptr)
{
  printf("on_init\n");
  config.data = "Helloworld";
  config.len = strlen(config.data);

  config.attr.num_pages = NUM_PAGES;
  config.attr.pagesize = PAGESIZE;
  config.attr.watermark = WATERMARK;
  config.attr.timeout_ms = 1000;

  netio_buffered_send_socket_init_and_connect(&config.socket, &config.ctx, config.hostname, config.port, &config.attr);
  config.socket.cb_connection_established = on_connection_established;
  config.socket.cb_connection_closed = on_connection_closed;
  config.socket.cb_error_connection_refused = on_error_connection_refused;

  config.pid = getpid();
  sprintf(config.cmd, "lsof -a -p %d | wc -l", config.pid);
  config.last_fds = execute(config.cmd);
  printf("%s\n", config.last_fds);
}

void on_connection_established(struct netio_buffered_send_socket* socket)
{
  printf("connection established %p\n", socket);

  netio_buffered_send(&config.socket, (void*)config.data, config.len);
  netio_buffered_flush(&config.socket);

  netio_disconnect(&socket->send_socket);

  char* fds = execute(config.cmd);
  printf("Iteration: %d: Number of File Descrpiptors: %s\n", config.count, fds);
  // if (strcmp(config.last_fds, fds)) {
  //   printf("Number of FDs is increasing over time.\n");
  //   exit(2);
  // }
  free(config.last_fds);
  config.last_fds = fds;

  config.count++;
  if (config.count > config.messages) {
    netio_terminate(&config.ctx.evloop);
  }

  netio_buffered_connect(&config.socket, config.hostname, config.port);
}

void on_connection_closed(struct netio_buffered_send_socket* socket) {
  printf("on_connection_closed %p\n", socket);
}

void on_error_connection_refused(struct netio_buffered_send_socket* socket) {
  printf("on_error_connection_refused %p\n", socket);
}

int main(int argc, char** argv)
{
  if (argc > 4) {
    fprintf(stderr, "Usage: %s <hostname> <port> <messages>\n", argv[0]);
    fprintf(stderr, "Version: %s", FELIX_TAG);
    return 1;
  }

  config.hostname = argv[1];
  config.port = atoi(argv[2]);
  config.messages = atoi(argv[3]);

  netio_init(&config.ctx);
  config.ctx.evloop.cb_init = on_init;
  netio_run(&config.ctx.evloop);

  return 0;
}
