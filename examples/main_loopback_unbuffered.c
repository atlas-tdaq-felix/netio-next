#include <stdio.h>
#include <string.h>
#include <math.h>

#include "netio/netio.h"

#include "felixtag.h"

#define NUM_BUFFERS (256)
#define BUFFER_SIZE (64*1024)

struct {
    const char* hostname;
    unsigned recv_port;
    netio_tag_t recv_tag;
    unsigned pub_port;
    netio_tag_t pub_tag;

    struct netio_context ctx;

    struct netio_listen_socket recv_socket;
    struct netio_unbuffered_socket_attr recv_attr;

    struct netio_unbuffered_publish_socket pub_socket;
    struct netio_buffer buf;

    uint8_t* data;
    size_t datasize;
} config;

// Forward declarations
void recv_on_connection_established(struct netio_recv_socket*);
void recv_on_connection_closed(struct netio_recv_socket*);
void on_msg_received(struct netio_recv_socket*, struct netio_buffer* buf, void*, size_t);
void pub_on_subscribe(struct netio_unbuffered_publish_socket*, netio_tag_t, void*, size_t);
void pub_on_connection_established(struct netio_unbuffered_publish_socket*);
void pub_on_connection_closed(struct netio_unbuffered_publish_socket*);

// Callbacks
void on_init(void *ptr)
{
    setbuf(stdout, NULL);

    printf("1. on_init\n");
    config.recv_attr.num_buffers = NUM_BUFFERS;
    config.recv_attr.buffer_size = BUFFER_SIZE;

    netio_listen_socket_init_and_listen(&config.recv_socket, &config.ctx, config.hostname, config.recv_port, &config.recv_attr);
    config.recv_socket.cb_connection_established = recv_on_connection_established;
    config.recv_socket.cb_connection_closed = recv_on_connection_closed;
    config.recv_socket.cb_msg_received = on_msg_received;

    printf("Opening publish socket on %s:%u\n", config.hostname, config.pub_port);

    config.buf.size = 1024;
    config.buf.data = malloc(config.buf.size);

    netio_unbuffered_publish_socket_init(&config.pub_socket, &config.ctx, config.hostname, config.pub_port,  &config.buf);
    config.pub_socket.cb_subscribe = pub_on_subscribe;
    config.pub_socket.cb_connection_established = pub_on_connection_established;
    config.pub_socket.cb_connection_closed = pub_on_connection_closed;
}

void recv_on_connection_established(struct netio_recv_socket* socket)
{
    printf("4. recv_on_connection_established\n");
}

void recv_on_connection_closed(struct netio_recv_socket* socket) {
    printf("recv_on_connection_closed\n");
}

void on_msg_received(struct netio_recv_socket* socket, struct netio_buffer* buf, void* data, size_t len)
{
    printf("5. on_msg_received %zu\n", len);

    char* payload = (char*)data;
    printf("data: %s\n", payload);

    struct iovec iov;
    iov.iov_base = payload;
    iov.iov_len = strlen(payload);
    uint64_t key = 0xFF;
    // supply an answer
    netio_unbuffered_publishv(&config.pub_socket,
                            config.pub_tag,
                            &iov,
                            1,    // iov count
                            &key, // key
                            0,    // flags
                            NULL  // subscription cache
                            );

}

// publish
void pub_on_subscribe(struct netio_unbuffered_publish_socket* socket, netio_tag_t pub_tag, void* addr, size_t addrlen) {
    printf("2. pub_on_subscribe to pub-tag 0x%lx\n", pub_tag);
}

void pub_on_connection_established(struct netio_unbuffered_publish_socket* socket) {
    printf("3. pub_on_connection_established\n");
}

void pub_on_connection_closed(struct netio_unbuffered_publish_socket* socket) {
    printf("pub connection to subscriber closed\n");
}

int main(int argc, char** argv)
{
    if (argc < 6) {
        fprintf(stderr, "Usage: %s <hostname> <recv-port> <recv-tag> <pub-port> <pub-tag>\n", argv[0]);
        fprintf(stderr, "Version: %s", FELIX_TAG);
        return 1;
    }

    config.hostname = argv[1];
    config.recv_port = atoi(argv[2]);
    config.recv_tag = strtol(argv[3], NULL, 0);
    config.pub_port = atoi(argv[4]);
    config.pub_tag = strtol(argv[5], NULL, 0);

    netio_init(&config.ctx);
    config.ctx.evloop.cb_init = on_init;
    netio_run(&config.ctx.evloop);

    return 0;
}
