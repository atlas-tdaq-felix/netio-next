#include <stdio.h>
#include <time.h>
#include "netio/netio.h"
#include "netio/netio_tcp.h"

#define NUM_BUFFERS 1 //MJ: Does it make sense for TCP to have more than 1 buffer?
#define BUFFER_SIZE 1000000 


struct 
{
  int senders;
  char* hostname;
  unsigned port;
  struct netio_context ctx;
  struct netio_listen_socket socket;
  struct netio_listen_socket socket2;
} config;


// Forward declarations
void on_connection_established(struct netio_recv_socket*);
void on_connection_established2(struct netio_recv_socket*);
void on_msg_received(struct netio_recv_socket*, struct netio_buffer* buf, void* data, size_t len);
void on_msg_received2(struct netio_recv_socket*, struct netio_buffer* buf, void* data, size_t len);


//Globals
struct netio_buffer recv_buf, recv_buf2;
int messages_received1 = 0, messages_received2 = 0, marker;


// Callbacks
/*********************/
void on_init(void *ptr)
/*********************/
{
  struct netio_unbuffered_socket_attr attr = {NUM_BUFFERS, BUFFER_SIZE};

  netio_init_listen_tcp_socket(&config.socket, &config.ctx, &attr);
  netio_listen_tcp(&config.socket, config.hostname, config.port);
  config.socket.cb_connection_established = on_connection_established;
  config.socket.cb_msg_received           = on_msg_received;

  if(config.senders == 2)
  {
    netio_init_listen_tcp_socket(&config.socket2, &config.ctx, &attr);
    netio_listen_tcp(&config.socket2, config.hostname, config.port + 1);
    config.socket2.cb_connection_established = on_connection_established2;
    config.socket2.cb_msg_received           = on_msg_received2;
  }
}


/**************************************************************/
void on_connection_established(struct netio_recv_socket* socket)
/**************************************************************/
{
  netio_post_recv(socket, &recv_buf);
  printf("Receiver 1, on_connection_established OK\n");         
}


/***************************************************************/
void on_connection_established2(struct netio_recv_socket* socket)
/***************************************************************/
{
  netio_post_recv(socket, &recv_buf2);  
  printf("Receiver 2, on_connection_established OK\n");         
}


/******************************************************************************************************/
void on_msg_received(struct netio_recv_socket* socket, struct netio_buffer* buf, void* data, size_t len)
/******************************************************************************************************/
{
  int *maker_ptr;
  
  if (len != 1000000)
    printf("Receiver 1, Message %d has wrong length %d\n", messages_received1, (int)len);         

  maker_ptr = (int *)buf[0].data;
  if (maker_ptr[0] != marker + messages_received1)
  {
    printf("Receiver 1, Message %d has wrong first word 0x%08x (0x%08x expected)\n", messages_received1, maker_ptr[0], marker + messages_received1);         
    exit(-1);
  }

  if (maker_ptr[len / 4 - 1] != marker + 0x10000000 + messages_received1)
  {
    printf("Receiver 1, Message %d has wrong last word 0x%08x\n", messages_received1, maker_ptr[0]);         
    exit(-1);
  }

  messages_received1++;
  netio_post_recv(socket, &buf[0]);
  
  if (messages_received1 < 10)
    printf("Receiver 1, %d messages received\n", messages_received1);    
  
  if ((messages_received1 % 10000) == 0)
    printf("Receiver 1, %d messages received\n", messages_received1);    
    
  if ((messages_received1 % 0x10000000) == 0)
// if (messages_received1 == 10)
  {
    printf("Receiver 1, That shouild be enough\n");    
    exit(0);
  }           
}


/*******************************************************************************************************/
void on_msg_received2(struct netio_recv_socket* socket, struct netio_buffer* buf, void* data, size_t len)
/*******************************************************************************************************/
{
  int *maker_ptr;
  
  if (len != 1000000)
    printf("Receiver 2, Message %d has wrong length %d\n", messages_received2, (int)len);         

  maker_ptr = (int *)buf[0].data;
  if (maker_ptr[0] != 0x30000000 + messages_received2)
  {
    printf("Receiver 2, Message %d has wrong first word 0x%08x\n", messages_received2, maker_ptr[0]);         
    exit(-1);
  }

  if (maker_ptr[len / 4 - 1] != 0x40000000 + messages_received2)
  {
    printf("Receiver 2, Message %d has wrong last word 0x%08x\n", messages_received2, maker_ptr[0]);         
    exit(-1);
  }

  messages_received2++;
  netio_post_recv(socket, &buf[0]);

  if ((messages_received2 % 10000) == 0)
    printf("Receiver 2, %d messages received\n", messages_received2);           

  if ((messages_received2 % 0x10000000) == 0)
  {
    printf("Receiver 2, That shouild be enough\n");    
    exit(0);
  }           
}


/*****************************/
int main(int argc, char** argv)
/*****************************/
{
  if(argc != 4) 
  {
    fprintf(stderr, "Usage: %s <hostname> <port> <#senders>\n", argv[0]);
    return 1;
  }

  config.hostname = argv[1];
  config.port     = atoi(argv[2]);
  config.senders  = atoi(argv[3]);

  if (config.port == 61618 && config.senders == 1)
  {
    printf("marker set for 2-1-1\n");
    marker = 0x30000000;
  }
  else
    marker = 0x10000000;

  netio_init(&config.ctx);  
  config.ctx.evloop.cb_init = on_init;
  netio_run(&config.ctx.evloop);

  return 0;
}
