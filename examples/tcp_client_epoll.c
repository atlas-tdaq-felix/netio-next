/****************************************************************/
/*								*/
/*  This is a stand-alone TCP-IP client and serves as a 	*/
/*  playgroud for testing ideas that may later end up in 	*/
/*  netio-next							*/
/*								*/
/*  5. Feb. 21  M. Joos  created				*/
/*								*/
/***********C 2021 - A nickel program worth a dime***************/


/*TO_DO
- Add IPv6 support, if required
*/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <string.h> 
#include <errno.h>
#include <netdb.h>	
#include <unistd.h>
#include <fcntl.h>
#include <arpa/inet.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/epoll.h>


#define MAXLINE   4096   //max text line length
#define SERV_PORT 3001   //port
#define MAXEVENTS 4      //for epoll


//Globals
int sockfd, efd;
struct epoll_event event, *events;


/*******************************************/
int hostname_to_ip(char *hostname , char *ip)
/*******************************************/
{
  struct addrinfo hints, *servinfo, *p;
  int rv;

  memset(&hints, 0, sizeof hints);
  hints.ai_family   = AF_UNSPEC; // use AF_INET6 to force IPv6
  hints.ai_socktype = SOCK_STREAM;

  if ((rv = getaddrinfo(hostname, "http", &hints, &servinfo)) != 0) 
  {
    printf("getaddrinfo: %s\n", gai_strerror(rv));
    return 1;
  }

  // loop through all the results and connect to the first we can
  for(p = servinfo; p != NULL; p = p->ai_next) 
  {
    struct sockaddr_in *h;
    h = (struct sockaddr_in *) p->ai_addr;
    strcpy(ip, inet_ntoa(h->sin_addr));
  }

  freeaddrinfo(servinfo); 
  
  return 0;
}


/************************************/
void make_socket_non_blocking(int sfd)
/************************************/
{
  int flags, s;

  flags = fcntl(sfd, F_GETFL, 0);
  if (flags == -1)
  {
    printf("ERROR in fcntl. flags = %d\n", flags);
    exit(0);
  }

  flags |= O_NONBLOCK;
  s = fcntl(sfd, F_SETFL, flags);
  if (s == -1)
  {
    printf("ERROR in fcntl. s = %d\n", s);
    exit(0);
  }
}


/***************************************************/
void check_socket(int event, int fd1, int fd2, int n)
/***************************************************/
{
  socklen_t in_len;
  int ret, retVal;
  
  if (n != 1)
  {
    printf("epoll_wait returns n = %d\n", n);
    exit(-7);
  }

  if(fd1 != fd2)
  {
    printf("check_socket: File descriptors don't match\n");
    exit(-5);
  }

  if (event != EPOLLIN && event != EPOLLOUT && event != (EPOLLIN | EPOLLOUT))
  {
    printf("check_socket: epoll_wait error = %d (%s)\n", event, strerror(event));
    printf("EPOLLIN      = 0x%08x\n", EPOLLIN);
    printf("EPOLLPRI     = 0x%08x\n", EPOLLPRI);
    printf("EPOLLOUT     = 0x%08x\n", EPOLLOUT);
    printf("EPOLLERR     = 0x%08x\n", EPOLLERR);
    printf("EPOLLHUP     = 0x%08x\n", EPOLLHUP);
    printf("EPOLLRDHUP   = 0x%08x\n", EPOLLRDHUP);
    printf("EPOLLWAKEUP  = 0x%08x\n", EPOLLWAKEUP);
    printf("EPOLLONESHOT = 0x%08x\n", EPOLLONESHOT);
    printf("EPOLLET      = 0x%08x\n", EPOLLET);
    exit(-1);
    //continue;
  }

  if (event & EPOLLOUT)
    printf("check_socket: EPOLLOUT received\n");
  if (event & EPOLLIN)
    printf("check_socket: EPOLLIN received\n");

  in_len = sizeof(retVal);
  ret = getsockopt(fd1, SOL_SOCKET, SO_ERROR, &retVal, &in_len);
  if (ret < 0)
  {
    printf("check_socket: ERROR: getsockopt failed. ret = %d\n", ret);
    exit(-6);
  }
  else if (retVal != 0)
  {
    printf("check_socket: ERROR: operation did not work. retVal = %d\n", retVal);
    exit(-6);
  }
  else
    printf("check_socket: Transmission OK\n");
}



/*****************************/
int main(int argc, char **argv)
/*****************************/
{
  int loop, ret, n;
  struct sockaddr_in servaddr;
  char sendline[MAXLINE], recvline[MAXLINE];
  char *hostname = argv[1], ip[100];
  char hbuf[NI_MAXHOST], sbuf[NI_MAXSERV];
  socklen_t in_len;

  if (argc != 2) 
  {
    perror("Usage: TCPClient <IP address of the server>");
    exit(1);
  }

  //Create the epoll file descriptor and connect it to the TCP/IP socket
  efd = epoll_create1(0);   //Instead of "0" we could pass "EPOLL_CLOEXEC" to the function. Check later which one makes more sense
  if (efd == -1)
  {
    perror ("epoll_create");
    abort();
  }
  printf("epoll created\n");
  
  //Create a socket for the client
  if ((sockfd = socket(AF_INET, SOCK_STREAM, 0)) < 0) 
  {
    perror("Problem in creating the socket");
    exit(2);
  }

  events = calloc(MAXEVENTS, sizeof event);    // Buffer where events are returned
  printf("Attaching socket %d to epoll\n", sockfd); 
  event.data.fd = sockfd;
  event.events = EPOLLIN | EPOLLOUT | EPOLLET;           //MJ: Do we want an edge trigger (EPOLLET) or a level trigger?
  //event.events = EPOLLIN | EPOLLOUT;                    //MJ: if we use a level trigger, connect() will wake up epoll all the time
  ret = epoll_ctl(efd, EPOLL_CTL_ADD, sockfd, &event);
  if (ret == -1)
  {
    perror("epoll_ctl");
    abort();
  }
  
  hostname_to_ip(hostname , ip);
  printf("%s resolved to %s\n" , hostname , ip);

  //Creation of the socket
  memset(&servaddr, 0, sizeof(servaddr));
  servaddr.sin_family      = AF_INET;

  ret = inet_pton(AF_INET, ip, &servaddr.sin_addr);  //IPv4
  if (ret < 1)
  {
    perror("Problem with inet_pton");
    exit(3);
  }

  servaddr.sin_port = htons(SERV_PORT);       //convert to big-endian order

  make_socket_non_blocking(sockfd);

  //Connection of the client to the socket
  ret = connect(sockfd, (struct sockaddr *) &servaddr, sizeof(servaddr));
  if (ret == 0)
  {
    printf("Already connected. Is this a problem for eopll?\n");
  }
  else
  {
    printf("ret = %d\n", ret);
    printf("errno = %d\n", errno);
   
    if (errno == EINPROGRESS)
    {
      printf("connect is EINPROGRESS. Let epoll handle it.\n");
    }
    else
    {
      printf("errno = %d (%s)\n", errno, strerror(errno));
      printf("Problem in connecting to the server. ret = %d\n", ret);
      exit(3);
    }
  } 

  printf("Calling epoll_wait to handle connect\n");
  n = epoll_wait(efd, events, MAXEVENTS, -1);   
  check_socket(events[0].events, events[0].data.fd, sockfd, n);
  printf("connect OK\n");

  in_len = sizeof servaddr;
  ret = getnameinfo((struct sockaddr *) &servaddr, in_len, hbuf, sizeof hbuf, sbuf, sizeof sbuf, 0);
  if (ret == 0)
    printf("Connection made on descriptor %d (host=%s, service=%s)\n", sockfd, hbuf, sbuf);

  while(1)
  {
    printf("Enter text to send........\n");
    fgets(sendline, MAXLINE, stdin);

    printf("Sending....\n");
    n = send(sockfd, sendline, strlen(sendline), 0);
    if(n != strlen(sendline))
    {
      printf("n = %d, strlen(sendline) = %lu\n",n ,(unsigned long) strlen(sendline));
      exit(-1);
    }
    printf("Send OK\n");

    for (loop = 0; loop < MAXLINE; loop++)
      recvline[loop] = 0;                          //Clear the receive buffer

    printf("Calling epoll_wait to handle receive\n");
    n = epoll_wait(efd, events, MAXEVENTS, -1);
    check_socket(events[0].events, events[0].data.fd, sockfd, n);
    printf("Data received. Process it...\n");
   
    int nchars = recv(sockfd, recvline, MAXLINE, 0);
    printf("nchars = %d\n", nchars);
 
    while(nchars > 0)  
    {
      printf("String of %d characters received from server:\n", n);
      puts(recvline);
      nchars = recv(sockfd, recvline, MAXLINE, 0);
    }

    printf("Play it again!..........\n\n\n\n");
  }
  exit(0);  //We will never get to this point...
}

