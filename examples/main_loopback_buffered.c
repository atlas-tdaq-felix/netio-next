#include <stdio.h>
#include <time.h>

#include "netio/netio.h"
#include "netio/netio_tcp.h"

#include "felixtag.h"

#define NUM_PAGES (256)
#define WATERMARK (56*1024)
#define PAGESIZE (64*1024)

struct {
    const char* hostname;
    unsigned recv_port;
    netio_tag_t recv_tag;
    unsigned pub_port;
    netio_tag_t pub_tag;

    struct netio_context ctx;

    struct netio_buffered_listen_socket recv_socket;
    struct netio_buffered_socket_attr recv_attr;

    struct netio_publish_socket pub_socket;
    struct netio_buffered_socket_attr pub_attr;

    uint8_t* data;
    size_t datasize;
} config;

// Forward declarations
void recv_on_connection_established(struct netio_buffered_recv_socket*);
void recv_on_connection_closed(struct netio_buffered_recv_socket*);
void on_msg_received(struct netio_buffered_recv_socket*, void*, size_t);
void pub_on_subscribe(struct netio_publish_socket*, netio_tag_t, void*, size_t);
void pub_on_connection_established(struct netio_publish_socket*);
void pub_on_connection_closed(struct netio_publish_socket*);

// Callbacks
void on_init(void *ptr)
{
    setbuf(stdout, NULL);

    printf("1. on_init\n");
    config.recv_attr.num_pages = NUM_PAGES;
    config.recv_attr.pagesize = PAGESIZE;
    config.recv_attr.watermark = WATERMARK;

    netio_buffered_listen_socket_init_and_listen(&config.recv_socket, &config.ctx, config.hostname, config.recv_port, &config.recv_attr);
    config.recv_socket.cb_connection_established = recv_on_connection_established;
    config.recv_socket.cb_connection_closed = recv_on_connection_closed;
    config.recv_socket.cb_msg_received = on_msg_received;

    config.pub_attr.num_pages = NUM_PAGES;
    config.pub_attr.pagesize = PAGESIZE;
    config.pub_attr.watermark = WATERMARK;
    config.pub_attr.timeout_ms = 1000;

    printf("Opening publish socket on %s:%u\n", config.hostname, config.pub_port);
    netio_publish_socket_init(&config.pub_socket, &config.ctx, config.hostname, config.pub_port, &config.pub_attr);
    config.pub_socket.cb_subscribe = pub_on_subscribe;
    config.pub_socket.cb_connection_established = pub_on_connection_established;
    config.pub_socket.cb_connection_closed = pub_on_connection_closed;
}

void recv_on_connection_established(struct netio_buffered_recv_socket* socket)
{
    printf("4. recv_on_connection_established\n");
}

void recv_on_connection_closed(struct netio_buffered_recv_socket* socket) {
    printf("recv_on_connection_closed\n");
}

void on_msg_received(struct netio_buffered_recv_socket* socket, void* data, size_t len)
{
    printf("5. on_msg_received %zu\n", len);

    char* payload = (char*)data;
    printf("data: '%s'\n", payload);

    // supply an answer
    netio_buffered_publish(&config.pub_socket, config.pub_tag, (void*)data, len, 0, NULL);
    printf("sent for tag 0x%lx\n", config.pub_tag);
    netio_buffered_publish_flush(&config.pub_socket, 0, NULL);
    printf("flushed\n");
}

// publish
void pub_on_subscribe(struct netio_publish_socket* socket, netio_tag_t pub_tag, void* addr, size_t addrlen) {
    printf("2. pub_on_subscribe to pub-tag 0x%lx\n", pub_tag);
}

void pub_on_connection_established(struct netio_publish_socket* socket) {
    printf("3. pub_on_connection_established\n");
}

void pub_on_connection_closed(struct netio_publish_socket* socket) {
    printf("pub connection to subscriber closed\n");
}

int main(int argc, char** argv)
{
    if (argc < 6) {
        fprintf(stderr, "Usage: %s <hostname> <recv-port> <recv-tag> <pub-port> <pub-tag>\n", argv[0]);
        fprintf(stderr, "Version: %s", FELIX_TAG);
        return 1;
    }

    config.hostname = argv[1];
    config.recv_port = atoi(argv[2]);
    config.recv_tag = strtol(argv[3], NULL, 0);
    config.pub_port = atoi(argv[4]);
    config.pub_tag = strtol(argv[5], NULL, 0);

    netio_init(&config.ctx);
    config.ctx.evloop.cb_init = on_init;
    netio_run(&config.ctx.evloop);

    return 0;
}
